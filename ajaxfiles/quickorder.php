<?php
require_once '../app/Mage.php';
umask(0);
Mage::app('default');
Mage::getSingleton('core/session', array('name'=>'frontend'));

$email = trim($_GET['email']);
$keycode = trim($_GET['offer']);
$productSku = trim($_GET['sku']);
$itemflg = $_GET['itemflg'];
$emailflg = $_GET['emailflg'];
$offerflg = $_GET['offerflg'];
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
if($emailflg !=null ){
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        Mage::getSingleton('core/session')->setData("quickOrderEmailId",'');
        echo 'Where will your order confirmation be sent?';
    }else{
        Mage::getSingleton('core/session')->setData("quickOrderEmailId",$email);
        echo 'Your confirmation will be sent to: '.$email;
    }
}
else if($offerflg != null ){
    $rule_id = $write->FetchOne("select rule_id from salesrule_coupon where code ='".$keycode."' ");
    $current_date = date('Y-m-d');
    $bannerImage='';
    $bannerCount = $write->fetchOne("SELECT count(Offer_Code) as count  FROM  salesrule where rule_id='".$rule_id."' and '".$current_date."' BETWEEN  from_date AND to_date ");
    $block ='';
    if($bannerCount){
        $rule = Mage::getModel('salesrule/rule')->load($rule_id);
        $bannerImage = $rule->getDescription();
        $imageArray = explode("^", $bannerImage);
        $bannerType = $imageArray[0];
        $expiredate = $imageArray[1];


        if($imageArray[0]==1){
            $block ='<div class="bc-Twentyfive mainhomebanner" >
						<img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/main-bc-01.jpg"  border="0"  />
					</div>';
        }else if($imageArray[0]==2 ){
            $block ='<div class="bc-Twentyfive mainhomebanner"  >
						<img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/main-bc-02.jpg"  border="0"  />
					</div>';

        }else if($imageArray[0]==3 ){

            $block ='<div class="bc-Twentyfive mainhomebanner"  >
						<img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/main-bc-03.jpg"  border="0"  />
					</div>';
        }else if($imageArray[0]==4){
            $block = '<div class="bc-Twentyfive mainhomebanner"  >
						<img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/main-bc-04.jpg"  border="0"  />
					</div>';
        }else if($imageArray[0]==5 ){
            
            $block ='<div class="bc-Twentyfive mainhomebanner"  >
						<img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/main-bc-05.jpg"  border="0"  />
					</div>';
        }else if($imageArray[0]==6 ){           
            $block ='<div class="bc-Twentyfivee mainhomebanner"  >
						<img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/main-bc-06.jpg"  border="0"  />
					</div>';
        }else if($imageArray[0]==7){
            $block ='
					<div class="bc-Seven"  >
						<span class="TextOne">Hurry! <br />Take advantage of our</span>
						<span class="TextTwo">1¢ Sale</span>
						<span class="TextThree"> Buy one item at regular price get the second for only a penny! </span> <span class="TextFour">Start Saving Now!</span>

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
        }else if($imageArray[0]==8 ){
            $block ='
					<div class="bc-Eight"  >
						<span class="TextOne">Limited -Time ONLY!</span>
						<span class="TextTwo">FREE
						<span>Shipping & Handling</span></span>
						<span class="TextThree"> Order by the deadline below and you\'ll get FREE Shipping Handling on your next order!</span> <span class="TextFour">Start Saving Now!</span>

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
        }else if($imageArray[0]==9 ){
            $block ='
					<div class="bc-Eight" >
						<span class="TextOne">Limited -Time ONLY!</span>
						<span class="TextTwo">FREE
						<span>Shipping & Handling</span></span>
						<span class="TextThree">Order by the deadline below and you\'ll get FREE Shipping Handling on your next order of $25 or more!</span> <span class="TextFour">Start Saving Now!</span>

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
        }else if($imageArray[0]==10 ){
            $block ='
					<div class="bc-Eight"  >
						<span class="TextOne">Limited -Time ONLY!</span>
						<span class="TextTwo">FREE
						<span>Shipping & Handling</span></span>
						<span class="TextThree">Order by the deadline below and you\'ll get FREE Shipping Handling on your next order of $75 or more!</span> <span class="TextFour">Start Saving Now!</span>

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
        }else if($imageArray[0]==11 ){
            $block ='
					<div class="bc-Eleven"  >
						<span class="TextOne">Sitewide Sale!</span>
						<span class="TextTwo">Save 10% Off<span>your total order.</span></span>
						<span class="TextThree">Order by the deadline below and Save 10% off your next order!</span>
						<span class="TextFour">Start Saving Now!</span>

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
        }else if($imageArray[0]==12 ){
            $block ='
					<div class="bc-Twelve"  >
						<span class="TextOne">Sitewide Sale!</span>
						<span class="TextTwo">Save 15% Off<span><em>Plus Free Shipping & Handling</em></span></span>
						<span class="TextThree">Order by the deadline below and Save 15% off and get Free Shipping and Handling with $25 Order!</span>
						<span class="TextFour">Start Saving Now!</span>
						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
        }else if($imageArray[0]==13 ){
            $block ='';
        }else if($imageArray[0]==14 ){
            $block ='
					<div class="bc-Fourteen"  >
						<span class="TextOne">Take Our Survey and</span>
						<span class="TextTwo">Get A FREE<br  />$5 Coupon</span>
						<span class="TextThree">After you receive your order we\'ll email you a 5 question survey. Just answer the question and you\'ll receive a $5 savings coupon for your next purchase. It\'s that simple!</span>
						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
        }else if($imageArray[0]==15 ){
            $block ='
					<div class="bc-Fifteen" >
						<span class="TextOne">For A Limited-Time ONLY!</span>
						<span class="TextTwo">SAVE $25<span>when you spend $50</span></span>
						<span class="TextTwo Green">SAVE $50<span>when you spend $100</span></span>
						<span class="TextTwo DarkGreen" >SAVE $100<span>when you spend $200</span></span>


						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
        }else if($imageArray[0]==16 ){
            $block ='
					<div class="bc-Sixteen"  >
						<span class="TextOne">For A Limited-Time ONLY!</span>
						<span class="TextTwo">Save up to 75% Off<span>Select Items</span></span>
						<span class="TextThree">Take advantage of special saving on limited<br />
						qualities of some of our popular items!</span>
						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
        }
        else if($imageArray[0]==18 ){
            $block ='
					<div class="bc-Eighteen"  >
						<span class="TextOne">Special Offer</span>
						<span class="TextTwo">Save up to '.$imageArray[2].'% <span>on Selected Items</span></span>
						<span class="TextThree">Order by the deadline below and Save up to '.$imageArray[2].'%!</span>
						<span class="TextFour">Start Saving Now!</span>

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
        }
        else if($imageArray[0]==19 ){
            $block =
                '<div class="bc-Nineteen"  >
						<div style=" position:absolute; left:0px; top:0px;"><img src="'.$imageArray[1].'" width="320" height="320" /></div>
						<span class="TextOne">'.$imageArray[2].'</span>
						<span class="TextTwo">'.$imageArray[3].' <span>Save '.$imageArray[4].'%</span></span>
						<span class="TextFour">&nbsp;</span>
						<span class="TextExpires"><span>Hurry!</span> Offer Expires<br />'.$imageArray[5].'</span>
					</div>';
        }
        else if($imageArray[0]==20 ){
            $block ='
				<div class="bc-Twenty"  > <span class="TextOne">'.$imageArray[1].'</span>
					<span class="TextTwo">SAVE<span>UP<br />TO</span>&nbsp;&nbsp;'.$imageArray[2].'%!</span>
					<span class="TextThree">Hurry! Offer Expires '.$imageArray[3].'</span>
					<span class="TextExpires">&nbsp;</span>
				</div>';
        }
        else if($imageArray[0]==21 ){
            $block ='
					<div class="bc-Twentyone"  > <span class="TextOne">Sitewide Sale!</span>
					<span class="TextTwo">Save '.$imageArray[1].'% Off<span>your total order</span></span>
					<span class="TextThree">Order by the deadline below and Save '.$imageArray[1].'% off your next order!</span>
					<span class="TextFour">&nbsp;</span>
					<span class="TextExpires"><span style="color:#fff;">Hurry!</span> Offer Expires<br /> '.$imageArray[2].'</span>
					</div>';
        }
        else if($imageArray[0]==23 ){
            $block =
                '<div class="bc-Twentythree"  >
						<div style=" position:absolute; left:0px; top:0px;"><img src="'.$imageArray[1].'" width="325" height="320" /></div>
						<span class="TextOne">Take $'.$imageArray[2].' Off</span>
						<span class="TextTwo">your next order of $'.$imageArray[3].' or more.</span></span>
						<div style="text-align:center;margin-top:5px;" ><img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/start-shopping-big.png"  /></div>
					</div>';
        }
        else if($imageArray[0]==24 ){
            $block =
                '<div class="bc-Twentyfour"  >
						<span class="TextOne">Take $'.$imageArray[1].' Off</span>
						<span class="TextTwo">your next order of $'.$imageArray[2].' or more.</span>
						<div style="text-align:center;margin-top:5px;" ><img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/banner/start-shopping-big24.png"  /></div>
					</div>';
        }
        else if($imageArray[0]==25 ){
            $block =
                '<div class="bc-Twentyfive"  >
						<img src="'.$imageArray[1].'"  border="0"  />
					</div>';
        }
    }
    //echo $block;
    if($block!=null){
        Mage::getSingleton('core/session')->setData("offerCode",$keycode);
        $block .= '<div class="close offerClosePop" onclick="closeOfferPopUp()">Close<span>X</span></div>';
        //$block .='<a  class="close" onclick="closeOfferPopUp()" href="javascript:void(0)"></a>';
        echo '<div id="bannerImagePopUp" style="display:none;" >'.$block.'</div>';
        echo 'Your offer has been activated. <a href="javascript:void(0)'.$offerCode.'" id="offerPopUp" style="color: #336633;font-weight:bold;" >Click here</a> for offer details.';
    }else{
        Mage::getSingleton('core/session')->setData("offerCode",'');
        echo 'Activate any offers with the key code number in the green box';
    }


}else{
    $offerCode = 4; //For SH, the default offer code is always 4
    if (!empty($keycode)){
        $offerCode = $write->fetchOne("SELECT Offer_Code FROM keycodes where Keycode='".$keycode."' ");
        if(empty($offerCode))
            $offerCode = 4;
    }

    $offerProductPrice= $write->fetchAll("SELECT * FROM price where Product_Number='".$productSku."' and  Offer_Code='".$offerCode."' ");
    if(empty($offerProductPrice))
        $offerProductPrice = $write->fetchAll("SELECT * FROM price where Product_Number='".$productSku."' and  Offer_Code='4' ");

    $block = '';
    $i=0;
    $entity_id = $write->fetchOne("SELECT entity_id FROM catalog_product_entity where sku='".$productSku."' ");

    $option = '<option value="" class=""></option>';
    if($entity_id){
        $product = Mage::getModel('catalog/product')->load($entity_id);
        $uom = $product->getUnitOfMeasure();
        if(empty($uom)) $uom=1;
        $option='';
        for($i=1;$i<=10;$i++){
            $option .= '<option value="'.$i*$uom.'" class="">'.$i*$uom.'</option>';
        }
    }
    if(count($offerProductPrice)>0){
        foreach($offerProductPrice as $tierInfo){
            $i++;
            if(empty($uom)) $uom = 1;
            if($uom){
                if($offerCode==4){
                    $withOutOfferProductPrice = $write->fetchOne("SELECT Price FROM price where Qty_Low='".$tierInfo['Qty_Low']."' AND Product_Number='".$productSku."' and  Offer_Code='4' ");
                    $withOutOfferFinalPrice = substr( number_format(($withOutOfferProductPrice*$tierInfo['Qty_Low']),4) , 0 , -2 );
                    $withOutOfferProductSalePrice = $write->fetchOne("SELECT Sale_Price FROM price where Qty_Low='".$tierInfo['Qty_Low']."' AND Product_Number='".$productSku."' and  Offer_Code='4' ");
                    $withOutOfferSaleFinalPrice = substr( number_format(($withOutOfferProductSalePrice*$tierInfo['Qty_Low']),4) , 0 , -2 );
                    $finalPrice = substr( number_format(($tierInfo['Price']*$tierInfo['Qty_Low']),4) , 0 , -2 );
                    if($tierInfo['Sale_Price']){
                        //echo $withOutOfferFinalPrice."++++++".$withOutOfferSaleFinalPrice;
                        if($withOutOfferFinalPrice<$withOutOfferSaleFinalPrice){
                            if($i==1)
                                $block .= "<span class='price' style='text-decoration: line-through;' >".($uom*$tierInfo['Qty_Low']) . " for $" .$withOutOfferSaleFinalPrice. "</span> <br />";
                            $block .= "<span class='price' style='color:#FF0000;' >".($uom*$tierInfo['Qty_Low']) . " for $" .$withOutOfferFinalPrice . "</span> <br />";
                        }else{
                            $block .= "<span class='price'  >".($uom*$tierInfo['Qty_Low']) . " for $" .$withOutOfferFinalPrice . "</span> <br />";
                        }


                    }else{
                        if($tierInfo['Price']!=$withOutOfferProductPrice){
                            $withOutOfferFinalPrice = substr( number_format(($withOutOfferProductPrice*$tierInfo['Qty_Low']),4) , 0 , -2 );
                            if($i==1)
                                $block .= "<span class='price' style='text-decoration: line-through;' >".($uom*$tierInfo['Qty_Low']) . " for $" .$withOutOfferFinalPrice . "</span> <br />";

                            $block .= "<span class='price' style='color:#FF0000;' >".($uom*$tierInfo['Qty_Low']) . " for $" .$finalPrice . "</span> <br />";
                        }else{
                            $block .= "<span class='price' >".($uom*$tierInfo['Qty_Low']) . " for $" .$finalPrice . "</span> <br />";
                        }
                    }
                }else{
                    $withOutOfferProductPrice = $write->fetchOne("SELECT Price FROM price where Qty_Low='".$tierInfo['Qty_Low']."' AND Product_Number='".$productSku."' and  Offer_Code='4' ");
                    $finalPrice = substr( number_format(($tierInfo['Price']*$tierInfo['Qty_Low']),4) , 0 , -2 );
                    if($tierInfo['Price']!=$withOutOfferProductPrice){
                        $withOutOfferFinalPrice = substr( number_format(($withOutOfferProductPrice*$tierInfo['Qty_Low']),4) , 0 , -2 );
                        if($i==1)
                            $block .= "<span class='price' style='text-decoration: line-through;' >".($uom*$tierInfo['Qty_Low']) . " for $" .$withOutOfferFinalPrice . "</span> <br />";

                        $block .= "<span class='price' style='color:#FF0000;' >".($uom*$tierInfo['Qty_Low']) . " for $" .$finalPrice . "</span> <br />";
                    }else{
                        if($tierInfo['Sale_Price']!=null && $tierInfo['Sale_Price'] > $tierInfo['Price'] ){
                            if($i==1)
                                $block .= "<span class='price' style='text-decoration: line-through;' >".($uom*$tierInfo['Qty_Low']) . " for $" .$tierInfo['Sale_Price'] . "</span> <br />";
                            $block .= "<span class='price' style='color:#FF0000;' >".($uom*$tierInfo['Qty_Low']) . " for $" .substr( number_format(($tierInfo['Price']*$tierInfo['Qty_Low']),4) , 0 , -2 ) . "</span> <br />";
                        }else{
                            $block .= "<span class='price'  >".($uom*$tierInfo['Qty_Low']) . " for $" .$finalPrice . "</span> <br />";
                        }
                    }
                }
            }else{
                $block .= "<span class='price'>".$tierInfo['Qty_Low'] . " for </span>" . Mage::helper('core')->currency($tierInfo['Price']). "<br />";
            }
        }

    }

    if($entity_id && $block!=null){

        echo '<td class="text" style="width:30%;min-height:25px;" ><input type="text" name="sku-entry[]" id="productSku" value="'.$productSku.'" class="input-text" onkeyup="getItemInfo();" /></td>
        <td class="text" style="width:20%;min-height:25px;" >'.$block.'</td>
        <td class="text" style="width:20%;min-height:25px;"><select name="qty[]" class="" >
            '.$option.'
        </select></td>
        <td class="text" style="width:30%; min-height:25px;">
         <button type="button" onclick="document.getElementById(\'contactForm\').submit();" class="button btn-cart"><span><span style="color:#fff;">ADD TO CART</span></span></button>
        <div id="loaderImg" style="display: none;"><img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/loader_quickorder.gif" border="0" width="24" height="24"></div></td>
      ';
    }else{
        echo '
        <td class="text" style="width:40%;min-height:25px;"><input type="text" style="width:100px" name="sku-entry[]" id="productSku" value="'.$productSku.'" class="input-text" onkeyup="getItemInfo();" /></td>
        <td class="text" style="width:30%;min-height:25px;">--</td>
        <td class="text" style="width:20%;min-height:25px;">
            <select name="qty[]" class="" >
                <option value="" class=""></option>
            </select>
        </td>
        <td class="text" style="width:10%;min-height:25px;">
        <div id="loaderImg" style="display: none;"><img src="'.Mage::getBaseUrl().'skin/frontend/enterprise/springhills/images/loader_quickorder.gif" border="0" width="24" height="24"></div></td>
      ';
    }


}


?>