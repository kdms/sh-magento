<?php
require_once '../app/Mage.php'; 
umask(0);
Mage::app('default');
$zipcode= $_GET['zipcode'];
$resource = new Mage_Core_Model_Resource();
$read = $resource->getConnection('core_read');
$select = $read->select()->from('hardiness_zone')->where('zipcode=?', $zipcode);
$fetchregdata = $read->fetchAll($select);
$count_reg = count($fetchregdata);
$zone = $fetchregdata[0]['zone'];
echo "Your Garden is in Hardiness Zone ".$zone;
?>
