<?php

class Magestore_Banner_Block_Adminhtml_Customerattribute_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'banner';
        $this->_controller = 'adminhtml_customerattribute';
        
        $this->_updateButton('save', 'label', Mage::helper('banner')->__('Save Attribute'));
        $this->_updateButton('delete', 'label', Mage::helper('banner')->__('Delete Attribute'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('customerattribute_description') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'customerattribute_description');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'customerattribute_description');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('customerattribute_data') && Mage::registry('customerattribute_data')->getId() ) {
			
		   return Mage::helper('banner')->__("Edit Attribute '%s'", $this->htmlEscape(Mage::registry('customerattribute_data')->getAttrType()));
        } else {
            
			return Mage::helper('banner')->__('Add Attribute');
        }
    }
}
