<?php
class Magestore_Banner_Block_Adminhtml_Category extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_category';
    $this->_blockGroup = 'banner';
    $this->_headerText = Mage::helper('banner')->__('Category Manager');
    $this->_addButtonLabel = Mage::helper('banner')->__('Add Category');
    parent::__construct();
  }
}