<?php

class Springhills_Catalogprice_Adminhtml_CatalogpriceController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("catalogprice/catalogprice")->_addBreadcrumb(Mage::helper("adminhtml")->__("Catalogprice  Manager"),Mage::helper("adminhtml")->__("Catalogprice Manager"));
				return $this;
		}
		public function indexAction() 
		{
				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{
				$brandsId = $this->getRequest()->getParam("id");
				$brandsModel = Mage::getModel("catalogprice/catalogprice")->load($brandsId);
				if ($brandsModel->getId() || $brandsId == 0) {
					Mage::register("catalogprice_data", $brandsModel);
					$this->loadLayout();
					$this->_setActiveMenu("catalogprice/catalogprice");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Catalogprice Manager"), Mage::helper("adminhtml")->__("Catalogprice Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Catalogprice Description"), Mage::helper("adminhtml")->__("Catalogprice Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("catalogprice/adminhtml_catalogprice_edit"))->_addLeft($this->getLayout()->createBlock("catalogprice/adminhtml_catalogprice_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("catalogprice")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("catalogprice/catalogprice")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("catalogprice_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("catalogprice/catalogprice");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Catalogprice Manager"), Mage::helper("adminhtml")->__("Catalogprice Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Catalogprice Description"), Mage::helper("adminhtml")->__("Catalogprice Description"));


		$this->_addContent($this->getLayout()->createBlock("catalogprice/adminhtml_catalogprice_edit"))->_addLeft($this->getLayout()->createBlock("catalogprice/adminhtml_catalogprice_edit_tabs"));

		$this->renderLayout();

		       // $this->_forward("edit");
		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {
                        /* validation while adding*/
                        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                        $ProductSku = $post_data['Product_Number'];
                        $ProductOfferCode = $post_data['Offer_Code'];
                        $ProductQtyLow = $post_data['Qty_Low'];
						$editId = $this->getRequest()->getParam("id");
                        if(empty($editId)){
                            $query = "SELECT COUNT(*) FROM price where Product_Number= '".$ProductSku."' AND Offer_Code='".$ProductOfferCode."' AND Qty_Low='".$ProductQtyLow."'";
                        }else{
                            $query = "SELECT COUNT(*) FROM price where catalogprice_id!=".$editId."	AND Product_Number= '".$ProductSku."' AND Offer_Code='".$ProductOfferCode."' AND Qty_Low='".$ProductQtyLow."'";
                        }
			echo $query;
                        $rowCount = $write->fetchOne($query);
                        if($rowCount > 0){
                            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("catalogprice")->__("Price already exists with this combination of product number['".$ProductSku."'],offercode['".$ProductOfferCode."'] and min qty['".$ProductQtyLow."']"));
                            $this->_redirect("*/*/");
                            return;
                        }
                        /******END OF VALIDATION*******/
						$post_data['updated_at']=date('Y-m-d H:i:s');
                      	$brandsModel = Mage::getModel("catalogprice/catalogprice")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Catalogprice was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setCatalogpriceData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $brandsModel->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setCatalogpriceData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$brandsModel = Mage::getModel("catalogprice/catalogprice");
						$brandsModel->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}
}
