<?php


class Springhills_Catalogprice_Block_Adminhtml_Catalogprice extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

		$this->_controller = "adminhtml_catalogprice";
		$this->_blockGroup = "catalogprice";
		$this->_headerText = Mage::helper("catalogprice")->__("Catalog Price Manager");
		$this->_addButtonLabel = Mage::helper("catalogprice")->__("Add New Price");
		parent::__construct();
		//$this->removeButton('add');

	}

}
