<?php
	
class Springhills_Catalogprice_Block_Adminhtml_Catalogprice_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "catalogprice_id";
				$this->_blockGroup = "catalogprice";
				$this->_controller = "adminhtml_catalogprice";
				$this->_updateButton("save", "label", Mage::helper("catalogprice")->__("Save Price"));
				//$this->_updateButton("delete", "label", Mage::helper("catalogprice")->__("Delete Price"));
                $this->removeButton('delete');
				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("catalogprice")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("catalogprice_data") && Mage::registry("catalogprice_data")->getId() ){

				    return Mage::helper("catalogprice")->__("Edit Sku %s", $this->getProductNumber());

				} 
				else{

				     return Mage::helper("catalogprice")->__("Add Item");

				}
		}
}
