<?php
/**
* Example View block
*
* @codepool   Local
* @category   Fido
* @package    Springhills_Cache
* @module     Example
*/
class Springhills_Cache_Block_Zonesignup extends Mage_Core_Block_Template
{
    private $message;
    private $att;
	private $actionName;
	private $routeName;
	private $moduleName;
	private $controllerName;

   public function getSignupBlock() {
        $message = '';
		$bannerofferCode = Mage::getSingleton('core/session')->getData("offerCode");
		$cookieEmailId = Mage::getModel('core/cookie')->get('email_id');
		
		if(empty($cookieEmailId)){

		$message .= '
		<form action="'.$this->getBaseUrl().'newsletter/subscriber/new/" method="post" id="newsletter-validate-detail-zone">
  

		<div class="getfree">
    		<div class="textone">Get Free $25 Now</div>
   			 <div class="texttwo">With our Email Newsletter</div>
    		<div class="email">
     			<input type="text"  onfocus="if (this.value == \'Email Address\') {this.value = \'\';} this.style.color = \'#B2B2B2\';" onblur="if (this.value == \' \') {this.value = \'Email Address\'; } this.style.color = \'#B2B2B2\';" title="Email Address"  value="\'Email Address\'" />
    		</div>
   		 <input type="button" class="rebutton" alt="Receive Email Coupon" title="Receive Email Coupon" />
 	 </div>


		</form>
		<script type="text/javascript">
		//<![CDATA[
			var newsletterSubscriberFormDetail = new VarienForm(\'newsletter-validate-detail-zone\');
		//]]>
		</script>
		';

		}else{
			$message .= '<div style="height: 30px;">&nbsp;</div> ';
		}
		
					
		return $message;
    }

	public function setParams($moduleName,$actionName,$routeName,$controllerName){
		if($routeName != 'enterprise_pagecache'){
			$this->actionName = $actionName;
			$this->moduleName = $moduleName;
			$this->routeName = $routeName;
			$this->controllerName = $controllerName;
		}
	}


 	protected function _toHtml() {
        $html = parent::_toHtml();
        return $html;
    }

	public function getCacheKeyInfo() {
		$info = parent::getCacheKeyInfo();

		$moduleName = Mage::app()->getRequest()->getModuleName();
		$actionName = Mage::app()->getRequest()->getActionName();
		$routeName = Mage::app()->getRequest()->getRouteName();
		$controllerName = Mage::app()->getRequest()->getControllerName();
		if($routeName!='enterprise_pagecache'){
			$info['routeName'] = $routeName;
			$info['actionName'] = $actionName;
			$info['moduleName'] = $moduleName;
			$info['controllerName'] = $controllerName;
		}

		return $info;
	}


}
