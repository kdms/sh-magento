<?php
require_once 'redback/RedbackServices.php';
class Springhills_Newsletter_Model_Observer extends Mage_Core_Model_Abstract 
{
	/**
	 * Running cron job for redback_status=0;
	 */
	public function runCronForRedBackStatus(){
		$write =Mage::getSingleton('core/resource')->getConnection('core_write');
		$arrSubscriber =$write->fetchAll("SELECT subscriber_id,subscriber_emai,created_time from  newsletter_subscriber where redback_status=0");
		foreach ($arrSubscriber as $subscriber){
			 //@todo Make REDBACK API CALL SEND THE EMAIL ID and TITLE ,Update the status in DB
			 //REDBACK API CALL TO SEND THE EMAIL ID
	               $objRedBackApi = new RedbackServices();
				   $data =array(
						'Title'	=>'5',
						'CustNumber'=>'',
						'CustFirstName'=>'',
						'CustLastName'=>'',
						'CustCompany'=>'',
						'CustAddr1'=>'',
						'CustAddr2'=>'',
						'CustCity'=>'',
						'CustState'=>'',
						'CustZip'=>'',
						'CustEmail'=>$email,
						'CustEmIP'=>'',
						'CustPhone'=>'',
						'PromoCode'=>'',
						'OptOutFlag'=>'',
						'ContestCode'=>'',					
						'CatErrCode'=>'',
						'CatErrMsg'=>'',
						'Bonus'=>'',
						'debug'=>'',
						'debug_id'=>'',
						'BirthMonth'=>'',
						'BirthDay'=>'',
						'ConfirmEmail'=>'',
						'IPaddress'=>'',
						'TimeStamp'=>''
						);
					$redbackStatus = $objRedBackApi->newsletterService($data);
			 if($redbackStatus['Catalog_RequestResult']){
				$this->updateRedBackStatus($subscriber['subscriber_id'],1);
			 }else{
				//Check for the time its not been updated to redback since it has stoed in database ,if time is more than 12 hours send mail to support email
				$date1 = date('Y-m-d H:i:s');
				$ts1 = strtotime($date1);
				$ts2 = strtotime($subscriber['created_time']);
				$seconds_diff = $ts1 - $ts2;
				$hours = floor($seconds_diff/3600);
				//Iflast import is done before 36 hours send mail
				if($hours > 12){
					//mail Body
					$body ="Redback sync failed for the email id".$subscriber['subscriber_email'];				
					$adminInfo =$write->fetchAll("SELECT * from  admin_user");
					$userFirstname =$adminInfo[0]['firstname'];
					$userLastname =$adminInfo[0]['lastname'];
					$userEmail=$adminInfo[0]['email'];				
					$arrTo[]=array("toEmail"=>$userEmail,"toName"=>$userFirstname.' '.$userLastname);
					$mail = new Zend_Mail();
					$mail->setType(Zend_Mime::MULTIPART_RELATED);
					$mail->setBodyHtml('Redback Sync failed for signup request');
					$mail->setFrom($userEmail, $userFirstname.' '.$userLastname);
					//SENDING MAIL TO Janesh,imsupport@gardensalive.com and Magento Admin
					foreach($arrTo as $to){
						$mail->addTo($to['toEmail'], $to['toName']);
					}
					$mail->setSubject('Sync failed for user signup');
					try {
						if($mail->send())
						{
							//Mage::getSingleton('core/session')->addSuccess('Log Information has been mailed');
						}
					}
					catch(Exception $ex) {
						Mage::getSingleton('core/session')->addSuccess('Unable to send mail');
						
					}
				} 
			}
		}
	}
	
	/**
	 * To update redbackAPI status
	 * @param int $id
	 * @param tinyint $iStatus
	 */
	public function updateRedBackStatus($id,$iStatus){
		$write =Mage::getSingleton('core/resource')->getConnection('core_write');
		$write->query("update newsletter_subscriber set redback_status=".$iStatus." where subscriber_id=".$id);
	}
}
