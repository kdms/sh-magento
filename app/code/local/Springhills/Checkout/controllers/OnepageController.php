<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

require_once("app/code/core/Mage/Checkout/controllers/OnepageController.php");
require_once 'redback/RedbackServices.php';
class Springhills_Checkout_OnepageController extends Mage_Checkout_OnepageController
{
    

	public function saveBillingAction()
		{
			$this->_expireAjax();
			if ($this->getRequest()->isPost()) {
				$data = $this->getRequest()->getPost('billing', array());
				$customerAddressId = $this->getRequest()->getPost('billing_address_id', false);
				$result = $this->getOnepage()->saveBilling($data, $customerAddressId);	
				$session = Mage::getSingleton("core/session");
				if(!empty($data['company'])){
					$session->setIsMobilephone("1");
				}else{
					$session->setIsMobilephone("");
				}
				if($data['is_subscribed'] ==1){
					$session->setIsMobileoptin('1');
				}else{
					$session->setIsMobileoptin('');
				}
 
				if (!isset($result['error'])) {
				    /* check quote for virtual */
				    if ($this->getOnepage()->getQuote()->isVirtual()) {
				        $result['goto_section'] = 'payment';
				        $result['update_section'] = array(
				            'name' => 'payment-method',
				            'html' => $this->_getPaymentMethodsHtml()
				        );
				    }
				    elseif (isset($data['use_for_shipping']) && $data['use_for_shipping'] == 1) {

					$data2 = $this->getRequest()->getPost('shipping_method','flatmodifiedshipping_flatmodifiedshipping');
					$result2 = $this->getOnepage()->saveShippingMethod($data2);
				Mage::getSingleton('checkout/type_onepage')->getQuote()->getShippingAddress()-> setShippingMethod($data2)->save();

				    $result['goto_section'] = 'payment';
				    $result['update_section'] = array(
				        'name' => 'payment-method',
				        'html' => $this->_getPaymentMethodsHtml()
				    );
				    }
				    else {
				        $result['goto_section'] = 'shipping';
				    }
				}
				$this->getResponse()->setBody(Zend_Json::encode($result));
			}
		} 

		public function saveShippingAction()
			{
				$this->_expireAjax();
				if ($this->getRequest()->isPost()) {
					$this->saveShippingMethodAction();
					$data = $this->getRequest()->getPost('shipping', array());
					$customerAddressId = $this->getRequest()->getPost('shipping_address_id', false);
					$result = $this->getOnepage()->saveShipping($data, $customerAddressId);

					if (!isset($result['error'])) {
						$result['goto_section'] = 'payment';
						$result['update_section'] = array(
						    'name' => 'payment-method',
						    'html' => $this->_getPaymentMethodsHtml()
						);
					}

			//        $this->loadLayout('checkout_onepage_shippingMethod');
			//        $result['shipping_methods_html'] = $this->getLayout()->getBlock('root')->toHtml();
			//        $result['shipping_methods_html'] = $this->_getShippingMethodsHtml();
					$this->getResponse()->setBody(Zend_Json::encode($result));
				}
			} 


	public function saveShippingMethodAction()
		{
			$this->_expireAjax();
			if ($this->getRequest()->isPost()) {
				$data = $this->getRequest()->getPost('shipping_method', 'flatmodifiedshipping_flatmodifiedshipping');
				$result = $this->getOnepage()->saveShippingMethod($data);
				Mage::getSingleton('checkout/type_onepage')->getQuote()->getShippingAddress()-> setShippingMethod($data)->save();
				    
				/*
				$result will have erro data if shipping method is empty
				*/
				if(!$result) {
				    Mage::dispatchEvent('checkout_controller_onepage_save_shipping_method', array('request'=>$this->getRequest(), 'quote'=>$this->getOnepage()->getQuote()));
				    $this->getResponse()->setBody(Zend_Json::encode($result));
				        
				    $result['goto_section'] = 'payment';
				    $result['update_section'] = array(
				        'name' => 'payment-method',
				        'html' => $this->_getPaymentMethodsHtml()
				    );

				    //$result['payment_methods_html'] = $this->_getPaymentMethodsHtml();
				}
				$this->getResponse()->setBody(Zend_Json::encode($result));
			}
		} 
    

   /**
     * Checkout page
     */
    public function indexAction()
    {
	/********Modified for redirecting onestepcheckout if session of customer is exists*************/	
	$session = Mage::getSingleton("core/session");
	$arrySession=$session->getData();
	if( !empty($arrySession['cust_number'])){
		$redirectUrl = Mage::getBaseUrl().'onestepcheckout';
		$this->_redirectUrl($redirectUrl);
	}
	
        if (!Mage::helper('checkout')->canOnepageCheckout()) {
            Mage::getSingleton('checkout/session')->addError($this->__('The onepage checkout is disabled.'));
            $this->_redirect('checkout/cart');
            return;
        }
        $quote = $this->getOnepage()->getQuote();
        if (!$quote->hasItems() || $quote->getHasError()) {
            $this->_redirect('checkout/cart');
            return;
        }
        if (!$quote->validateMinimumAmount()) {
            $error = Mage::getStoreConfig('sales/minimum_order/error_message') ?
                Mage::getStoreConfig('sales/minimum_order/error_message') :
                Mage::helper('checkout')->__('Subtotal must exceed minimum order amount');

            Mage::getSingleton('checkout/session')->addError($error);
            $this->_redirect('checkout/cart');
            return;
        }
        Mage::getSingleton('checkout/session')->setCartWasUpdated(false);
        Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::getUrl('*/*/*', array('_secure'=>true)));
        $this->getOnepage()->initCheckout();
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->getLayout()->getBlock('head')->setTitle($this->__('Checkout'));
        $this->renderLayout();
    }
    /**
     * Order success action
     */
    public function successAction()
    {
        $session = $this->getOnepage()->getCheckout();
        if (!$session->getLastSuccessQuoteId()) {
            $this->_redirect('checkout/cart');
            return;
        }

        $lastQuoteId = $session->getLastQuoteId();
        $lastOrderId = $session->getLastOrderId();
        $lastRecurringProfiles = $session->getLastRecurringProfileIds();
        if (!$lastQuoteId || (!$lastOrderId && empty($lastRecurringProfiles))) {
            $this->_redirect('checkout/cart');
            return;
        }
        $session->clear();
        $this->loadLayout();
        $this->_initLayoutMessages('checkout/session');
        Mage::dispatchEvent('checkout_onepage_controller_success_action', array('order_ids' => array($lastOrderId)));
        $this->renderLayout();
    }

    
    /**
     * Save payment ajax action
     *
     * Sets either redirect or a JSON response
     */
    public function savePaymentAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        try {
            if (!$this->getRequest()->isPost()) {
                $this->_ajaxRedirectResponse();
                return;
            }
		
            // set payment to quote
            $result = array();
            $data = $this->getRequest()->getPost('payment', array());
            $result = $this->getOnepage()->savePayment($data);
	
            // get section and redirect data
            $redirectUrl = $this->getOnepage()->getQuote()->getPayment()->getCheckoutRedirectUrl();
            if (empty($result['error']) && !$redirectUrl) {
                $this->loadLayout('checkout_onepage_review');
                $result['goto_section'] = 'review';
                $result['update_section'] = array(
                    'name' => 'review',
                    'html' => $this->_getReviewHtml()
                );
            }
		$checkResource =  Mage::getResourceSingleton('checkout/cart');
		$quoteid = $this->getOnepage()->getQuote()->getId();
		//Insert credit card details -- Redback		
		$checkResource->insertCustomerCreditCardInfo($data,$quoteid);
		$session = Mage::getSingleton("core/session");
		$session->setOffer($data['hdnOfferCatalog']);
		$session->setCustGiftReplacementOne($payment['hdnGiftreplacment1']);
	        $session->setCustGiftReplacementTwo($payment['hdnGiftreplacment2']);
	    	$session->setCustGiftReplacementThree($payment['hdnGiftreplacment3']);

            if ($redirectUrl) {
                $result['redirect'] = $redirectUrl;
            }
        } catch (Mage_Payment_Exception $e) {
            if ($e->getFields()) {
                $result['fields'] = $e->getFields();
            }
            $result['error'] = $e->getMessage();
        } catch (Mage_Core_Exception $e) {
            $result['error'] = $e->getMessage();
        } catch (Exception $e) {
            Mage::logException($e);
            $result['error'] = $this->__('Unable to set Payment Method.');
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * Get Order by quoteId
     *
     * @return Mage_Sales_Model_Order
     */
    protected function _getOrder()
    {
        if (is_null($this->_order)) {
            $this->_order = Mage::getModel('sales/order')->load($this->getOnepage()->getQuote()->getId(), 'quote_id');
            if (!$this->_order->getId()) {
                throw new Mage_Payment_Model_Info_Exception(Mage::helper('core')->__("Can not create invoice. Order was not found."));
            }
        }
        return $this->_order;
    }

    

    /**
     * Create order action
     */
    public function saveOrderAction()
    { 
        if ($this->_expireAjax()) {
            return;
        }
        $result = array();
        try {
            if ($requiredAgreements = Mage::helper('checkout')->getRequiredAgreementIds()) {
                $postedAgreements = array_keys($this->getRequest()->getPost('agreement', array()));
                if ($diff = array_diff($requiredAgreements, $postedAgreements)) {
                    $result['success'] = false;
                    $result['error'] = true;
                    $result['error_messages'] = $this->__('Please agree to all the terms and conditions before placing the order.');
                    $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                    return;
                }
            }
            if ($data = $this->getRequest()->getPost('payment', false)) {
                $this->getOnepage()->getQuote()->getPayment()->importData($data);
            }

			$quote = $this->getOnepage()->getQuote();
			$orderCollection = Mage::getModel('sales/order')->getCollection()
								->addAttributeToFilter('quote_id', $quote->getId())
								->addAttributeToFilter('base_grand_total', $quote->getBaseGrandTotal());
			//If there is an order with the same quote id and amount then show success message and DON'T place the order

			if(count($orderCollection) > 0 ){
				 $rdbOrderNumber = $rdbCustNumber = $rdbZipNumber ='';
				 foreach($orderCollection as $row){
		  			 $rdbOrderNumber = $row['redback_order_number'];//$salesOrder->getRedbackOrderNumber();
					 $rdbCustNumber = $row['redback_cust_num'];//$salesOrder->getRedbackCustNum();
		  			 $rdbZipNumber = $row['redback_cust_zip'];//$salesOrder->getRedbackCustZip();
				 }
				 Mage::getSingleton('core/session')->addSuccess($this->__('Your order has been placed successfully. View your using order #<a href="'.Mage::getBaseUrl().'myorder/index/order/orderId/'.$rdbOrderNumber.'/cn/'.$rdbCustNumber.'/cz/'.$rdbZipNumber.'">'.$rdbOrderNumber.'</a>'));
				 
			  	 $session = $this->getOnepage()->getCheckout();
				 $session->clear();
				 $this->getOnepage()->setDisplaySuccess(true);    
				 $this->_redirect('*/*/success');
			}else {
				$this->getOnepage()->saveOrder();		

				//Create Order Using RedbackAPI
				$checkResource =  Mage::getResourceSingleton('checkout/cart');
				$checkResource->createRedbackOrder();
				//End of create order redback API
			}
            $redirectUrl = $this->getOnepage()->getCheckout()->getRedirectUrl();
            $result['success'] = true;
            $result['error']   = false;
        } catch (Mage_Payment_Model_Info_Exception $e) {
            $message = $e->getMessage();
            if( !empty($message) ) {
                $result['error_messages'] = $message;
            }
            $result['goto_section'] = 'payment';
            $result['update_section'] = array(
                'name' => 'payment-method',
                'html' => $this->_getPaymentMethodsHtml()
            );
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $e->getMessage());
            $result['success'] = false;
            $result['error'] = true;
            $result['error_messages'] = $e->getMessage();

            if ($gotoSection = $this->getOnepage()->getCheckout()->getGotoSection()) {
                $result['goto_section'] = $gotoSection;
                $this->getOnepage()->getCheckout()->setGotoSection(null);
            }

            if ($updateSection = $this->getOnepage()->getCheckout()->getUpdateSection()) {
                if (isset($this->_sectionUpdateFunctions[$updateSection])) {
                    $updateSectionFunction = $this->_sectionUpdateFunctions[$updateSection];
                    $result['update_section'] = array(
                        'name' => $updateSection,
                        'html' => $this->$updateSectionFunction()
                    );
                }
                $this->getOnepage()->getCheckout()->setUpdateSection(null);
            }
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $e->getMessage());
            $result['success']  = false;
            $result['error']    = true;
            $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
        }
        $this->getOnepage()->getQuote()->save();
        /**
         * when there is redirect to third party, we don't want to save order yet.
         * we will save the order in return action.
         */
        if (isset($redirectUrl)) {
            $result['redirect'] = $redirectUrl;
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

	public function logoutAction()
    {
        session_unset ();
        Mage::getSingleton("checkout/session")->unsetAll();
        Mage::getSingleton("core/session")->unsetAll();
        Mage::getSingleton("checkout/session")->clear();
        Mage::getSingleton("core/session")->clear();
        $this->_redirect('/');
    }

	public function giftreplacementAction(){
		echo "giftreplacementAction";exit;
	}
	
	public function keycodeauthenticationAction(){
		echo "keycodeauthenticationAction";exit;
	}
}
