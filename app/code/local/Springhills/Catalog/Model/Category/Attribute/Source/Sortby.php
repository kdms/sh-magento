<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vikasp
 * Date: 6/2/13
 * Time: 11:34 AM
 * To change this template use File | Settings | File Templates.
 */
class Springhills_Catalog_Model_Category_Attribute_Source_Sortby
    extends Mage_Catalog_Model_Category_Attribute_Source_Sortby
{
    public function getAllOptions()
    {
        if (is_null($this->_options)) {
            $this->_options = array(array(
                'label' => Mage::helper('catalog')->__('Position'),
                'value' => 'position'
            ));
            foreach ($this->_getCatalogConfig()->getAttributesUsedForSortBy() as $attribute) {
                $this->_options[] = array(
                    'label' => Mage::helper('catalog')->__($attribute['frontend_label']),
                    'value' => $attribute['attribute_code']
                );
            }
        }
        return $this->_options;
    }
}
