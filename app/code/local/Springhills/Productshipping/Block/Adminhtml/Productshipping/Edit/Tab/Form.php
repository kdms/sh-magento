<?php
class Springhills_Productshipping_Block_Adminhtml_Productshipping_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("productshipping_form", array("legend"=>Mage::helper("productshipping")->__("Item information")));

                $postData = Mage::registry("productshipping_data")->getData();
                //Readonly for Offer_Code in Edit action
                if(count($postData)> 0){
                    $fieldset->addField("Offer_Code", "text", array(
                    "label" => Mage::helper("productshipping")->__("Offer Code"),
                    "class" => "required-entry",
                    "required" => true,
                    "name" => "Offer_Code",
                    "readonly"=>true,
                    ));
                }else{
                    $fieldset->addField("Offer_Code", "text", array(
                        "label" => Mage::helper("productshipping")->__("Offer Code"),
                        "class" => "required-entry",
                        "required" => true,
                        "name" => "Offer_Code",
                    ));
                }
				$fieldset->addField("Order_Low", "text", array(
				"label" => Mage::helper("productshipping")->__("Order Low"),
				"class" => "required-entry validate-number",
				"required" => true,
				"name" => "Order_Low",
				));
				$fieldset->addField("Order_High", "text", array(
				"label" => Mage::helper("productshipping")->__("Order High"),
				"class" => "required-entry validate-number",
				"required" => true,
				"name" => "Order_High",
				));
				$fieldset->addField("Shipping", "text", array(
				"label" => Mage::helper("productshipping")->__("Shipping"),
                "class" => "required-entry validate-number",
                "required" => true,
				"name" => "Shipping",
				));
				$fieldset->addField("Last_Update", "text", array(
				"label" => Mage::helper("productshipping")->__("Last Update"),
				"name" => "Last_Update",
				));
                $fieldset->addField("Catalog_Type", "text", array(
                    "label" => Mage::helper("productshipping")->__("Catalog_Type"),
                    "name" => "Catalog_Type",
                ));
                $fieldset->addField("Type", "text", array(
                    "label" => Mage::helper("productshipping")->__("Type"),
                    "name" => "Type",
                ));
                $fieldset->addField("Percent", "text", array(
                    "label" => Mage::helper("productshipping")->__("Percent"),
                    "name" => "Percent",
                ));

				if (Mage::getSingleton("adminhtml/session")->getProductshippingData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getProductshippingData());
					Mage::getSingleton("adminhtml/session")->setProductshippingData(null);
				} 
				elseif(Mage::registry("productshipping_data")) {
				    $form->setValues(Mage::registry("productshipping_data")->getData());
				}
				return parent::_prepareForm();
		}
}
