<?php

class Springhills_Productshipping_Block_Adminhtml_Productshipping_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("productshippingGrid");
				$this->setDefaultSort("productshipping_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("productshipping/productshipping")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("productshipping_id", array(
				"header" => Mage::helper("productshipping")->__("ID"),
				"align" =>"right",
				"width" => "50px",
				"index" => "productshipping_id",
				));
				$this->addColumn("Offer_Code", array(
				"header" => Mage::helper("productshipping")->__("Offer Code"),
				"align" =>"left",
				"index" => "Offer_Code",
				));

				$this->addColumn("Order_Low", array(
				"header" => Mage::helper("productshipping")->__("Order Low"),
				"align" =>"left",
				"index" => "Order_Low",
				));

				$this->addColumn("Order_High", array(
				"header" => Mage::helper("productshipping")->__("Order High"),
				"align" =>"left",
				"index" => "Order_High",
				));


				$this->addColumn("Shipping", array(
				"header" => Mage::helper("productshipping")->__("Shipping"),
				"align" =>"left",
				"index" => "Shipping",
				));


				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}

}
