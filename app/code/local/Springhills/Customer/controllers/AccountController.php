<?php
require_once 'app/code/core/Mage/Customer/controllers/AccountController.php';
require_once 'redback/RedbackServices.php';
class Springhills_Customer_AccountController extends Mage_Customer_AccountController
{


    /**
     * Default customer account page
     */
    public function indexAction()
    {
        //If session is exists then redirect to onestepcheckout
        $session = Mage::getSingleton("core/session");
        $arrySession = $session->getData();
        if (!empty($arrySession['cust_number'])) {
            $redirectUrl = Mage::getBaseUrl() . 'onestepcheckout';
            $this->_redirectUrl($redirectUrl);
        }
        $this->getLayout()->getBlock('head')->setTitle($this->__('My Account'));
        $this->renderLayout();
    }

    /**
     * Customer login form page
     */
    public function loginAction()
    {
        //If session is exists then redirect to myorder
        $session = Mage::getSingleton("core/session");
        $arrySession = $session->getData();
        if (!empty($arrySession['cust_number'])) {
            $redirectUrl = Mage::getBaseUrl() . 'myorder';
            $this->_redirectUrl($redirectUrl);
        }

        $this->getResponse()->setHeader('Login-Required', 'true');

        $this->loadLayout();

        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();

    }

    /**
     * Login post action
     */
    public function loginPostAction()
    {
        if ($this->_getSession()->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }
        $session = Mage::getSingleton("core/session");
        $arrySession = $session->getData();
        if ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost('login');


            $email = $login['username'];
            $zip = $login['password'];
            $custNo = trim($login['accno']);
            $keyLogin = $login['keyLogin']; // if equal to 1 then only key_code_login enable

            //if the user enters a keycode on the login page, use that (and set to the offer code in session for observer)
            //else check and see if user entered a keycode on the cart page and use it

            $offerCode = '';
            $keycode = $login['keycode'];
            if (!empty($keycode))
                Mage::getSingleton('core/session')->setData("offerCode", $keycode);

            $offerCode = Mage::getSingleton('core/session')->getData("offerCode");


            // For Revolving credit as payment method in expresscheckout
            //REDBACK API CALL
            $objRedBackApi = new RedbackServices();

            if (!empty($offerCode) && !empty($custNo) && empty($arrySession['rev_credit_cust_number']) ) {

                $keyCodeLoginParams = array(
                    "Keycode" => $offerCode,
                    "Title" => '4',
                    "Catalog" => '',
                    "CustNumber" => $custNo,
                    "CustName" => '',
                    "CustFirstName" => '',
                    "CustLastName" => '',
                    "CustCompany" => '',
                    "CustAdd1" => '',
                    "CustAdd2" => '',
                    "CustCity" => '',
                    "CustState" => '',
                    "CustZip" => $zip,
                    "CustPhone" => '',
                    "CustEmail" => '',
                    "CustClubFlag" => '',
                    "CustClubDisc" => '',
                    "CustClubDate" => '',
                    "CustError" => '',
                    "CustMessage" => '',
                    "debug_id" => '',
                    "KeycodeValid" => '',
                    "RevCreditPlan" => '',
                    "InterestRate" => ''
                );

                $responseKeycode = $objRedBackApi->keyCodeRevCreditLoginService($keyCodeLoginParams);
                //print_r($responseKeycode);
                if (empty($responseKeycode['CustError'])) {
                    $session->setEnableRevCreditPlan(1);
                    $session->setRevCreditPlan($responseKeycode['RevCreditPlan']);
                    $session->setKeycodeValid($responseKeycode['KeycodeValid']);
                    $session->setInterestRate($responseKeycode['InterestRate']);
                    $session->setRevCreditCustNumber($responseKeycode['CustNumber']);
                    if(!empty($responseKeycode['CustNumber']))
                        $custNo = $responseKeycode['CustNumber']; // replacing customer number received from key code login response
                }
            }
            // Call to customer master
            $params = array('CustNumber' => $custNo,
                'Title' => '4',
                'CustName' => '',
                'CustAdd1' => '',
                'CustAdd2' => '',
                'CustState' => '',
                'CustCity' => '',
                'CustZip' => $zip,
                'CustCountry' => '',
                'CustPhone' => '',
                'CustFax' => '',
                'CustEmail' => $email,
                'CustPin' => '',
                'CustClubFlag' => '',
                'CustClubDisc' => '',
                'CustClubDate' => '',
                'CustPinHint' => '',
                'CustOptInFlag' => '',
                'CustError' => '',
                'CustMessage' => '',
                'CustOrder' => '',
                'CustFirstName' => '',
                'CustLastName' => '',
                'ClubNumber' => '');

            $responseCustomer = $objRedBackApi->CustMasterAccessEmailZip($params);


            //If Customer error code is zero then successfully logged
            if ($responseCustomer['CustError'] == '0') {
                // set cookie for email subscription
                Mage::getModel('core/cookie')->set('email_id', $email);

                $zipcode = $responseCustomer['CustZip'];
                //if zipcode is more than 5 length then take first 5
                if (strlen($zipcode) > 5) {
                    $zipcode = substr($zipcode, 0, 5);
                }
                $stateCode = $responseCustomer['CustState'];
                if (empty($stateCode)) {
                    $checkResource = Mage::getResourceSingleton('checkout/cart');
                    $stateCode = $checkResource->get_zip_info($zipcode);
                }

                $customerPhone = str_replace(".", "", $responseCustomer['CustPhone']);
                $customerEmail = $responseCustomer['CustEmail'];
                if (empty($customerEmail))
                    $customerEmail = $login['username'];

                $session->setCustNumber($responseCustomer['CustNumber']);
                $session->setCustAdd1($responseCustomer['CustAdd1']);
                $session->setCustAdd2($responseCustomer['CustAdd2']);
                $session->setCustState($stateCode);
                $session->setCustCity($responseCustomer['CustCity']);
                $session->setCustCountry($responseCustomer['CustCountry']);
                $session->setCustPhone($customerPhone);
                $session->setCustFax($responseCustomer['CustFax']);
                $session->setCustEmail($customerEmail);
                $session->setCustPin($zipcode);
                $session->setCustFirstName($responseCustomer['CustFirstName']);
                $session->setCustLastName($responseCustomer['CustLastName']);
                $session->setCustZip($zipcode);
                Mage::getSingleton('core/session')->setData("couponAccount",$responseCustomer['CustNumber']);
                Mage::getSingleton('core/session')->setData("couponZip",$zipcode);
                Mage::getModel('core/cookie')->set('couponZip', $zipcode);
                Mage::getSingleton('core/session')->addSuccess('Welcome ! You are successfully logged in.');
                if (isset($login['action']) && $login['action'] == 'login') {
                    $redirectUrl = Mage::getBaseUrl() . 'myorder';
                } else {

                    if (!empty($offerCode) && !empty($keycode)) { // If user enters keycode from checkout login page
                        $resource = Mage::getResourceSingleton('checkout/cart');
                        $validity = $resource->getCouponValidity($offerCode);
                        if ($validity == 1) {
                            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                            $quoteid = Mage::getSingleton('checkout/session')->getQuote()->getId();
                            $quote = Mage::getModel('sales/quote')->load($quoteid);
                            $subtotal = $quote->getSubtotal();
                            $records = $write->fetchAll("select * from salesrule_coupon where code like '" . $offerCode . "%' order by coupon_id desc ");
                            $count = count($records);
                            $noCoupon = Mage::getModel("checkout/session")->getData("nocoupon");
                            if ($count > 0 && $noCoupon == null) {
                                foreach ($records as $coupon) {
                                    $couponArray = explode("_", $coupon['code']);

                                    if (count($couponArray) == 1) {
                                        $conditions_serialized = $write->fetchOne("select conditions_serialized from salesrule where rule_id = '" . $coupon['rule_id'] . "' ");
                                        $dataArray = unserialize($conditions_serialized);
                                        foreach ($dataArray as $skey => $svalue) {
                                            if ($skey == 'conditions') {
                                                foreach ($svalue as $sv) {
                                                    if ($sv['attribute'] == 'base_subtotal') {
                                                        $rule_base_subtotal = $sv['value'];
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (count($couponArray) > 1) {

                                        $amount = $couponArray[1];
                                        if ($subtotal >= $amount) {
                                            // Applying the new coupon
                                            Mage::getModel("checkout/session")->setData("coupon_code", $coupon['code']);
                                            Mage::getModel('checkout/cart')->getQuote()->setCouponCode($coupon['code'])->save();
                                            Mage::getModel('checkout/cart')->getQuote()->collectTotals();
                                            Mage::getModel('checkout/cart')->getQuote()->save();
                                            Mage::getSingleton('core/session')->addSuccess('Coupon Code "' . $offerCode . '" was applied.');
                                            break;
                                        }
                                    } else {
                                        if ($subtotal >= $rule_base_subtotal) {
                                            // Applying the new coupon
                                            Mage::getModel("checkout/session")->setData("coupon_code", $coupon['code']);
                                            Mage::getModel('checkout/cart')->getQuote()->setCouponCode($coupon['code'])->save();
                                            Mage::getModel('checkout/cart')->getQuote()->collectTotals();
                                            Mage::getModel('checkout/cart')->getQuote()->save();
                                            Mage::getSingleton('core/session')->addSuccess('Coupon Code "' . $offerCode . '" was applied.');
                                        }
                                    }
                                }

                            }
                        }
                    }
                    $redirectUrl = Mage::getBaseUrl() . 'onestepcheckout';
                }
                $this->_redirectUrl($redirectUrl);
            } else {
                //print_r($responseCustomer);die();
                if (!$responseCustomer) {
                    if (isset($login['action']) && $login['action'] == 'login') {
                        Mage::getSingleton('core/session')->addError("We are unable to provide the status of your order at this time.  Please check back in a couple of hours, or contact Customer Service at (513) 354-1509");
                    } else {
                        Mage::getSingleton('core/session')->addError("We're sorry. It looks like there was a problem finding your information at this time. Please feel free to continue your purchase by selecting \"click here to purchase as a guest\" and continuing through the normal checkout process.  We will add this order to your existing account order history.  If you have any questions, feel free to call our customer service at 513-354-1509");
                    }

                } else {
                    Mage::getSingleton('core/session')->addError($responseCustomer['CustMessage']);
                }
                if (isset($login['action']) && $login['action'] == 'login') {
                    $redirectUrl = Mage::getBaseUrl() . 'customer/account/login';
                } else {
                    $redirectUrl = Mage::getBaseUrl() . 'checkout/onepage/';
                }
                $this->_redirectUrl($redirectUrl);

            }

        }

    }

}
