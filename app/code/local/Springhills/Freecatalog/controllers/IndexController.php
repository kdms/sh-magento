<?php
require_once 'redback/RedbackServices.php';
class Springhills_Freecatalog_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {

        $this->loadLayout();
        $this->getLayout()->getBlock("head")->setTitle($this->__("Free Catalog"));
        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home"),
            "title" => $this->__("Home"),
            "link"  => Mage::getBaseUrl()
        ));
        $breadcrumbs->addCrumb("Free Catalog ", array(
            "label" => $this->__("Free Catalog "),
            "title" => $this->__("Free Catalog ")
        ));
        $this->renderLayout();

    }

    public function postAction()
    {
        $post = $this->getRequest()->getPost();
        if ($post) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);
                $error = false;
                if (!Zend_Validate::is(trim($post['name']) , 'NotEmpty')) {
                    $error = true;
                }
                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }
                if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                    $error = true;
                }
                $write =Mage::getSingleton('core/resource')->getConnection('core_write');
                $regionInfo = $write->FetchAll("SELECT * FROM directory_country_region WHERE default_name= '".$post['state']."' and country_id='US'");
                //REDBACK API CALL TO SEND THE EMAIL ID
                $objRedBackApi = new RedbackServices();
                // This is an archaic parameter list
                $data =array(
                    'Title'	=>	'4',
                    'CustNumber'=>'',
                    'CustFirstName'=>trim($post['firstname']),
                    'CustLastName'=>trim($post['lastname']),
                    'CustCompany'=>trim($post['company']),
                    'CustAddr1'=>trim($post['address1']),
                    'CustAddr2'=>trim($post['address2']),
                    'CustCity'=>trim($post['city']),
                    'CustState'=>trim($regionInfo[0]['code']),
                    'CustZip'=>trim($post['zipcode']),
                    'CustEmail'=>trim($post['email'])	,
                    'CustEmIP'=>'',
                    'CustPhone'=>trim($post['phone']),
                    'PromoCode'=>'',
                    'ContestCode'=>'',
                    'OptOutFlag'=>'',
                    'CatErrCode'=>'',
                    'CatErrMsg'=>'',
                    'Bonus'=>'',
                    'debug'=>'',
                    'debug_id'=>'',
                    'BirthMonth'=>'',
                    'BirthDay'=>'',
                    'ConfirmEmail'=>'',
                    'IPaddress'=>$_SERVER["REMOTE_ADDR"],
                    'TimeStamp'=>''
                );

                //$redbackStatus = $objRedBackApi->freecatalogService($data);
                //If Errorcode is empty or Redback dint return false
                $iStatus=0;
                if($redbackStatus)
                    $iStatus=1;

                /* Saving values in database */
                $data['firstname']=trim($post['firstname']);
                $data['lastname']=trim($post['lastname']);
                $data['company']=trim($post['company']);
                $data['address1']=trim($post['address1']);
                $data['address2']=trim($post['address2']);
                $data['city']=trim($post['city']);
                $data['state']=trim($post['state']);
                $data['zipcode']=trim($post['zipcode']);
                $data['country']=trim($post['country']);
                $data['email']=trim($post['email']);
                $data['phone']=trim($post['phone']);
                $data['mobile']=trim($post['mobil']);
                $data['created_at']=date('Y-m-d H:i:s');
                $data['updated_at']=date('Y-m-d H:i:s');
                $data['redback_status']=$iStatus;
                $data['redback_ip']=$_SERVER["REMOTE_ADDR"];
                $model = Mage::getModel('freecatalog/freecatalog');
                $model->setData($data);
                $model->save();
                $is_subscribed = $post['is_subscribed'];
                $write = Mage::getSingleton("core/resource")->getConnection("core_write");
                $count = $write->fetchOne("SELECT count(*) as cnt from  newsletter_subscriber where subscriber_email='".$data['email']."' ");
                //exit;
                 if($count==0 && $is_subscribed ){
                       Mage::getModel('newsletter/subscriber')->subscribe($data['email']);
                 }


                // Transactional Email Template's ID Use template id of template created for free catalog
                $templateId = 2;
                // Set sender information
                $senderName = Mage::getStoreConfig('trans_email/ident_custom1/name');
                $senderEmail = Mage::getStoreConfig('trans_email/ident_custom1/email');
                // $senderName ="Amit";
                // $senderEmail = "amits@kensium.com";
                $sender = array('name' => $senderName,
                    'email' => $senderEmail);
                // Set recepient information
                $recepientEmail = $data['email'];
                $recepientName = $data['firstname'];
                // Get Store ID
                $storeId = Mage::app()->getStore()->getId();
                // Set variables that can be used in email template
                $vars = array('customerEmail' => $data['email']);
                $translate  = Mage::getSingleton('core/translate');
                // Send Transactional Email
                Mage::getModel('core/email_template')
                    ->sendTransactional($templateId, $sender, $recepientEmail, $recepientName, $vars, $storeId);
                $translate->setTranslateInline(true);

                if( $is_subscribed ){
                    Mage::getModel('core/cookie')->set('email_id', $post['email']);
                }

                // set cookie for email subscription

               /* Mage::getModel('core/cookie')->set('firstname', $post['firstname']);
                Mage::getModel('core/cookie')->set('lastname', $post['lastname']);
                Mage::getModel('core/cookie')->set('email_id', $post['email']);

                Mage::getModel('core/cookie')->set('Address', $post['address1']);
                Mage::getModel('core/cookie')->set('Address2', $post['address2']);
                Mage::getModel('core/cookie')->set('City', $post['city']);
                Mage::getModel('core/cookie')->set('State', $post['state']);
                Mage::getModel('core/cookie')->set('ZipCode', $post['zipcode']);*/

               // For free catalog script

                Mage::getSingleton('core/session')->setData('firstname', $post['firstname']);
                Mage::getSingleton('core/session')->setData('lastname', $post['lastname']);
                Mage::getSingleton('core/session')->setData('email_id', $post['email']);
                Mage::getSingleton('core/session')->setData("Address",$post['address1']);
                Mage::getSingleton('core/session')->setData("Address2",$post['address2']);
                Mage::getSingleton('core/session')->setData("City",$post['city']);
                Mage::getSingleton('core/session')->setData("State",$post['state']);
                Mage::getSingleton('core/session')->setData("ZipCode",$post['zipcode']);




                Mage::getSingleton('core/session')->setFormData(false);
                $this->_redirect('freecatalog-thankyou');
                return;

            } catch (Exception $e) {
                $translate->setTranslateInline(true);
                //Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('Unable to submit your request. Please, try again later'));
                //Mage::getSingleton('core/session')->addSuccess('Unable to submit your request. Please, try again later');
                $this->_redirect('*/*/');
                return;
            }

        } else {
            $this->_redirect('*/*/');
        }
    }
}
