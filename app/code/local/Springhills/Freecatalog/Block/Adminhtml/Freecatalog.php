<?php


class Springhills_Freecatalog_Block_Adminhtml_Freecatalog extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

		$this->_controller = "adminhtml_freecatalog";
		$this->_blockGroup = "freecatalog";
		$this->_headerText = Mage::helper("freecatalog")->__("Freecatalog Manager");
		$this->_addButtonLabel = Mage::helper("freecatalog")->__("Add New Item");
		parent::__construct();
		$this->removeButton('add'); 

	}

}
