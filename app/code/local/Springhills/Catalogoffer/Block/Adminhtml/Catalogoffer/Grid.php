<?php

class Springhills_Catalogoffer_Block_Adminhtml_Catalogoffer_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("catalogofferGrid");
				$this->setDefaultSort("catalogoffer_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("catalogoffer/catalogoffer")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("catalogoffer_id", array(
				"header" => Mage::helper("catalogoffer")->__("ID"),
				"align" =>"right",
				"width" => "50px",
				"index" => "catalogoffer_id",
				));
				$this->addColumn("Offer_Code", array(
				"header" => Mage::helper("catalogoffer")->__("Offer Code"),
				"align" =>"left",
				"index" => "Offer_Code",
				));

				$this->addColumn("Offer", array(
				"header" => Mage::helper("catalogoffer")->__("Offer"),
				"align" =>"left",
				"index" => "Offer",
				));

                $this->addColumn("Offer_Date", array(
                    "header" => Mage::helper("catalogoffer")->__("Offer Start Date"),
                    "align" =>"left",
                    "index" => "Offer_Date",
                ));
				$this->addColumn("Offer_Exp_Date", array(
				"header" => Mage::helper("catalogoffer")->__("Offer Exp Date"),
				"align" =>"left",
				"index" => "Offer_Exp_Date",
				));
                $this->addColumn("One_Use", array(
                    "header" => Mage::helper("catalogoffer")->__("One use"),
                    "align" =>"left",
                    "index" => "One_Use",
                ));
                $this->addColumn("Custnbr_Required", array(
                    "header" => Mage::helper("catalogoffer")->__("Customer Number Required"),
                    "align" =>"left",
                    "index" => "Custnbr_Required",
                ));


				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}

}
