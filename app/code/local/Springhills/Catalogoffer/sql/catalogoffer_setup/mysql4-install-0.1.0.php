<?php
$installer = $this;
$installer->startSetup();
$installer->run("CREATE TABLE IF NOT EXISTS `catalog_offers` (
  `catalogoffer_id` int(11) NOT NULL AUTO_INCREMENT,
  `Offer_Code` varchar(100) NOT NULL,
  `Offer_Date` date NOT NULL,
  `Offer_Exp_Date` date NOT NULL,
  `Offer` varchar(100) NOT NULL,
  `Offer_Text` text NOT NULL,
  `Bonus_Item_1` varchar(100) NOT NULL,
  `Bonus_Amount_1` varchar(100) NOT NULL,
  `Bonus_Item_2` varchar(100) NOT NULL,
  `Bonus_Amount_2` varchar(100) NOT NULL,
  `Bonus_Item_3` varchar(100) NOT NULL,
  `Bonus_Amount_3` varchar(100) NOT NULL,
  `Bonus_Item_4` varchar(100) NOT NULL,
  `Bonus_Amount_4` varchar(100) NOT NULL,
  `Coupon_Amt_1` varchar(100) NOT NULL,
  `Coupon_Min_Order_1` varchar(100) NOT NULL,
  `Coupon_Amt_2` varchar(100) NOT NULL,
  `Coupon_Min_Order_2` varchar(100) NOT NULL,
  `Coupon_Amt_3` varchar(100) NOT NULL,
  `Coupon_Min_Order_3` varchar(100) NOT NULL,
  `Coupon_Amt_4` varchar(100) NOT NULL,
  `Coupon_Min_Order_4` varchar(100) NOT NULL,
  `One_Use` varchar(100) NOT NULL,
  `Affiliate` varchar(100) NOT NULL,
  `Free_Ship_Upgrade` varchar(100) NOT NULL,
  `Coupon_Type` varchar(100) NOT NULL,
  `Affiliate_Prog` varchar(100) NOT NULL,
  `Custnbr_Required` varchar(100) NOT NULL,
  `Last_Update` varchar(255) DEFAULT NULL,
  `Catalog_Type` varchar(255) DEFAULT NULL,
  `House_Credit` varchar(100) NOT NULL,
  `OverWeight` varchar(100) NOT NULL,
  `Mailed` varchar(100) NOT NULL,
  `Media_Code` varchar(100) NOT NULL,
  `List_Type` varchar(100) NOT NULL,
  `Media_Type` varchar(100) NOT NULL,
  PRIMARY KEY (`catalogoffer_id`)
);
		");
//demo 
Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 
