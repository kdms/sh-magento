<?php
class Idev_OneStepCheckout_Block_Checkout_Onepage_Link extends Mage_Checkout_Block_Onepage_Link
{
    public function getCheckoutUrl()
    {
        $session = Mage::getSingleton("core/session");
        $arrySession=$session->getData();
        $customerName = $arrySession['cust_first_name'];
        if (!$this->helper('onestepcheckout')->isRewriteCheckoutLinksEnabled() || $customerName == null){
            return parent::getCheckoutUrl();
        }
        return $this->getUrl('onestepcheckout', array('_secure'=>true));
    }
}
