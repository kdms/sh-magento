var MapClient = Class.create();
MapClient.prototype = {
    initialize: function(changeCustomerUrl){
        
        this.changeCustomerUrl = changeCustomerUrl;
       
    },
	
	changeCustomer : function(customerId)
	{
		var url = this.changeCustomerUrl;
		
		url += 'customer_id/' + customerId;

		new Ajax.Updater('map-customer-info',url,{method: 'get', onComplete: function(){updateClientInfo();} ,onFailure: ""}); 	
		
	}
}

function updateClientInfo()
{
	$('name').value = $('map_customer_name').value;
	$('email').value = $('map_customer_email').value;
	$('contact').value = $('map_customer_billing_telephone').value;
	$('customer_id').value = $('map_customer_id').value;
}

function ClientResetForm()
{
	location.href='';
}