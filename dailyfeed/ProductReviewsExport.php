<?php
/**
 * Created by JetBrains PhpStorm.
 * User: amits
 * Date: 7/25/13
 * Time: 3:18 PM
 * To change this template use File | Settings | File Templates.
 */
require_once '/var/www/html/springhill/app/Mage.php';
umask(0);
set_time_limit(0);
Mage::app('default');
global $global_dir_Archive,$global_server_path;
$global_server_path = "http://www.springhillnursery.com/";
$global_dir_Archive = "/var/www/html/springhill/dailyfeed/singlefeed/";
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$query = 'SELECT cpe.entity_id FROM catalog_product_entity as cpe
          INNER JOIN catalog_product_entity_int as cpei on cpe.entity_id=cpei.entity_id
          WHERE cpei.store_id=1 and cpe.sku!="" and cpe.sku not like "%_VSPMAAC" GROUP BY cpe.entity_id';
//$query = 'SELECT entity_id,sku,type_id FROM catalog_product_entity WHERE sku!="" and sku not like "%_VSPMAAC"';
//$query = 'SELECT entity_id,sku,type_id FROM catalog_product_entity WHERE sku="83112"';
$fetchAllProducts = $write->fetchAll($query);
$productReviews= array();
$i=0;

foreach ($fetchAllProducts as $product){
    $prodId = $product['entity_id'];
    $storeId    = Mage::app()->getStore('default')->getId();
    $_Pdetails = Mage::getModel('catalog/product')->load($prodId);
    $reviews = Mage::getModel('review/review')
        ->getResourceCollection()
        ->addStoreFilter($storeId)
        ->addEntityFilter('product', $prodId)
        //->addStatusFilter(Mage_Review_Model_Review::STATUS_APPROVED)
        ->setDateOrder()
        ->addRateVotes();

    /**
     * Getting average of ratings/reviews
     */
    $avg = 0;
    $ratings = array();
    if (count($reviews) > 0) {
        foreach ($reviews->getItems() as $review) {
            if(count($review) >0 ){
                $productReviews[$i]['Sku'] = $_Pdetails->getSku();
                $productReviews[$i]['Product_Name'] = $_Pdetails->getName();
                $productReviews[$i]['Review_Id']= $review->getId();
                $productReviews[$i]['Title']= trim(preg_replace('/\s+/', ' ',$review->getTitle()));
                $productReviews[$i]['Detail']= trim(preg_replace('/\s+/', ' ',$review->getDetail()));
                $productReviews[$i]['Name']= trim(preg_replace('/\s+/', ' ',$review->getNickname()));
                if($review->getStatusId()==1)
                    $productReviews[$i]['Review_Status']= 'Approved';
                elseif($review->getStatusId()==2)
                    $productReviews[$i]['Review_Status']= 'Pending';
                elseif($review->getStatusId()==3)
                    $productReviews[$i]['Review_Status']= 'Not Approved';
                foreach( $review->getRatingVotes() as $vote ) {
                    $productReviews[$i]['Rating'] = $vote->getPercent()/20;
                }
                $productReviews[$i]['CreatedOn']= date('M-d-Y H:i:s',strtotime($review->getCreatedAt()));
                $i++;
               /* if($i==10)
                    break;*/
            }

        }
    }

}

if(count($productReviews)>0){
    try {
        $txFileName = "ProductReviewExport.txt";
        $timestamp = date("Ymd") . "_";
        $dir_Archive = $GLOBALS["global_dir_Archive"];
        $ProductReviewFile = $dir_Archive . $timestamp . $txFileName;
        $file =$timestamp . $txFileName;
        if(file_exists($ProductReviewFile)){
            unlink($ProductReviewFile);
        }
        $fh = fopen($ProductReviewFile, 'a+') or die("can't open file");
        $header ="SKU^Product Name^Review Id^Title^Detail^Name^Review Status^Rating^CreatedOn\n";
        fwrite($fh, $header);
        foreach ($productReviews as $id => $info) {
            $stringData = implode("^", $info) . "\n";
            fwrite($fh, $stringData);
        }
        fclose($fh);
        removeOldFiles($dir_Archive);
        echo '<a href="'.$global_server_path.'dailyfeed/singlefeed/'.$file.'">'.$file."</a>file is exported with all product Reviews";
    } catch (Exception $e) {
        $log_array[0]['Message'] = $e->getMessage();
        $log_array[0]['Trace'] = $e->getTraceAsString();
        $log_array[0]['Info'] = "Something went wrong while exporting data form magento.";
    }
}
function removeOldFiles($dir_Archive){
    //Reading all the files from the archive directory for finding latest price file
    if ($handle = opendir($dir_Archive)) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                //Check for price.txt extension
                if(strpos($entry, 'ProductReviewExport.txt')){
                    $timestamp = str_replace('_ProductReviewExport.txt','',$entry);
                    // get today and last 1 days time
                    $today = time();
                    $OneMonth = $today - (60*60*24*30);
                    $from = date("Ymd", $OneMonth);
                    if($from > $timestamp){
                        unlink($dir_Archive.$entry);
                    }

                }
            }
        }
        closedir($handle);
    }
}



/***END Get reviews of subcategories products ***/
