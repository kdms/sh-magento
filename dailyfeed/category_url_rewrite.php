<?php 
require_once('/var/www/html/springhill/app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$records = $write->fetchAll('select * from catalog_category_entity_varchar where attribute_id = 43 and value in (select value from catalog_category_entity_varchar where attribute_id = 43
group by value having count(value)>1)order by value, entity_id');
echo count($records)."\n";
$category = array();
$disabledCategoryId=array();
foreach($records as $row){
    $entityId = $row['entity_id'];
    $urlKey = $row['value'];
    $category[$urlKey][] = $entityId;
    if(count($category[$urlKey]) > 1){
        $categoryId  =  max($category[$urlKey]);
        $_category = Mage::getModel('catalog/category')->load($categoryId);
        $_category->setUrlKey(' ');
        $_category->setIsActive(0);
        $_category->save();
        $disabledCategoryId[] = $categoryId;
    }
}

$ids = implode("," , $disabledCategoryId );
if(count($disabledCategoryId) > 0 ){
    $mail = new Zend_Mail();
    $mail->setType(Zend_Mime::MULTIPART_RELATED);
    $mail->setBodyHtml('There was problem in category URL rewrites because some of the category having same url key, so we disabled the latest category of all duplicate keys. <br>Ids of disabled categories : '.$ids."<br><br>Curve Commerce Support Team");
    $mail->addTo('ccsupport@kensium.com', 'Sudhakar');
    $mail->setFrom('vikasp@kensium.com', 'Vikas');
    $mail->setSubject('Disabled categories having same url key');
    try {
        if ($mail->send()) {
            //Mage::getSingleton('core/session')->addSuccess('Log Information has been mailed');
        }
    } catch (Exception $ex) {
        //Mage::getSingleton('core/session')->addSuccess('Unable to send mail');
    }
}

?>
