<?php
require_once '/var/www/html/springhill/app/Mage.php';
umask(0000);
Mage::app('default');
include 'web_conf.php';
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
//$query = 'SELECT entity_id,sku,type_id FROM catalog_product_entity WHERE sku!=""';
$query = 'SELECT entity_id,sku,type_id FROM catalog_product_entity WHERE sku!="" and sku not like "%_VSPMAAC" order by entity_id desc';
$fetchAllProducts = $write->fetchAll($query);
$i=0;

foreach($fetchAllProducts as $product){
    $prodId = $product['entity_id'];
    $_Pdetails =Mage::getModel('catalog/product')->load($prodId);
    $productSku = $_Pdetails->getSku()."\n";
    /**** Update All Attributes to Product Details attribute ***/
//$_Pdetails = Mage::getModel('catalog/product')->load('4392');
    if(!empty($productSku)){
        $botanical_name = $_Pdetails->getBotanicalName();
        $form = $_Pdetails->getForm();
        $sun_exposure = $_Pdetails->getSunExposure();
        $height_habit  = stripslashes($_Pdetails->getHeightHabit());
        $spread = stripslashes($_Pdetails->getSpread());
        $spacing  = stripslashes($_Pdetails->getSpacing());
        $hardiness_zones = $_Pdetails->getHardinessZones();
        $foliage_type  = stripslashes($_Pdetails->getFoliageType());
        $flower_form  = $_Pdetails->getFlowerForm();
        $flower_color  = $_Pdetails->getFlowerColor();
        $flowering_date = $_Pdetails->getFloweringDate();
        $planting_requirements  = $_Pdetails->getPlantingRequirements();
        $soil_requirements  = $_Pdetails->getSoilRequirements();
        $growth_rate = $_Pdetails->getGrowthRate();
        $unique_characteristics  = $_Pdetails->getUniqueCharacteristics();
        $additional_information = $_Pdetails->getAdditionalInformation();
        $shipped  = $_Pdetails->getShipped();
        $restricted_states = $_Pdetails->getRestrictedStates();
        $common_name = $_Pdetails->getCommonName();
        $breck_name = $_Pdetails->getBreckName();
        $display_hardiness_zone = $_Pdetails->getDisplayHardinessZone();
        $container = $_Pdetails->getContainer();
        $discontinued = $_Pdetails->getDiscontinued();
        $borders = $_Pdetails->getBorders();
        $deer_resistant = $_Pdetails->getDeerResistant();
        $dried_flowers = $_Pdetails->getDriedFlowers();
        $cut_flowers = $_Pdetails->getCutFlowers();
        $shade  = $_Pdetails->getShade();
        $partial_shade  = $_Pdetails->getPartialShade();
        $indoors_only  = $_Pdetails->getIndoorsOnly();
        $io_in_warm_temp = $_Pdetails->getIoInWarmTemp();
        $ground_covers = $_Pdetails->getGroundCovers();
        $rock_gardens = $_Pdetails->getRockGardens();
        $full_sun  = $_Pdetails->getFullSun();
        $multiplies_annually  = $_Pdetails->getMultipliesAnnually();
        $season_shipped_spring = $_Pdetails->getSeasonShippedSpring();
        $season_shipped_fall  = $_Pdetails->getSeasonShippedFall();
        $permantly_planted_garden  = $_Pdetails->getPermantlyPlantedGarden();
        $good_for_forcing  = $_Pdetails->getGoodForForcing();
        $good_for_naturalizing = $_Pdetails->getGoodForNaturalizing();
        $long_lasting_cut = $_Pdetails->getLongLastingCut();
        $long_lasting_in_garden  = $_Pdetails->getLongLastingInGarden ();
        $blooms_width  = $_Pdetails->getBloomsWidth();
        $average_number_blooms = $_Pdetails->getAverageNumberBlooms();
        $awards  = $_Pdetails->getAwards();
        $family  = $_Pdetails->getFamily();
        $duration   = $_Pdetails->getDuration();
        $light   = $_Pdetails->getLight();
        $bloom_time   = $_Pdetails->getBloomTime();
        $pruning   = $_Pdetails->getPruning();
        $time_of_pruning  = $_Pdetails->getTimeOfPruning();
        $special_care   = $_Pdetails->getSpecialCare();
        $special_care_instructions   = $_Pdetails->getSpecialCareInstructions();
        $type_of_fruit   = $_Pdetails->getTypeOfFruit();
        $web_search_flower_color   = $_Pdetails->getWebSearchFlowerColor();
        $web_search_zone   = $_Pdetails->getWebSearchZone();
        $web_height_search    = $_Pdetails->getWebHeightSearch();
        $web_flowering_time_search  = $_Pdetails->getWebFloweringTimeSearch();
        $light_needed_indoors    = $_Pdetails->getLightNeededIndoors();
        $key_difference_similar_bulbs    = $_Pdetails->getKeyDifferenceSimilarBulbs();
        $root_system    = $_Pdetails->getRootSystem();
        $resistance  = $_Pdetails->getResistance();
        $foliage   = $_Pdetails->getFoliage();
        $growth_rate     = $_Pdetails->getGrowthRate();
        $good_for_other       = $_Pdetails->getGoodForOther();
        $fragrance      = $_Pdetails->getFragrance();
        $usage     = $_Pdetails->getUsage();
        $season      = $_Pdetails->getSeason();
        $shipping_schedule     = $_Pdetails->getShippingSchedule();
        $planting_time      = $_Pdetails->getPlantingTime();
        $watering_requirements     = $_Pdetails->getWateringRequirements();
        $temperature_requirements     = $_Pdetails->getTemperatureRequirements();
        $fertilization_requirements      = $_Pdetails->getFertilizationRequirements();
        $_ProductDetails='';


        if ($botanical_name): $_ProductDetails .= '<p><strong>Botanical Name: </strong>'.stripslashes($botanical_name).'</p>'; endif;

        if ($breck_name): $_ProductDetails .= '<p><strong>Spring Hill Name: </strong>'.stripslashes($breck_name).'</p>'; endif;

        if ($common_name): $_ProductDetails .= '<p><strong>Common Name: </strong>'.stripslashes($common_name).'</p>'; endif;

        if ($family): $_ProductDetails .= '<p><strong>Family: </strong>'.stripslashes($family).'</p>'; endif;

        if ($form): $_ProductDetails .= '<p><strong>Form: </strong>'.stripslashes($form).'</p>'; endif;

        if ($root_system): $_ProductDetails .= '<p><strong>Root System: </strong>'.stripslashes($root_system).'</p>'; endif;

        if ($sun_exposure ): $_ProductDetails .= '<p><strong>Sun Exposure : </strong>';

            if($sun_exposure  == 1) { $_ProductDetails .='Yes'; } else { $_ProductDetails .= 'No'; } $_ProductDetails .= '</p>'; endif;

        if ($light): $_ProductDetails .='<p><strong>Light: </strong>'.stripslashes($light).'</p>'; endif;

        if($full_sun || $partial_shade):
            $_ProductDetails .='<p><strong>Sun Exposure: </strong>';
            if($full_sun && $partial_shade):
                $_ProductDetails .='Partial Shade/Full Sun';
            elseif($full_sun):
                $_ProductDetails .='Full Sun';
            elseif($full_sun):
                $_ProductDetails .='Partial Shade';
                $_ProductDetails .='</p>';
            endif;
        endif;

        if ($shade): $_ProductDetails .='<p><strong>Shade : </strong>';
            if($shade == 1) { $_ProductDetails .='Yes'; }
            else { $_ProductDetails .='No'; }
            $_ProductDetails .='</p>';
        endif;

        if ($height_habit): $_ProductDetails .='<p><strong>Height/Habit: </strong>'.stripslashes($height_habit).'</p>'; endif;

        if ($spread): $_ProductDetails .='<p><strong>Spread: </strong>'.stripslashes($spread).'</p>'; endif;

        if ($spacing): $_ProductDetails .='<p><strong>Spacing: </strong>'.stripslashes($spacing).'</p>'; endif;

        if ($growth_rate): $_ProductDetails .='<p><strong>Growth Rate: </strong>'.stripslashes($growth_rate).'</p>'; endif;

        if ($hardiness_zones): $_ProductDetails .='<p><strong>Hardiness Zones: </strong>'.stripslashes($hardiness_zones).'</p>'; endif;

        if ($permantly_planted_garden):
            $_ProductDetails .='<p><strong>Can be permantly planted in garden: </strong>';
            if($permantly_planted_garden  == 1) { $_ProductDetails .='Yes'; } else { $_ProductDetails .='No'; }
            $_ProductDetails .='</p>';
        endif;

        if ($indoors_only): $_ProductDetails .='<p><strong>Indoors only : </strong>';
            if($indoors_only == 1) { $_ProductDetails .='Yes'; }
            else { $_ProductDetails .='No';}
            $_ProductDetails .='</p>';
        endif;

        if ($io_in_warm_temp): $_ProductDetails .='<p><strong>Indoors/Outdoors in warm temperatures : </strong>';
            if($io_in_warm_temp == 1) { $_ProductDetails .='Yes'; }
            else { $_ProductDetails .='No';}
            $_ProductDetails .='</p>';
        endif;

        if ($light_needed_indoors):  $_ProductDetails .='<p><strong>Light Needed Indoors: </strong>'.stripslashes($light_needed_indoors).'</p>'; endif;

        if ($planting_time): $_ProductDetails .='<p><strong>Planting Time: </strong>'.stripslashes($planting_time).'</p>'; endif;

        if ($bloom_time): $_ProductDetails .='<p><strong>Bloom Time: </strong>'.stripslashes($bloom_time).'</p>'; endif;

        if ($flowering_date): $_ProductDetails .='<p><strong>Flowering Date: </strong>'.stripslashes($flowering_date).'</p>'; endif;

        if ($duration): $_ProductDetails .='<p><strong>Duration: </strong>'.stripslashes($duration).'</p>'; endif;


        if ($blooms_width): $_ProductDetails .='<p><strong>Blooms With: </strong>'.stripslashes($blooms_width).'</p>'; endif;

        if ($flower_color): $_ProductDetails .='<p><strong>Flower Color: </strong>'.stripslashes($flower_color).'</p>'; endif;

        if ($flower_form): $_ProductDetails .='<p><strong>Flower Form: </strong>'.stripslashes($flower_form).'</p>'; endif;

        if ($average_number_blooms): $_ProductDetails .='<p><strong>Average Number of Blooms: </strong>'.stripslashes($average_number_blooms).'</p>'; endif;

        if ($foliage): $_ProductDetails .='<p><strong>Foliage: </strong>'.stripslashes($foliage).'</p>'; endif;

        if ($foliage_type): $_ProductDetails .='<p><strong>Foliage Type: </strong>'.stripslashes($foliage_type).'</p>'; endif;


        if ($type_of_fruit): $_ProductDetails .='<p><strong>Type Of Fruit: </strong>'.stripslashes($type_of_fruit).'</p>'; endif;

        if ($usage): $_ProductDetails .='<p><strong>Usage: </strong>'.stripslashes($usage).'</p>'; endif;

        if ($ground_covers): $_ProductDetails .='<p><strong>Ground Covers: </strong>';
            if($ground_covers == 1) { $_ProductDetails .='Yes'; }
            else { $_ProductDetails .='No'; }
            $_ProductDetails .='</p>';
        endif;

        if ($borders): $_ProductDetails .='<p><strong>Borders : </strong>';
            if($borders == 1) { $_ProductDetails .='Yes'; }
            else { $_ProductDetails .='No';}
            $_ProductDetails .='</p>';
        endif;

        if ($rock_gardens): $_ProductDetails .='<p><strong>Rock Gardens : </strong>';
            if($rock_gardens  == 1) { $_ProductDetails .='Yes'; }
            else { $_ProductDetails .='No';}
            $_ProductDetails .='</p>';
        endif;

        if ($container): $_ProductDetails .='<p><strong>Container: </strong>';
            if($container == 1) { $_ProductDetails .='Yes'; }
            else { $_ProductDetails .='No'; }
            $_ProductDetails .='</p>';
        endif;


        if ($cut_flowers): $_ProductDetails .='<p><strong>Cut Flowers : </strong>';
            if($cut_flowers == 1) { $_ProductDetails .='Yes'; }
            else { $_ProductDetails .='No';}
            $_ProductDetails .='</p>';
        endif;

        if ($dried_flowers): $_ProductDetails .='<p><strong>Dried Flowers : </strong>';
            if($dried_flowers == 1) { $_ProductDetails .='Yes'; }
            else { $_ProductDetails .='No';}
            $_ProductDetails .='</p>';
        endif;

        if ($good_for_naturalizing): $_ProductDetails .='<p><strong>Good For Naturalizing: </strong>';
            if($good_for_naturalizing == 1) { $_ProductDetails .='Yes'; }
            else { $_ProductDetails .='No';}
            $_ProductDetails .='</p>';
        endif;


        if ($good_for_forcing): $_ProductDetails .='<p><strong>Good For Forcing: </strong>';
            if($good_for_forcing == 1) { $_ProductDetails .='Yes'; }
            else { $_ProductDetails .='No';}
            $_ProductDetails .='</p>';
        endif;


        if ($good_for_other): $_ProductDetails .='<p><strong>Good For Other: </strong>'.stripslashes($good_for_other).'</p>'; endif;

        if ($fragrance): $_ProductDetails .='<p><strong>Fragrance: </strong>'.stripslashes($fragrance).'</p>'; endif;

        if ($resistance): $_ProductDetails .='<p><strong>Resistance: </strong>'.stripslashes($resistance).'</p>'; endif;

        if ($multiplies_annually): $_ProductDetails .='<p><strong>Multiplies Annually: </strong>';
            if($multiplies_annually == 1) { $_ProductDetails .='Yes'; }
            else { $_ProductDetails .='No'; }
            $_ProductDetails .='</p>';
        endif;

        if ($long_lasting_in_garden): $_ProductDetails .='<p><strong>Long Lasting In Garden: </strong>';
            if($long_lasting_in_garden == 1) { $_ProductDetails .='Yes'; }
            else { $_ProductDetails .='No';}
            $_ProductDetails .='</p>';
        endif;


        if ($long_lasting_cut): $_ProductDetails .='<p><strong>Long Lasting Cut: </strong>';
            if($long_lasting_cut == 1) { $_ProductDetails .='Yes'; }
            else { $_ProductDetails .='No';} $_ProductDetails .='</p>';
        endif;

        if ($planting_requirements): $_ProductDetails .='<p><strong>Planting Requirements: </strong>'.stripslashes($planting_requirements).'</p>'; endif;

        if ($soil_requirements): $_ProductDetails .='<p><strong>Soil Requirements: </strong>'.stripslashes($soil_requirements).'</p>'; endif;

        if ($fertilization_requirements): $_ProductDetails .='<p><strong>Fertilization Requirements: </strong>'.stripslashes($fertilization_requirements).'</p>'; endif;

        if ($watering_requirements): $_ProductDetails .='<p><strong>Watering Requirements: </strong>'.stripslashes($watering_requirements).'</p>'; endif;


        if ($temperature_requirements): $_ProductDetails .='<p><strong>Temperature Requirements: </strong>'.stripslashes($temperature_requirements).'</p>'; endif;

        // if ($winter_requirement): $_ProductDetails .='<p><strong>Winter Requirements: </strong>'.stripslashes($winter_requirement).'</p>'; endif;

        if ($special_care_instructions): $_ProductDetails .='<p><strong>Special care or planting instructions: </strong>'.stripslashes($special_care_instructions).'</p>'; endif;

        if ($special_care): $_ProductDetails .='<p><strong>Special Care: </strong>'.stripslashes($special_care).'</p>'; endif;

        if ($pruning): $_ProductDetails .='<p><strong>Pruning: </strong>'.stripslashes($pruning).'</p>'; endif;

        if ($time_of_pruning): $_ProductDetails .='<p><strong>Time Of Pruning: </strong>'.stripslashes($time_of_pruning).'</p>'; endif;

        // if ($two_unique_characteristic): $_ProductDetails .='<p><strong>2unique Characteristics: </strong>'.stripslashes($two_unique_characteristic).'</p>'; endif;

        if ($additional_information): $_ProductDetails .='<p><strong>Additional Information: </strong>'.stripslashes($additional_information).'</p>'; endif;

        if ($shipped): $_ProductDetails .='<p><strong>Shipped: </strong>'.stripslashes($shipped).'</p>'; endif;
        $_ProductDetails .='<p> <strong> Shipping: </strong> <a href="'.Mage::getBaseUrl().'shipping-schedule">View Shipping Schedule</a>';
        if($season_shipped_fall || $season_shipped_spring):
            $_ProductDetails .='<br>';
            if($season_shipped_fall == '1' && $season_shipped_spring == '1'):
                $_ProductDetails .='This item ships in both Spring and Fall';
            elseif($season_shipped_fall == '1' && $season_shipped_spring == '0'):
                $_ProductDetails .='This item ships in Fall';
            elseif($season_shipped_fall == '0' && $season_shipped_spring == '1'):
                $_ProductDetails .='This item ships in Spring';
            endif;
        endif;
        $_ProductDetails .='</p>';

        if ($restricted_states): $_ProductDetails .='<p><strong>Restricted States: </strong>'.stripslashes($restricted_states).'</p>'; endif;

        if ($awards): $_ProductDetails .='<p><strong>Awards: </strong>'.stripslashes($awards).'</p>'; endif;


        if ($web_flowering_time_search && 0): $_ProductDetails .='<p><strong>Web Flowering Time Search: </strong>'.stripslashes($web_flowering_time_search).'</p>'; endif;

        if ($web_height_search && 0): $_ProductDetails .='<p><strong>Web Height Search: </strong>'.stripslashes($web_height_search).'</p>'; endif;

        if ($web_search_flower_color && 0): $_ProductDetails .='<p><strong>Web Search Flower Color: </strong>'.stripslashes($web_search_flower_color).'</p>'; endif;

        /*if ($lifetime_guarantee): $_ProductDetails .='<p><strong>Lifetime Guarantee : </strong>';
            if($lifetime_guarantee == 1) { $_ProductDetails .='Yes'; }
            else { $_ProductDetails .='No' ;}
            $_ProductDetails .='</p>';
        endif;*/

        if ($discontinued): $_ProductDetails .='<p><strong>Discontinued/Obsolete : </strong>';
            if($discontinued == 1) { $_ProductDetails .='Yes'; }
            else { $_ProductDetails .='No';}
            $_ProductDetails .='</p>';
        endif;

        if ($sun_exposure): $_ProductDetails .='<p><strong>Sun Exposure: </strong>'.stripslashes($sun_exposure).'</p>'; endif;

        if ($key_difference_similar_bulbs): $_ProductDetails .='<p><strong>Key difference from similar bulbs: </strong>'.stripslashes($key_difference_similar_bulbs).'</p>'; endif;

        if ($shipping_schedule): $_ProductDetails .='<p><strong>Shipping Schedule: </strong>'.stripslashes($shipping_schedule).'</p>'; endif;

        $_ProductDetails .='<span itemprop="brand" style="display:none;">Springhill</span>';
        if(!empty($strCategories)) {
            $_ProductDetails .='<span itemprop="category" style="display:none;">'.$strCategories.'</span>';
        }
        $_ProductDetails .='<div style="float:left; clear:left;">';
        if ($unique_characteristics):
            $_ProductDetails .='<p><strong>Unique Characteristics: </strong>'.stripslashes($unique_characteristics).'</p>';
        endif;
        $_ProductDetails .='</div>';
        // echo $_ProductDetails;
        if ($_ProductDetails) {
            $_Pdetails->setProductDetails($_ProductDetails);
            $flag = 1;
        }

        if ($_Pdetails->getId()) {
            try {

                //$_Pdetails->save();
                if ($flag == 1) {
                    $arrayProduct = array(
                        "product_details" => $_ProductDetails
                        );
                    $productId =$_Pdetails->getId();
                    $client->call($session_id, 'catalog_product.update', array($productId, $arrayProduct));
                    print $i++."===Updated Product SKU----->".$productSku."\n";
                }
            }  catch (Exception $e) {
                echo 'Exception1---->: ' . $e->getMessage() . "\n";
            }
        }
    }

}
?>
