<?php

$log_array = array();
require_once '../app/Mage.php';
umask(0);
Mage::app('default');
//include 'web_conf.php';


$write = Mage::getSingleton('core/resource')->getConnection('core_write');

$InventoryStatus = $write->fetchAll("SELECT status FROM import_notification_history 
    WHERE import_type='inventoryFeed' or import_type='inventory' or import_type='catalogOffers'");

if ($InventoryStatus[0]['status'] != "inProgress" &&
        $InventoryStatus[1]['status'] != "inProgress" &&
        $InventoryStatus[2]['status'] != "inProgress") {
//Mail to be sent for
    //$arrTo[] = array("toEmail" => "amitblues123@gmail.com", "toName" => "imsupport");
    $arrTo[] = array("toEmail" => "mohansaip@kensium.com", "toName" => "Janesh");
//This URL IS USED FOR BATCH PROCESSING

    $CronURL = '../dailyfeed/PriceUpdateCron.php';

//Semaphore Flag
    $txtFlagFile = '../dailyfeed/import/UploadComplete.txt';

//Directory path where import files are located
    $dir_import = "../dailyfeed/import/";

//Directory Path where Import files to be moved after successful import
    $dir_Archive = "../dailyfeed/archive/";

    $txFileName = "Price.txt";

    $timestamp = date("YmdHis") . "_";

    $txtArchivePrice = $dir_Archive . $timestamp . $txFileName;

    $tableName = "Price";

    $txtPriceFile = $dir_import . $txFileName;

    $write = Mage::getSingleton('core/resource')->getConnection('core_write');

    $InventoryId = $write->fetchOne("SELECT id FROM import_notification_history WHERE import_type='price' and status='notStarted'");

//Checking flag file and then proceeding for Inventory Update

    if (file_exists($txtFlagFile) && file_exists($txtPriceFile) && ((!isset($argv[1]) && !empty($InventoryId)) || (isset($argv[1]) && empty($InventoryId)))) {
        $limit = 10000;
        $start = 0;
        //Get the Start Index for Import
        if (!empty($argv[1]))
            $start = $argv[1];
        else {
            $start = 0;
        }
//Saving the lastupdated time in database for 36 hours email notification
        $InventoryFeedId = $write->fetchOne("SELECT id FROM import_notification_history WHERE import_type='price'");
        if (empty($InventoryFeedId)) {
            $write->query("INSERT INTO import_notification_history(import_type,status) VALUES('price','inProgress')");
        } else {
            $write->query("UPDATE import_notification_history SET status='inProgress' WHERE id=" . $InventoryFeedId);
        }

//Get the limit to be imported by default 100        

        /*
         * Update Inventory values with Product info
         */

        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');

        $totalResults = $connection->select()
                ->from('Price', array('count(*) as count'));
        $totalResultsCount = $connection->fetchAll($totalResults);

        if (file_exists($txtPriceFile) && empty($argv[1])) {
            try {
                $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                $write->delete($tableName);

                $write->query("LOAD DATA LOCAL INFILE '" . $txtPriceFile . "' INTO TABLE " . $tableName . "
FIELDS TERMINATED BY '|' ENCLOSED BY '' 
LINES TERMINATED BY '\n' STARTING BY ''");
                $write->query("DELETE FROM " . $tableName . " where Price='Price'");
            } catch (Exception $e) {
                $log_array[0]['Message'] = $e->getMessage();
                $log_array[0]['Trace'] = $e->getTraceAsString();
                $log_array[0]['Info'] = "Something went wrong while importing from " . $txFileName . " file.";
            }
        }
        $totalResults1 = "";

        $totalResults1 = $connection->select()
                ->from('Price as P', array('count(*) as count', 'P.Product_Number', 'P.Price'))
                ->join("catalog_product_entity AS cpe", "cpe.sku = P.Product_Number", array())
                ->where('P.Offer_Code=4')
                //->where('P.Product_Number IN (80357,80480,80411,80482,80401,80485,80432,80491,84415,64975,82517,80412,80418,80395,66738,21501)')
                ->group('P.Product_Number');

        $totalResultsCount1 = $connection->fetchAll($totalResults1);

        if ($start < count($totalResultsCount1)) {
            if ($start == 0) {
                $priceSelect = $connection->select()
                        ->from('Price as P', array('count(*) as count', 'P.Product_Number', 'P.Price'))
                        ->join("catalog_product_entity AS cpe", "cpe.sku = P.Product_Number", array())
                        ->where('P.Offer_Code=4')
                        // ->where('P.Product_Number IN (80357,80480,80411,80482,80401,80485,80432,80491,84415,64975,82517,80412,80418,80395,66738,21501)')
                        ->group('P.Product_Number')
                        ->limit($limit);
            } else {
                $priceSelect = $connection->select()
                        ->from('Price as P', array('count(*) as count', 'P.Product_Number', 'P.Price'))
                        ->join("catalog_product_entity AS cpe", "cpe.sku = P.Product_Number", array())
                        ->where('P.Offer_Code=4')
                        // ->where('P.Product_Number IN (80357,80480,80411,80482,80401,80485,80432,80491,84415,64975,82517,80412,80418,80395,66738,21501)')
                        ->group('P.Product_Number')
                        ->limit($start, $limit);
            }

            $priceRowsArray = $connection->fetchAll($priceSelect);
            $UomQty = 0;
            $error_inc = 1;
            echo $res_cnt = count($priceRowsArray);
            $inc_val = 0;
	    $i=1;
            foreach ($priceRowsArray as $pKey => $pValue) {

                echo $sku = $pValue['Product_Number'];
		echo "===".$i++."======";
                $product_id = Mage::getModel('catalog/product')->getIdBySku("$sku");
                $offersku = $sku . "_VSPMAAC";
                //$_Pdetails_offer = Mage::getModel('catalog/product')->loadByAttribute('sku', $offersku);





                $productdataVal = Mage::getModel('catalog/product')->load($product_id);
                echo $psta = $productdataVal->isSaleable()."######".$product_id."\n";
                if ($psta != 1 && $product_id == 5283) {
                    echo "test"; die();

                }
            }
            //print_r($log_array);
            $start = $start + $limit;
            exec('php PriceUpdateCron.php ' . $start . ' ' . $limit);
        } else {
            $error_inc1 = $res_cnt + 1;
            try {
                if (copy($txtPriceFile, $txtArchivePrice)) {
                    //unlink($txtPriceFile);
                }
            } catch (Exception $e) {
                $log_array[$error_inc1 + 1]['Message'] = $e->getMessage();
                $log_array[$error_inc1 + 1]['Trace'] = $e->getTraceAsString();
                $log_array[$error_inc1 + 1]['Info'] = "Something went wrong while archive the " . $tableName . " file.";
            }

            try {
                if ((count(scandir($dir_import)) == 3)) {
//                $ourFileName = $txtFlagFile;
//                $ourFileHandle = fopen($ourFileName, 'w') or die("can't open file");
//                fclose($ourFileHandle);
                    unlink($txtFlagFile);
                }
            } catch (Exception $e) {
                $log_array[$error_inc1 + 2]['Message'] = $e->getMessage();
                $log_array[$error_inc1 + 2]['Trace'] = $e->getTraceAsString();
                $log_array[$error_inc1 + 2]['Info'] = "Something went wrong while removing flag file.";
            }
            try {
                /* $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                  $write->delete($tableName); */
                if (empty($start))
                    $a = "empty";
                else
                    $a=$start;
                $write->query("UPDATE import_notification_history SET status='notStarted', end_date='" . date("Y-m-d H:i:s") . "' WHERE import_type='price' and status='inProgress'");
            } catch (Exception $e) {
                $log_array[$error_inc1 + 3]['Message'] = $e->getMessage();
                $log_array[$error_inc1 + 3]['Trace'] = $e->getTraceAsString();
                $log_array[$error_inc1 + 3]['Info'] = "Something went wrong while deleting the records from " . $tableName . " table.";
            }
        }
//DELETE THE ARCHIVE FILES OLDER THAN 30 Days
        if ($handle = opendir($dir_Archive)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    $arrFiles = explode("_", $entry);

                    $fileTimeStamp = $arrFiles[0];
                    $date1 = date('YmdHis');
                    $date2 = $fileTimeStamp;

                    $ts1 = strtotime($date1);
                    $ts2 = strtotime($date2);
                    $seconds_diff = $ts1 - $ts2;
                    $days = floor($seconds_diff / 3600 / 24);
                    try {
//Delete the files older than 30 days
                        if ($days > 30) {
                            unlink($dir_Archive . $entry);
                        }
                    } catch (Exception $e) {
                        $log_array[$error_inc1 + 4]['Message'] = $e->getMessage();
                        $log_array[$error_inc1 + 4]['Trace'] = $e->getTraceAsString();
                        $log_array[$error_inc1 + 4]['Info'] = "Something went wrong while deleting 30 days old archive files.";
                    }
                }
            }
            closedir($handle);
        }
//ERROR LOG
//ERROR LOG
//        print "<pre>";
//        print_r($log_array);
        if (count($log_array) > 0) {
            $errorMsg = '';
            foreach ($log_array as $log) {
                $errorMsg .= '
          - - - - - - - -
          ERROR
          SUBJECT: ' . $log['Info'] . '
          Message: ' . $log['Message'] . '
          Trace: ' . $log['Trace'] . '
          ProductId: ' . $log['sku'] . '
          Time: ' . date('Y m d H:i:s') . '
          ';
                echo "\r\n" . $errorMsg;
            }
            file_put_contents($dir_Archive . '/log.txt', $errorMsg);
            //Getting admin Information
            $adminInfo = $write->fetchAll("SELECT * from admin_user");
            $userFirstname = $adminInfo[0]['firstname'];
            $userLastname = $adminInfo[0]['lastname'];
            $userEmail = $adminInfo[0]['email'];
            $arrTo[] = array("toEmail" => $userEmail, "toName" => $userFirstname . ' ' . $userLastname);

            $mail = new Zend_Mail();
            $mail->setType(Zend_Mime::MULTIPART_RELATED);
            $mail->setBodyHtml('There were errors updating the product database for Spring Hill. Please find the attached log file(s).');
            $mail->setFrom($userEmail, $userFirstname . ' ' . $userLastname);
            //SENDING MAIL TO Janesh,imsupport@gardensalive.com and Magento Admin
            foreach ($arrTo as $to) {
                $mail->addTo($to['toEmail'], $to['toName']);
            }
            $mail->setSubject('Error updating SH product database');
            $fileContents = file_get_contents($dir_Archive . '/log.txt');
            $file = $mail->createAttachment($fileContents);
            $file->filename = "log.txt";
            try {
                if ($mail->send()) {
                    Mage::getSingleton('core/session')->addSuccess('Log Information has been mailed');
                }
            } catch (Exception $ex) {
                Mage::getSingleton('core/session')->addSuccess('Unable to send mail');
            }
        }
    }
} else {
    
}




    
