<?php
require_once '/var/www/html/springhill/app/Mage.php';
umask(0);
Mage::app('default');
global $global_dir_Archive;
global $global_server_path, $global_server_image_path;
$global_server_path = "http://www.springhillnursery.com/";
$global_dir_Archive = "/var/www/html/springhill/dailyfeed/singlefeed/";
$global_server_image_path = $global_server_path."media/catalog/product";
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
//$query = 'SELECT entity_id,sku,type_id FROM catalog_product_entity WHERE sku!=""';
$query = 'SELECT cpe.entity_id FROM catalog_product_entity as cpe
          INNER JOIN catalog_product_entity_int as cpei on cpe.entity_id=cpei.entity_id
          WHERE cpei.store_id=1 and cpe.sku!="" and cpe.sku not like "%_VSPMAAC" GROUP BY cpe.entity_id';
$fetchAllProducts = $write->fetchAll($query);
$i=0;
$ProductInfo =array();
//echo count($fetchAllProducts);exit;
foreach($fetchAllProducts as $product){
    $prodId = $product['entity_id'];
    $_Pdetails =Mage::getModel('catalog/product')->load($prodId);
	$productSku = $_Pdetails->getSku();
    if(!empty($productSku) && $productSku!='product_sku'){        
        $ProductInfo[$i]['SKU']=$productSku;
        $ProductInfo[$i]['Name']=$_Pdetails->getName();
        $ProductInfo[$i]['Short_Description']=trim(preg_replace('/\s+/', ' ', html_entity_decode($_Pdetails->getShortDescription())));
        $ProductInfo[$i]['Description']=trim(preg_replace('/\s+/', ' ', html_entity_decode($_Pdetails->getDescription())));
        $ProductInfo[$i]['Keywords']=$_Pdetails->getSearchWords();
        // $ProductInfo[$i]['Status']=$_Pdetails->getStatus();
	    if($_Pdetails->isSaleable() && ($_Pdetails->getFutureSeasonMessage() > 1 || $_Pdetails->getFutureSeasonMessage()=='')){
            $ProductInfo[$i]['Status']=1;
        }else{
            $ProductInfo[$i]['Status']=2;
        }
        echo "count===".$i."==SKU===".$productSku."\n";
    }
    $i++;
}


if(count($ProductInfo)>0){
    try {
        $txFileName = "NewProductExport.txt";
        $timestamp = date("Ymd") . "_";
        $dir_Archive = $GLOBALS["global_dir_Archive"];
        $ProductFile = $dir_Archive . $timestamp . $txFileName;
        $file =$timestamp . $txFileName;
        if(file_exists($ProductFile)){
            unlink($ProductFile);
        }
        $fh = fopen($ProductFile, 'a+') or die("can't open file");
        $header ="SKU^Name^Short Description^Long Description^Keywords^Item Status\n";
        fwrite($fh, $header);
        foreach ($ProductInfo as $id => $info) {
             $stringData = implode("^", $info) . "\n";
             fwrite($fh, $stringData);
        }
        fclose($fh);
        removeOldFiles($dir_Archive);
        //echo $ProductFile."file is exported with all product details";
        echo '<a href="'.$global_server_path.'dailyfeed/archive/'.$file.'">'.$file."</a>file is exported with all product details";
    } catch (Exception $e) {
        $log_array[0]['Message'] = $e->getMessage();
        $log_array[0]['Trace'] = $e->getTraceAsString();
        $log_array[0]['Info'] = "Something went wrong while exporting data form magento.";
    }
}

function removeOldFiles($dir_Archive){
    //Reading all the files from the archive directory for finding latest price file
    if ($handle = opendir($dir_Archive)) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
                //Check for price.txt extension
                if(strpos($entry, 'NewProductExport.txt')){
                    $timestamp = str_replace('_NewProductExport.txt','',$entry);
                    // get today and last 1 days time
                    $today = time();
                    $OneMonth = $today - (60*60*24*30);
                    $from = date("Ymd", $OneMonth);
                    if($from > $timestamp){
                        unlink($dir_Archive.$entry);
                    }

                }
            }
        }
        closedir($handle);
    }
}


