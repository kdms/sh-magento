<?php

$num_items = 2000;
$log_array = array();
require_once '../app/Mage.php';
umask(0);
Mage::app('default');
include 'web_conf.php';




$write = Mage::getSingleton('core/resource')->getConnection('core_write');
if (!empty($_GET['start']))
    $start = $_GET['start'];
else
    $start=0;

//$info = $write->fetchAll("SELECT * FROM product_description_missing order by File ASC");
$info = $write->fetchAll("SELECT * FROM product_description_temp where item_number != '' order by item_number");
$store = "admin";
$websites = array(1);
$has_options = 0;
// 						//No Layout updates
$page_layout = 'No Layout updates';
// 						//Block after info colomn
$options_container = 'container2';
// 						//Manufacturer's Suggested Retail Price
$msrp = '';
// 						//Use config
$msrp_enabled = '2';
// 						//Use config
$msrp_display_actual_price_type = '4';
// 						$gift_message_available  = 'No';
// 						$gift_wrapping_available = 'No';
$is_returnable = '2';
// 						$custom_layout_update  = '';
// 						//No
$is_recurring = '0';
$visibility = '4';
$enable_googlecheckout = 1;
//Taxable Goods
$tax_class_id = '2';
$meta_keyword = '';
$meta_title = "";
$weight = '1.0';
//Product description
$recommendation_url = '';
$a = 0;
$i=0;

$sku_array=array(73182,73183,64844,64850,76506,79989);

foreach ($info as $row) {
    $product_skus = explode(",", $row['Item_Number']);

    if (count($product_skus) > 0) {
        $config = 1;
        foreach ($product_skus as $sku) {

            if(!in_array($sku,$sku_array)) continue;
			//echo $a++."######".$sku."\n";
            try{
                           
            if ($config == 0) {
                $config_sku = $sku;
                $type = 'configurable';
            }
            else
                $type = 'simple';


            if (!empty($sku)) {
               try {
                $product_info = $write->fetchAll("SELECT `name`,`option_name`,`description`,`detailed_description` FROM `SPRG_products_site` WHERE `product_number`=" . $sku);
                
                // echo "SELECT `name`,`option_name`,`description`,`detailed_description` FROM `SPRG_products_site` WHERE `product_number`=".$sku;exit;
                //echo '<pre>';print_r($product_info);



                $arrayProduct = array();
                $image = "http://springhillnursery.com/images/250/$sku.jpg";
                if ($sku) {
                    if (!file_exists("/var/www/springhill/media/import/" . $sku . ".jpg")) {
                        @copy($image, "/var/www/springhill/media/import/" . $sku . ".jpg");
                        if (!file_exists($image)) {
                            $image = '/noimage.gif';
                        } else {
                            $image = $sku . '.jpg';
                        }
                    } else {
                        $image = $sku . '.jpg';
                    }
                }

                $createArray = array(
                    "store" => $store,
                    "websites" => $websites,
                    "has_options" => $has_options,
                    "page_layout" => $page_layout,
                    "msrp" => $msrp,
                    "msrp_enabled" => $msrp_enabled,
                    "msrp_display_actual_price_type" => $msrp_display_actual_price_type,
                    "is_returnable" => $is_returnable,
                    "is_recurring" => $is_recurring,
                    "visibility" => $visibility,
                    "enable_googlecheckout" => $enable_googlecheckout,
                    "tax_class_id" => $tax_class_id,
                    "weight" => $weight,
                    "recommendation_url" => $recommendation_url,
                    "meta_keyword" => $meta_keyword,
                    "meta_title" => $meta_title,
                    "meta_description" => '',
                    "news_from_date" => '',
                    "news_to_date" => '',
                    "required_options" => '0',
                    "related_tgtr_position_limit" => '',
                    "related_tgtr_position_behavior" => '',
                    "upsell_tgtr_position_limit" => '',
                    "upsell_tgtr_position_behavior" => '',
                    "is_new_plant" => '0',
                    "is_web_only_plant" => '0',
                    "is_on_clearance" => '0',
                    "best_seller_indicator" => '0',
                    "lifetime_guarantee" => '0',
                    "special_price" => '',
                    "special_from_date" => '',
                    "special_to_date" => '',
                    "minimal_price" => '',
                    "tier_price" => array(),
                    "recurring_profile" => '',
                    "custom_design" => '',
                    "custom_design_from" => '',
                    "custom_design_to" => '',
                    "custom_layout_update" => '',
                    "options_container" => $options_container,
                    "gift_message_available" => '0',
                    "gift_wrapping_available" => '0',
                    "gift_wrapping_price" => '',
                    "status" => '1',
                    "price" => 0
                );

                //@todoBuilding Product array to be updated or created
                if ($row['Unique'] == 'TRUE') {
                    $arrPlant = array('planting_requirements' => 1);
                    $arrayProduct = array_merge($arrayProduct, $arrPlant);
                } elseif ($row['Unique'] == 'FALSE') {
                    $arrPlant = array('planting_requirements' => 0);
                    $arrayProduct = array_merge($arrayProduct, $arrPlant);
                }

                if ($row['SHADE'] == 'TRUE') {
                    $shade = array('shade' => 1);
                    $arrayProduct = array_merge($arrayProduct, $shade);
                } elseif ($row['SHADE'] == 'FALSE') {
                    $shade = array('shade' => 0);
                    $arrayProduct = array_merge($arrayProduct, $shade);
                }

                if ($row['PARTIAL_SHADE'] == 'TRUE') {
                    $partial_shade = array('partial_shade' => 1);
                    $arrayProduct = array_merge($arrayProduct, $partial_shade);
                } elseif ($row['PARTIAL_SHADE'] == 'FALSE') {
                    $partial_shade = array('partial_shade' => 0);
                    $arrayProduct = array_merge($arrayProduct, $partial_shade);
                }

                if ($row['FULL_SUN'] == 'TRUE') {
                    $full_sun = array('full_sun' => 1);
                    $arrayProduct = array_merge($arrayProduct, $full_sun);
                } elseif ($row['FULL_SUN'] == 'FALSE') {
                    $full_sun = array('full_sun' => 0);
                    $arrayProduct = array_merge($arrayProduct, $full_sun);
                }

                if ($row['BORDERS'] == 'TRUE') {
                    $borders = array('borders' => 1);
                    $arrayProduct = array_merge($arrayProduct, $borders);
                } elseif ($row['BORDERS'] == 'FALSE') {
                    $borders = array('borders' => 0);
                    $arrayProduct = array_merge($arrayProduct, $borders);
                }

                if ($row['ROCK_GARDENS'] == 'TRUE') {
                    $rock_gardens = array('rock_gardens' => 1);
                    $arrayProduct = array_merge($arrayProduct, $rock_gardens);
                } elseif ($row['ROCK_GARDENS'] == 'FALSE') {
                    $rock_gardens = array('rock_gardens' => 0);
                    $arrayProduct = array_merge($arrayProduct, $rock_gardens);
                }

                if ($row['GROUND_COVERS'] == 'TRUE') {
                    $ground_covers = array('ground_covers' => 1);
                    $arrayProduct = array_merge($arrayProduct, $ground_covers);
                } elseif ($row['GROUND_COVERS'] == 'FALSE') {
                    $ground_covers = array('ground_covers' => 0);
                    $arrayProduct = array_merge($arrayProduct, $ground_covers);
                }

                if ($row['Long_Lasting_in_Garden'] == 'TRUE') {
                    $long_lasting_in_garden = array('long_lasting_in_garden' => 1);
                    $arrayProduct = array_merge($arrayProduct, $long_lasting_in_garden);
                } elseif ($row['Long_Lasting_in_Garden'] == 'FALSE') {
                    $long_lasting_in_garden = array('long_lasting_in_garden' => 0);
                    $arrayProduct = array_merge($arrayProduct, $long_lasting_in_garden);
                }

                if ($row['CUT_FLOWERS'] == 'TRUE') {
                    $cut_flowers = array('cut_flowers' => 1);
                    $arrayProduct = array_merge($arrayProduct, $cut_flowers);
                } elseif ($row['CUT_FLOWERS'] == 'FALSE') {
                    $cut_flowers = array('cut_flowers' => 0);
                    $arrayProduct = array_merge($arrayProduct, $cut_flowers);
                }

                if ($row['Long-Lasting_cut'] == 'TRUE') {
                    $long_lasting_cut = array('long_lasting_cut' => 1);
                    $arrayProduct = array_merge($arrayProduct, $long_lasting_cut);
                } elseif ($row['Long-Lasting_cut'] == 'FALSE') {
                    $long_lasting_cut = array('long_lasting_cut' => 0);
                    $arrayProduct = array_merge($arrayProduct, $long_lasting_cut);
                }

                if ($row['Good_for_Forcing'] == 'TRUE') {
                    $good_for_forcing = array('good_for_forcing' => 1);
                    $arrayProduct = array_merge($arrayProduct, $good_for_forcing);
                } elseif ($row['Good_for_Forcing'] == 'FALSE') {
                    $good_for_forcing = array('good_for_forcing' => 0);
                    $arrayProduct = array_merge($arrayProduct, $good_for_forcing);
                }

                if ($row['Good_for_Naturalizing'] == 'TRUE') {
                    $good_for_naturalizing = array('good_for_naturalizing' => 1);
                    $arrayProduct = array_merge($arrayProduct, $good_for_naturalizing);
                } elseif ($row['Good_for_Naturalizing'] == 'FALSE') {
                    $good_for_naturalizing = array('good_for_naturalizing' => 0);
                    $arrayProduct = array_merge($arrayProduct, $good_for_naturalizing);
                }

                if ($row['Multiplies_Annually'] == 'TRUE') {
                    $multiplies_annually = array('multiplies_annually' => 1);
                    $arrayProduct = array_merge($arrayProduct, $multiplies_annually);
                } elseif ($row['Multiplies_Annually'] == 'FALSE') {
                    $multiplies_annually = array('multiplies_annually' => 0);
                    $arrayProduct = array_merge($arrayProduct, $multiplies_annually);
                }

                if ($row['DRIED_FLOWERS'] == 'TRUE') {
                    $dried_flowers = array('dried_flowers' => 1);
                    $arrayProduct = array_merge($arrayProduct, $dried_flowers);
                } elseif ($row['DRIED_FLOWERS'] == 'FALSE') {
                    $dried_flowers = array('dried_flowers' => 0);
                    $arrayProduct = array_merge($arrayProduct, $dried_flowers);
                }

                if ($row['CONTAINER'] == 'TRUE') {
                    $container = array('container' => 1);
                    $arrayProduct = array_merge($arrayProduct, $container);
                } elseif ($row['CONTAINER'] == 'FALSE') {
                    $container = array('container' => 0);
                    $arrayProduct = array_merge($arrayProduct, $container);
                }

                if ($row['Indoors_only'] == 'TRUE') {
                    $indoors_only = array('indoors_only' => 1);
                    $arrayProduct = array_merge($arrayProduct, $indoors_only);
                } elseif ($row['Indoors_only'] == 'FALSE') {
                    $indoors_only = array('indoors_only' => 0);
                    $arrayProduct = array_merge($arrayProduct, $indoors_only);
                }

                if ($row['Indoors_Outdoors_in_warm_temperatures'] == 'TRUE') {
                    $io_in_warm_temp = array('io_in_warm_temp' => 1);
                    $arrayProduct = array_merge($arrayProduct, $io_in_warm_temp);
                } elseif ($row['Indoors_Outdoors_in_warm_temperatures'] == 'FALSE') {
                    $io_in_warm_temp = array('io_in_warm_temp' => 0);
                    $arrayProduct = array_merge($arrayProduct, $io_in_warm_temp);
                }

                if ($row['Can_be_permantly_planted_in_garden'] == 'TRUE') {
                    $permantly_planted_garden = array('permantly_planted_garden' => 1);
                    $arrayProduct = array_merge($arrayProduct, $permantly_planted_garden);
                } elseif ($row['Can_be_permantly_planted_in_garden'] == 'FALSE') {
                    $permantly_planted_garden = array('permantly_planted_garden' => 0);
                    $arrayProduct = array_merge($arrayProduct, $permantly_planted_garden);
                }

                if ($row['Deer_Resistant'] == 'TRUE') {
                    $deer_resistant = array('deer_resistant' => 1);
                    $arrayProduct = array_merge($arrayProduct, $deer_resistant);
                } elseif ($row['Deer_Resistant'] == 'FALSE') {
                    $deer_resistant = array('deer_resistant' => 0);
                    $arrayProduct = array_merge($arrayProduct, $deer_resistant);
                }

                if ($row['Season_Shipped_Fall'] == 'TRUE') {
                    $season_shipped_fall = array('season_shipped_fall' => 1);
                    $arrayProduct = array_merge($arrayProduct, $season_shipped_fall);
                } elseif ($row['Season_Shipped_Fall'] == 'FALSE') {
                    $season_shipped_fall = array('season_shipped_fall' => 0);
                    $arrayProduct = array_merge($arrayProduct, $season_shipped_fall);
                }

                if ($row['Season_Shipped_Spring'] == 'TRUE') {
                    $season_shipped_spring = array('season_shipped_spring' => 1);
                    $arrayProduct = array_merge($arrayProduct, $season_shipped_spring);
                } elseif ($row['Season_Shipped_Spring'] == 'FALSE') {
                    $season_shipped_spring = array('season_shipped_spring' => 0);
                    $arrayProduct = array_merge($arrayProduct, $season_shipped_spring);
                }

                $breck_name = addslashes($row['Spring_Hill_Breck_Name']);
                $common_name = addslashes($row['COMMON_NAME']);
                $family = addslashes($row['Family']);
                $good_for_other = addslashes($row['Good_For_Other']);
                $web_height_search = addslashes($row['Web_Height_Search']);
                $height_habit = addslashes($row['Height_Habit']);
                $spread = addslashes($row['SPREAD']);
                $spacing = addslashes($row['Spacing']);
                $foliage_type = addslashes($row['TYPE_OF_FOLIAGE']);
                $flower_form = addslashes($row['Flower_Form']);
                $average_number_blooms = addslashes($row['Average_Number_of_Blooms']);
                $flower_color = addslashes($row['FLOWER_COLOR']);
                $web_search_flower_color = addslashes($row['Web_Search_Flower_Color']);
                $fragrance = addslashes($row['FRAGRANCE']);
                $planting_time = addslashes($row['Planting_Time']);
                $web_flowering_time_search = addslashes($row['Web_Flowering_Time_Search']);
                $flowering_date = addslashes($row['FLOWERING_DATE']);
                $duration = addslashes($row['DURATION']);
                $blooms_width = addslashes($row['BLOOMS_WITH']);
                $web_search_zone = addslashes($row['Web_Search_Zone']);
                $display_hardiness_zone = addslashes($row['hardiness_zone']);
                $hardiness_zones = addslashes($row['hardiness_zone']);
                $type_of_fruit = addslashes($row['TYPE_OF_FRUIT']);
                $shipped = addslashes($row['Shipped']);
                $special_care_instructions = addslashes($row['Special_care_or_planting_instructions']);
                $soil_requirements = addslashes($row['SOIL_REQUIREMENTS']);
                $root_system = addslashes($row['ROOT_SYSTEM']);
                $growth_rate = addslashes($row['GROWTH_RATE']);
                if (!empty($product_info[0]['detailed_description'])) {
                    //$description = mb_convert_encoding($product_info[0]['detailed_description'], 'CP1252', 'UTF-8');
					$detailDesc = str_replace("","",$product_info[0]['detailed_description']);
                    $description = htmlspecialchars($detailDesc);
                } else {
                    $description = $row['UNIQUE_CHARACTERISTICS'];
                }
                if (!empty($product_info[0]['description'])) {
                    // $short_description = mb_convert_encoding($product_info[0]['description'], 'CP1252', 'UTF-8');
					$shortdetailDesc = str_replace("","",$product_info[0]['description']);
                    $short_description = htmlspecialchars($shortdetailDesc);
                } else {
                    $short_description = $row['UNIQUE_CHARACTERISTICS'];
                }
                $option_name = htmlspecialchars($product_info[0]['option_name']);

                $unique_characteristics = addslashes($row['UNIQUE_CHARACTERISTICS']);
                $two_unique_characteristic = addslashes($row['2unique_Characteristics']);
                $key_difference_similar_bulbs = $row['Key_difference_from_similar_bulbs'];
                $form = $row['FORM'];
                $resistance = $row['RESISTANCE'];
                $pruning = $row['PRUNING'];
                $time_of_pruning = $row['TIME_OF_PRUNING'];
                $additional_information = $row['ADDITIONAL_INFORMATION'];
                $awards = $row['Awards'];
                $winter_requirement = $row['WINTER_REQUIREMENTS'];
                $temperature_requirements = $row['Temperature_Requirements'];
                $light_needed_indoors = $row['Light_Needed_Indoors'];
                $watering_requirements = $row['Watering_Requirements'];
                $fertilization_requirements = $row['Fertilization_Requirements'];
                $special_care = $row['Special_Care'];
                $search_words = "";
				$botanical_name =htmlspecialchars( addslashes($row['Registered_or_Botanical_Name']));

                $txtArrayProduct = array(
                    "breck_name" => $breck_name,
					"botanical_name" => $botanical_name,
                    "common_name" => $common_name,
                    "family" => $family,
                    "good_for_other" => $good_for_other,
                    "web_height_search" => $web_height_search,
                    "height_habit" => $height_habit,
                    "spread" => $spread,
                    "spacing" => $spacing,
                    "foliage_type" => $foliage_type,
                    "flower_form" => $flower_form,
                    "average_number_blooms" => $average_number_blooms,
                    "flower_color" => $flower_color,
                    "web_search_flower_color" => $web_search_flower_color,
                    "fragrance" => $fragrance,
                    "planting_time" => $planting_time,
                    "web_flowering_time_search" => $web_flowering_time_search,
                    "flowering_date" => $flowering_date,
                    "duration" => $duration,
                    "blooms_width" => $blooms_width,
                    "web_search_zone" => $web_search_zone,
                    "display_hardiness_zone" => $display_hardiness_zone,
                    "hardiness_zones" => $hardiness_zones,
                    "type_of_fruit" => $type_of_fruit,
                    "shipped" => $shipped,
                    "special_care_instructions" => $special_care_instructions,
                    "soil_requirements" => $soil_requirements,
                    "root_system" => $root_system,
                    "growth_rate" => $growth_rate,
                    "description" => $description,
                    "short_description" => $short_description,
                    "option_name" => $option_name,
                    "unique_characteristics" => $unique_characteristics,
                    "two_unique_characteristic" => $two_unique_characteristic,
                    "key_difference_similar_bulbs" => $key_difference_similar_bulbs,
                    "form" => $form,
                    "resistance" => $resistance,
                    "pruning" => $pruning,
                    "time_of_pruning" => $time_of_pruning,
                    "additional_information" => $additional_information,
                    "awards" => $awards,
                    "winter_requirement" => $winter_requirement,
                    "temperature_requirements" => $temperature_requirements,
                    "light_needed_indoors" => $light_needed_indoors,
                    "watering_requirements" => $watering_requirements,
                    "fertilization_requirements" => $fertilization_requirements,
                    "special_care" => $special_care,
                    "search_words" => $search_words,
                    "is_featured" => '0',
                    "featured_product_sort_order" => '',
                    "light" => '',
                    "bloom_time" => '',
                    "sun_exposure" => '',
                    "foliage" => '',
                    "usage" => '',
                    "shipping_schedule" => '',
                    "related_video_url" => '',
                    
                );

                //Merge array with main array
                $arrayProduct = array_merge($arrayProduct, $txtArrayProduct);

                //For the name of the product
                if (!empty($product_info[0]['name'])) {
                    //$name = mb_convert_encoding($product_info[0]['name'],'CP1252','UTF-8');
                    $name = htmlspecialchars($product_info[0]['name']);
                } elseif (!empty($breck_name)) {
                    $name = $breck_name;
                } else if (!empty($common_name)) {
                    $name = $common_name; //$start value is less than the number of rows in the product description else show messageommon_name;
                } else if (!empty($botanical_name)) {
                    $name = $botanical_name;
                }

                //add SKU,status to the arrayProduct array
                $arrName = array("name" => $name);
                $arrayProduct = array_merge($arrayProduct, $arrName);

                $arrImage = array("image" => $image, "image_label" => $name, "small_image_label" => $name, "thumbnail_label" => $name);
                //$arrayProduct = array_merge($arrayProduct, $arrImage);
                //Check if sku is already exists or not if exists Update else create
                $_Pdetails = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
                if ($_Pdetails) {
                    $productid = $_Pdetails->getId();
                    try {
                        $client->call($session_id, 'catalog_product.update', array($productid, $arrayProduct));
                    } catch (Exception $e) {
                        
                    }
                } else {

		            //add SKU,status to the arrayProduct array
		            $arrSku = array("sku" => $sku);
		            $arrayProduct = array_merge($arrayProduct, $arrSku);

		            //Adding default attributes
		            $arrayProduct = array_merge($arrayProduct, $createArray);

		            //get attribute set
		            $attributeSets = $client->call($session_id, 'product_attribute_set.list');
		            //echo '<pre>';print_r($arrayProduct);
		            $attributeSet = $attributeSets[1];
		            try {
		                $result = $client->call($session_id, 'catalog_product.create', array('simple', $attributeSet['set_id'], $sku, $arrayProduct));
	//                    if ($type == 'configurable') {
	//                       // $result = $client->call($session_id, 'catalog_product.create', array($type, $attributeSet['set_id'], $sku, $arrayProduct));
	//                    } elseif ($type == 'simple') {
	//                        $result = $client->call($session_id, 'catalog_product.create', array($type, $attributeSet['set_id'], $sku, $arrayProduct));
	//                       // $result2 = $client->call($session_id, 'product_link.assign', array('configurable', $config_sku, $sku, array('position' => $config, 'color' => 92)));
	//                    }

		                //echo 'new==>' . $i . '-----'.$row['File'].'--'  . $sku .'---------'. $name. "\n";
		                $i++;
		            } catch (Exception $e) {
		                echo 'Caught exception: ', $e->getMessage(), "\n";
		                echo 'create failed' . $sku . '<br/>';
		                echo "<pre>";
		                print_r($arrayProduct);
		               
		            }
				}
            } catch (Exception $e) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    echo 'create failed' . $sku . '<br/>';
                    echo "<pre>";
                    print_r($arrayProduct);
                    
                } 
            }
            echo  $a."#####".$sku."\n";die();
            $config++;
             } catch (Exception $e) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    echo 'create failed' . $sku . '<br/>';
                    echo "<pre>";
                    print_r($arrayProduct);
                    
                } 
        
        }
		
    }
    $i++;
die();
}


$start = $start + $num_items;
$CronURL = 'http://192.168.10.122/springhill/apiclient/prodcuct_import.php';
//echo '<meta http-equiv="refresh" content="2; url=' . $CronURL . '?start=' . $start . '">';
?>
