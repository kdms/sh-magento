<?php
require_once '../app/Mage.php';
umask(0);
Mage::app('default');

$handle = fopen("SH_Site_Reviews-2.csv", "r");
$i=0;
$catArray=array();
//echo "<pre>";
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
while (($data = fgetcsv($handle, 100000, "|")) !== FALSE) {
//print_r($data );
	if($i==0){
		$i++;
		continue;	
	}

//die();
 	$review = Mage::getModel('review/review');
	//$data[1] sku,$data[5]title,$data[7]nickname,$data[3]rating,$data[6]content === are mandatory fields
	if(!empty($data[1]) && !empty($data[5]) && !empty($data[3]) && !empty($data[7]) && !empty($data[6])){
		//If sku is only 4 digits then prepend 0 to the sku		
		if(strlen($data[1])==4){
			$data[1]='0'.$data[1];
		}
	//Check if sku is already exists or not if exists Update else create
	    $_product = Mage::getModel('catalog/product')->loadByAttribute('sku',$data[1]);
		//valid Product
		if($_product){
			try{
				

				$review = Mage::getModel('review/review');
				$review->setEntityPkValue($_product->getId());
				$review->setRatingSummary($data[3]);
				if($data[10]=='yes')
					$review->setStatusId(1);
				else
					$review->setStatusId(2);

				$review->setTitle($data[5]);
				$review->setDetail($data[6]);
				$review->setEntityId(1);                                      
				$review->setStoreId(Mage::app()->getStore()->getId());     
				if(!empty($data['7']))
				{
					$NickName=$data['7'];
					$stateCountry='';
					if(!empty($data['8']))
						$stateCountry=$data['8'];
					 if(!empty($data['9'])){
						if(!empty($stateCountry))
							$stateCountry .=' '.$data['9'];
						else
							$stateCountry .= $data['9'];
					}
					if(!empty($stateCountry))
						$NickName=$NickName.' ('.$stateCountry.')';
				
					$review->setNickname($NickName); 
				}
				$review->setStores(array(Mage::app()->getStore()->getId()));
				$review->setReviewId($review->getId());                    
				$review->save();
				$review->aggregate();
				$resource = Mage::getResourceSingleton('review/review');
				$resource->updateRating($data[3],$review->getId());
				//add to the original value
				$optionId=5+$data[3];	
				Mage::getModel('rating/rating')
					->setRatingId(2)
					->setReviewId($review->getId())
					->setCustomerId(0)
					->addOptionVote($optionId, $_product->getId());

				$newdate = "";
				$newreview = Mage::getModel('review/review')->load($review->getId());
				$date =  explode("/",$data['4']);
				echo $newdate = $date[2]."-".$date['1']."-".$date['0']."\n";
				$newreview->setCreatedAt($newdate);
				$newreview->save();

				echo  $i."=======sku=========".$data[1]."=====prodid====".$_product->getId().'=====reviewId====='.$review->getId()."\n";
			}catch(Exception $e){
				print_r($e->getMessage());
			}	           
		}
	}

$i++;
}
