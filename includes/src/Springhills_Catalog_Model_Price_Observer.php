<?php
class Springhills_Catalog_Model_Price_Observer
{
    public function __construct()
    {
    }
    /**
     * Applies the special price percentage discount
     * @param   Varien_Event_Observer $observer
     * @return  Xyz_Catalog_Model_Price_Observer
     */
    public function apply_discount_percent($observer)
    {
		  $actionName = Mage::app()->getRequest()->getModuleName();
		  $event = $observer->getEvent();
		  $product = $event->getProduct();   
		  // process percentage discounts only for simple products  
		  $quoteid=Mage::getSingleton("checkout/session")->getQuote()->getId();   
		  if ($product->getSuperProduct() && $product->getSuperProduct()->isConfigurable()) {
		  } else {
			$session_code = Mage::getSingleton('core/session')->getData("offerCode");
			if($session_code){
				$id = $product->getId();
				$sku = $product->getSku();
				$write = Mage::getSingleton('core/resource')->getConnection('core_write');
				$offerCode = $write->fetchOne("SELECT Offer_Code FROM Keycodes where Keycode='".$session_code."' ");
				$offerProductPrice= $write->fetchAll("SELECT * FROM Price where Product_Number='".$sku."' and  Offer_Code='".$offerCode."' ");
				$tierPrices = array();
				if(count($offerProductPrice)>1){
					foreach($offerProductPrice as $tierInfo){
						 $tierPrices[$tierInfo['Qty_Low']] = $tierInfo['Price'];
					}
					if(count($tierPrices)>0){
						if($actionName=='checkout'){
							$session = Mage::getSingleton('checkout/session');
							foreach ($session->getQuote()->getAllItems() as $item) {
								$product_id = $item->getProductId();		
								$product_qty = $item->getQty();
								if($product_id == $id){									
									$offerProductQtyPrice= $write->fetchOne("SELECT Price FROM Price where Product_Number='".$item->getSku()."' and  Offer_Code='".$offerCode."' and  '".$product_qty."' between Qty_Low AND Qty_High ");
									$product->setFinalPrice($offerProductQtyPrice); 
								}
							}
						}
					}
				}else{
					$offerProductPrice ='';					
					$offerProductPrice= $write->fetchOne("SELECT Price FROM Price where Product_Number='".$sku."' and  Offer_Code='".$offerCode."' ");
					if($offerProductPrice){
						$product->setFinalPrice($offerProductPrice); 
					}  
				}
			}
		  }         
		  //return $this;
    }
}
