<?php

class Magestore_Banner_Adminhtml_CustomerattributeController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('banner/customerattribute')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Customerattribute Manager'), Mage::helper('adminhtml')->__('Attribute Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
		
	}



	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('banner/customerattribute')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('customerattribute_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('banner/customerattributes');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Attribute Manager'), Mage::helper('adminhtml')->__('Attribute Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Attribute News'), Mage::helper('adminhtml')->__('Attribute News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('banner/adminhtml_customerattribute_edit'))
				->_addLeft($this->getLayout()->createBlock('banner/adminhtml_customerattribute_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('banner')->__('Attribute does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
			$data['attr_code']=implode(",",$data['attr_code']);	
			$model = Mage::getModel('banner/customerattribute');		
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			try {
				
				
				
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('banner')->__('Attribute was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('banner')->__('Unable to find attribute to save'));
        $this->_redirect('*/*/');
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('banner/customerattribute');
				 
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
					 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Attribute was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
        $customerattributeIds = $this->getRequest()->getParam('customerattribute');
        if(!is_array($customerattributeIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select attribute(s)'));
        } else {
            try {
                foreach ($customerattributeIds as $customerattributeId) {
                    $customerattribute = Mage::getModel('banner/customerattribute')->load($customerattributeId);
                    $customerattribute->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($customerattributeIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
    public function massStatusAction()
    {
        $customerattributeIds = $this->getRequest()->getParam('customerattribute');
        if(!is_array($customerattributeIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select attribute(s)'));
        } else {
            try {
                foreach ($customerattributeIds as $customerattributeId) {
                    $customerattribute = Mage::getSingleton('banner/customerattribute')
                        ->load($customerattributeId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($customerattributeIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
  
    public function exportCsvAction()
    {
        $fileName   = 'customerattribute.csv';
        $content    = $this->getLayout()->createBlock('banner/adminhtml_customerattribute_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'customerattribute.xml';
        $content    = $this->getLayout()->createBlock('banner/adminhtml_customerattribute_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}
