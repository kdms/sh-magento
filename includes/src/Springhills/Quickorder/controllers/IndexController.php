<?php
class Springhills_Quickorder_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      
	  $this->loadLayout();   
	  $this->getLayout()->getBlock("head")->setTitle($this->__("Quickorder"));
	        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home"),
                "title" => $this->__("Home"),
                "link"  => Mage::getBaseUrl()
		   ));

      $breadcrumbs->addCrumb("quickorder", array(
                "label" => $this->__("Quickorder"),
                "title" => $this->__("Quickorder")
		   ));

      $this->renderLayout(); 
	  
    }
    public function postAction()
    {	
        $post = $this->getRequest()->getPost();
        
        if ($post) {
				
				$sku= Mage::app()->getRequest()->getParam('sku-entry', false);	
							
				$qty=Mage::app()->getRequest()->getParam('qty', false);
				
				//print_r($sku); echo count($sku); die();
				//php validations...
				for($i=0;$i<count($sku);$i++){ 
					
					$sku_length=strlen($sku[$i]);
					$qty_length=strlen($qty[$i]);

					if(($qty[$i]=="Qty.") && ($sku[$i]=="Item #")){		// Checking Validation				
						$error="true"; 
						Mage::getSingleton('core/session')->addError('Please enter minimum 5 letters for sku and 1 letter for quantity');
						Mage::getSingleton('core/session')->setFormData(false);
						$red_url= Mage::getBaseUrl()."quickorder/index/";
						header("location:".$red_url);
						exit;												
					}

					if(($qty_length<1) || ($qty[$i]=="Qty.")){		// Checking Validation				
						$error="true"; 
						Mage::getSingleton('core/session')->addError('Please enter minimum 1 letter for quantity');
						Mage::getSingleton('core/session')->setFormData(false);
						$red_url= Mage::getBaseUrl()."quickorder/index/";
						header("location:".$red_url);
						exit;												
					}


					if(($sku_length<3) || ($sku[$i]=="Item #")){		// Checking Validation				
						$error="true"; 
						Mage::getSingleton('core/session')->addError('Please enter minimum 3 letters for sku');
						Mage::getSingleton('core/session')->setFormData(false);
						$red_url= Mage::getBaseUrl()."quickorder/index/";
						header("location:".$red_url);
						exit;												
					}

					

					$_productId = Mage::getModel('catalog/product')->getIdBySku("".$sku[$i]."");
					$_prod = Mage::getModel('catalog/product')->load($_productId);
					
					if($_prod=='' || $_prod->getStatus()!=1 || $_prod->getFutureSeasonMessage()==1 || $_prod->getPrice()==0){
				
						$sku_arr[]=$sku[$i];
					}
					
				} 
					if($error=="true"){ //If Error
						$skuStr = implode(",",$sku_arr);
						Mage::getSingleton('core/session')->addError('The SKU you entered "'.$skuStr.'" was not found.');
						Mage::getSingleton('core/session')->setFormData(false);
						$red_url= Mage::getBaseUrl()."quickorder/index/";
						header("location:".$red_url);
						exit;
					}else{ 		// If Not Error							
						$product = Mage::getModel('catalog/product');				
						$productCollection = $product->getCollection()
				        ->addAttributeToSelect('*')
				        ->addAttributeToFilter('sku', $sku)
						->addAttributeToFilter("price", array('gt'=> 0))
						->addAttributeToFilter('future_season_message', array('gt'=>1))
				        ->addAttributeToFilter('status', 1)
				        ->load();

						//echo count($productCollection);
						if(count($productCollection)<1){  // If Only one SKU entered
							$skuStr = implode(",",$sku_arr);
							Mage::getSingleton('core/session')->addError('SKU you entered "'.$skuStr.'" was not found.');
							Mage::getSingleton('core/session')->setFormData(false);
							$red_url= Mage::getBaseUrl()."quickorder/index/";
							header("location:".$red_url);
							exit;
						}else{ // If more than one SKU entered
							$cart = Mage::getModel("checkout/cart");						
							$i=0; 
							 $arrsku = count($sku); 
							 $countpro = count($productCollection);
							 
							$same_flag=="false";
							if($arrsku!=$countpro){
								$same_flag="true";								
								//$skuStr = implode(",",$sku_arr);
								//$msg.= 'SKU you entered "'.$skuStr.'" was not found.'."<br />";
							} else {
								$same_flag="false";
							}
							//echo $same_flag; die(); 
							foreach($productCollection as $_product){
									
									 $productid= $_product->getId(); 
									$_product = Mage::getModel('catalog/product')->load($productid);
									foreach ($_product->getOptions() as $o) { 
											$isRequire = $o->getIsRequire();
		       						} 

									$product_sku=$_product->getSKU(); 
									$key = array_search( $product_sku, $sku);

									$categoryId= $_product->getCategoryIds(); 
									//echo $i."--".$sku[$key]."--".$qty[$key];  echo "<br>";
								$stock_count = (int) Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product)->getQty();
									//echo "<br>";
									$session = Mage::getSingleton('checkout/session');
									foreach ($session->getQuote()->getAllItems() as $_cartitem) {
										$productCartId= $_cartitem->getProductId();
										$productCartQty= $_cartitem->getQty();
										if($productid == $productCartId){
												$stock_count = $stock_count-$productCartQty;
										}
									}
									if(in_array(260,$categoryId) || in_array(262,$categoryId) ){ 
										$membersku[]=$product_sku;
										$member_flag="true";
									}
									else{ 
										 	if(!($_product['has_options'] && $isRequire ) &&  $_product['type_id']=='simple' ){												
												if(($stock_count>=1)&&($stock_count>=$qty[$key])){
													$entered_qty = $qty[$key];
													$uom = $_product->getUnitOfMeasure();
													$seasonMsg = $_product->getFutureSeasonMessage();
													if($seasonMsg == "1") {
														$seasonMsg_flag = "true";
														$season_sku[] = $product_sku;
													}
													//die();
														if($uom > 1){
															$qty[$key] = $uom * (ceil($entered_qty/$uom));															
														}
														$uom_flag = "false";
														if(($entered_qty < $uom) || ($entered_qty > $uom && $entered_qty < $qty[$key])) {
															$uom_flag = "true";
														}
														//echo $qty[$key]; exit;
														
													if($qty[$key]<1){
														
														$cart->addProduct($productid, 1);
														$simple_flag="true";
														$cat_addeditem[]=$productid;
													}elseif($seasonMsg != "1"){ 								

														$cart->addProduct($productid, $qty[$key]);
														 $simple_flag="true";
														 $cat_addeditem[]=$productid;
													}
													$simple_options[]=$product_sku;
												}else{
													$anavailablesku[]=$product_sku;
													$simple_flag="false";
												} 
												$custom_flag="false";
											}else{
												
													$custom_options[]=$product_sku;
													$custom_flag="true";

											}

									}
									 $i++;					
							}

						$cart->save();	
						$_products = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
						$_productssku = $_product->getSKU();
						 $stock_count = (int) Mage::getModel('cataloginventory/stock_item')->loadByProduct($_products)->getQty();
						//print_r($simple_options); 
							if($seasonMsg_flag == "true") {
							$season_prod=implode(",",$season_sku);
							   $msg.= 'We are sorry, '.$season_prod.' is currently unavailable'."<br />";
							}
						if($stock_count<1 || $stock_count==NULL){		
							$out_stock_prod=implode(",",$anavailablesku);
							$gift_prod=implode(",",$gift_card);
							
							if(!empty($anavailablesku)){
								if(count($anavailablesku)>1){
										$msg.= '"'.$out_stock_prod.'"  Products not available'."<br />";
									}else{
										$msg.= '"'.$out_stock_prod.'"  Product not available'."<br />";
									}
							}
						}
						
						if($custom_flag=="true"){
								$out_stock_prod=implode(",",$custom_options);							
								if(count($custom_options)>1){
									$msg.= '"'.$out_stock_prod.'"  Products has custom options, So please add these products from product detailed page (<a href="'.$_products->getProductUrl().'" >click here</a>)'."<br />";
								}else{
									$msg.= '"'.$out_stock_prod.'"  Product has custom options, So please add this product from product detailed page (<a href="'.$_products->getProductUrl().'" >click here</a>)'."<br />";
								}
								
						} 

						if(($simple_flag=="true")&&($stock_count>=1)){
							$simple_stock_prod=implode(",",$simple_options);						
								if(count($simple_options)>1 && $same_flag=="false"){
									$Successmsg.= '"'.$simple_stock_prod.'" Added item in your shopping cart'."<br />";
								} elseif($uom_flag=="true" && $same_flag=="false"){
									$Successmsg.= '"'.$simple_stock_prod.'" Added in your shopping cart with Quantity Adjustments'."<br />";
								} elseif($uom_flag=="true" && $same_flag=="true"){
									$Successmsg.= 'Same SKUs entered, "'.$simple_stock_prod.'" Added in your shopping cart with Quantity Adjustments'."<br />";
								} elseif($same_flag=="true" && $uom_flag=="false") {
									$Successmsg.= 'Same SKUs entered, "'.$simple_stock_prod.'" Added in your shopping cart'."<br />";
								} else {
									$Successmsg.= '"'.$simple_stock_prod.'" Added in your shopping cart'."<br />";
								}							
						}

						if(($simple_flag=="false")&&($stock_count>1)){
							
							$out_stock_prod=implode(",",$anavailablesku);

								if(count($anavailablesku)>1){
									$msg.= '"'.$out_stock_prod.'" Products requested quantity not available'."<br />";
								}else{
									$msg.= '"'.$out_stock_prod.'" Product requested quantity not available'."<br />";
								}
						}
						
							if($msg)
								Mage::getSingleton('core/session')->addError($msg);
							if($Successmsg)
								Mage::getSingleton('core/session')->addSuccess($Successmsg);

							Mage::getSingleton('core/session')->setFormData(false);
							$red_url= Mage::getBaseUrl()."quickorder/index/";
							header("location:".$red_url);
							exit;					
							
						}
					}
				
        } else {
            $red_url= Mage::getBaseUrl()."quickorder/index/";
			header("location:".$red_url);
			exit;
        }
    }
}
