<?php
require_once 'app/code/core/Mage/Newsletter/controllers/SubscriberController.php';
class Springhills_Newsletter_SubscriberController extends Mage_Newsletter_SubscriberController
{
    /**
      * New subscription action
      */
    public function newAction()
    {

        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('email')) {
        	
            $session            = Mage::getSingleton('core/session');
            $customerSession    = Mage::getSingleton('customer/session');
            $email              = (string) $this->getRequest()->getPost('email');
            try {
                if (!Zend_Validate::is($email, 'EmailAddress')) {
                    Mage::throwException($this->__('Please enter a valid email address.'));
                }

                if (Mage::getStoreConfig(Mage_Newsletter_Model_Subscriber::XML_PATH_ALLOW_GUEST_SUBSCRIBE_FLAG) != 1 && 
                    !$customerSession->isLoggedIn()) {
                    Mage::throwException($this->__('Sorry, but administrator denied subscription for guests. Please <a href="%s">register</a>.', Mage::helper('customer')->getRegisterUrl()));
                }

                $resource = Mage::getResourceSingleton('newsletter/subscriber');
                $existenceCount = $resource->checkSubscriberEmail($email);
                if ($existenceCount > 0) {
                	Mage::throwException($this->__('This email address is already subscribed.'));
                }else{
	                $status = Mage::getModel('newsletter/subscriber')->subscribe($email);
	                //fetch the subscriber_id 
	                $subscriberId=$resource->getSubscriberId($email);
	                // set cookie for email subscription
	                //echo $email;
	                Mage::getModel('core/cookie')->set('email_id', $email);
	                //echo Mage::getModel('core/cookie')->get('email_id'); die();
	                if ($status == Mage_Newsletter_Model_Subscriber::STATUS_NOT_ACTIVE) {
	                    $session->addSuccess($this->__('Confirmation request has been sent.'));
	                }
	                else {
	                    $session->addSuccess($this->__('Thank you for your subscription.'));
	                    Mage::getSingleton('core/session')->setFormData(false);
	                    $this->_redirect('emailsignup-thankyou');
	                    return;
	                }
	                
                }
            }
            catch (Mage_Core_Exception $e) {
                $session->addException($e, $this->__('There was a problem with the subscription: %s', $e->getMessage()));
            }
            catch (Exception $e) {
                $session->addException($e, $this->__('There was a problem with the subscription.'));
            }
        }
        $this->_redirectReferer();
    }

   
}
