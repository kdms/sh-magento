<?php
class Springhills_Customerservice_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
         
	  $this->loadLayout();   
	  $this->getLayout()->getBlock("head")->setTitle($this->__("Customer service"));
	        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home"),
                "title" => $this->__("Home"),
                "link"  => Mage::getBaseUrl()
		   )); 
  
      $breadcrumbs->addCrumb("Customer service ", array(
                "label" => $this->__("Customer service "),
                "title" => $this->__("Customer service ")
		   ));  

      $this->renderLayout(); 
	  
    }
     public function postAction()
    { 
                
           $post = $this->getRequest()->getPost();
           
        
          


        if ( $post ) {
       
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);
             //var_dump ( $_POST ); die();

                $error = false;

                if (!Zend_Validate::is(trim($post['fullname']) , 'NotEmpty')) {
                    $error = true;
                }
        /*
                if (!Zend_Validate::is(trim($post['comment']) , 'NotEmpty')) {
                    $error = true;
                } */

                if (!Zend_Validate::is(trim($post['emailaddress']), 'EmailAddress')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['phonenumber']), 'NotEmpty')) {
                    $error = true;
                }                
                /*    
                if ($error) {
                    throw new Exception();
                }  */
			/* Saving values in database */
			$data['name']=trim($post['fullname']);
			$data['email']=trim($post['emailaddress']);			
			$data['phone']=trim($post['phonenumber']);
                        $data['message']=trim($post['message']);
			$model = Mage::getModel('customerservice/customerservice');
			$model->setData($data);
			$model->save();


/* Sending email */
 $store_email= Mage::getStoreConfig('trans_email/ident_general/email'); 
 $store_name = Mage::getStoreConfig('trans_email/ident_general/name');     
	$mail_body = "<h3>Customer service</h3>";        
        $to_email = $store_email;
        $to_name  = $store_name;
        $subject  = "Customer service";       
        $sender_email = $post['emailaddress'];
        $sender_name  = $post['firstname'];
	$template="customerservice_form.html";
        //$redirect  =  $post['redirecturl'];
        //$content = "Dear Admin, these are the Quick enquiry Content ".$post['message'];

        
        $content ='Name:'.$post['firstname'].' '.$post['lastname'].'<br>'.'Email:'.$post['emailaddress'].'<br>'.'Zipcode:'.$post['zipcode'].'<br>'.'Order Number:'.$post['ordernum'].'<br>'.'Account Number:'.$post['accnum'].'<br>'.'Category:'.$post['category'].'<br>'.'Message:'.$post['message'];
	 
        

   
         /*
             Thanks and Regards,
		    Gilla Srinivas
		    Customer Relations Executive
		    Tel: + 91-40-66332499
		    Mob: +91-9642348383
		    Email: spotsupport@capsgold.com.
          $mail->addCc($cc, $ccname);   
          $mail->addBCc($bcc, $bccname);
         */

	        $mail = new Zend_Mail();
		$mail->setType(Zend_Mime::MULTIPART_RELATED);
		$mail->setBodyHtml($content);
                //$mail->setBodyText( $content ); 
		$mail->setFrom($sender_email, $sender_name );
		$mail->addTo($to_email, $to_name);
		$mail->setSubject( $subject );
                //$mail->send();           
			
        try {
     if($mail->send())
              { 
              
             Mage::getSingleton('core/session')->addSuccess('We will get back to you shortly.');
     
                //Mage::getSingleton('core/session')->addSuccess('Your request was submitted and will be responded to as soon as possible. Thank you for contacting us.');
		//$msg='Your request was submitted and will be responded to as soon as possible. Thank you for contacting us.';
              Mage::getSingleton('core/session')->setFormData(false);
				$this->_redirect( 'service' );
				return;
     		}else{
			 Mage::getSingleton('core/session')->addError('mail not sent');
			 $this->_redirect( 'service' );
				return;
		}		
             
        }
        catch(Exception $ex) {
                Mage::getSingleton('core/session')->addError('Unable to submit your request. Please, try again later');
		//$msg='Unable to submit your request. Please, try again later.';		
        } 
		Mage::getSingleton('core/session')->setFormData(false);
                              // $this->_redirect( $redirect );
				$this->_redirect( 'service' );
				return;
	
            } catch (Exception $e) {
                $translate->setTranslateInline(true);

                //Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('Unable to submit your request. Please, try again later'));
				//Mage::getSingleton('core/session')->addSuccess('Unable to submit your request. Please, try again later');
                 //$this->_redirect( $redirect );
                $this->_redirect( 'service' );
                return;
            }

        } else {
            //$this->_redirect( $redirect );
            $this->_redirect( 'service' );
        }
    }
}
