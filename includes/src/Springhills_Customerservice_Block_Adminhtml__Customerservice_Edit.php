<?php
	
class Springhills_Customerservice_Block_Adminhtml_Customerservice_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "customerservice_id";
				$this->_blockGroup = "customerservice";
				$this->_controller = "adminhtml_customerservice";
				$this->_updateButton("save", "label", Mage::helper("customerservice")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("customerservice")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("customerservice")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("customerservice_data") && Mage::registry("customerservice_data")->getId() ){

				    return Mage::helper("customerservice")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("customerservice_data")->getName()));

				} 
				else{

				     return Mage::helper("customerservice")->__("Add Item");

				}
		}
}