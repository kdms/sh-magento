<?php

class Magestore_Banner_Block_Adminhtml_Condition_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{

		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('banner_form', array('legend'=>Mage::helper('banner')->__('Condition')));
	    $conditionId = $this->getRequest()->getParam('id');
	
       	$resource = new Mage_Core_Model_Resource();
       	$read = $resource->getConnection('core_read'); 
     	$select = $read->select()->from('bannercondition')->where('id=?', $conditionId);  
		
		$items = $read->fetchAll($select);

        // print_r($items);
		$banners = array();
		$banners[]     =  array ( 'label'=> $this->getRequest()->getParam('banner_id'),'value' => $this->getRequest()->getParam('banner_id') );
		if($this->getRequest()->getParam('banner_id')){
			$fieldset->addField('banner_id', 'select', array(
				  'name'     => 'banner_id',
				  'values'    => $banners,
				  'style'    => 'display:none;'		    	
			));
		}  

		$onchange = $fieldset->addField('attr_type', 'select', array(
			  'label'     => Mage::helper('banner')->__('Attribute Type'),
			  'name'      => 'attr_type',
			  'required'  => true,
			  'values'    => array(
				  array(
				      'value'     => '',
				      'label'     => Mage::helper('banner')->__('-- Please Select --'),
				  ),
				  array(
				      'value'     => 'Customer',
				      'label'     => Mage::helper('banner')->__('Customer'),
				  ),
                  array(
				       'value'     => 'CustomerAddress',
				       'label'     => Mage::helper('banner')->__('Customer Address'),
				                  ),
                  array(
				       'value'     => 'Category',
				       'label'     => Mage::helper('banner')->__('Category'),
				                  ),

				  array(
				      'value'     => 'Product',
				      'label'     => Mage::helper('banner')->__('Product'),
				  ),
        	  ),
             'onchange' => "changeAttributeCode(this.value)",

		));
        
        //$attr_type='Product';
        $resource = new Mage_Core_Model_Resource();
		$read = $resource->getConnection('core_read');
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
		$myArray=array();
		$attr =array();
		$attr[] = array("value"=>"","label"=>"--Please Select--");
		if( $this->getRequest()->getParam('id') ){
	
			$attr_type =  $write -> fetchOne( "SELECT attr_type  FROM bannercondition  where id = '".$this->getRequest()->getParam('id')."'");
			//$attr_type='Product';
		    $select = $read->select()->from('bannerattributes')->where('attr_type=?', "$attr_type");
		    $fetchregdata = $read->fetchAll($select);
		    $str1 = $fetchregdata[0]['attr_code'];
		
		    $myArray = explode(',', $str1);
		
		   // echo sizeof($fetchregdata);

			foreach($myArray as $attCode) {
		   		$attr[] = array(
							'label' => $attCode ,
							'value' => $attCode ,
						 );
			}
	
		}
		$onchange2 = $fieldset->addField('attr_code', 'select', array(
					  'label'     => Mage::helper('banner')->__("Attribute Code"),
					  'name'      => 'attr_code[]',
					  'required'  => true,
					  'values'     => $attr,
                      'onchange' => "changeAttributeType(this.value)",
		));

        $onchange3 =$fieldset->addField('relation', 'select', array(
			  'label'     => Mage::helper('banner')->__('Relation'),
			  'name'      => 'relation',
			  'required'  => true,
			  'values'    => array(
					array(
				      'value'     => '',
				      'label'     => Mage::helper('banner')->__('-- Please Select --'),
				  	),
					array(
						'value'     => 'Equals',
						'label'     => Mage::helper('banner')->__('Equals'),
					),
					array(
					  'value'     => 'Not Equals',
					  'label'     => Mage::helper('banner')->__('Not Equals'),
					),
					array(
					  'value'     => 'Greater than',
					  'label'     => Mage::helper('banner')->__('Greater'),
					),
					array(
					  'value'     => 'Lesser than',
					  'label'     => Mage::helper('banner')->__('Lesser'),
					),
					array(
					  'value'     => 'Greater than Equals',
					  'label'     => Mage::helper('banner')->__('Greater than Equals'),
					),
					array(
					  'value'     => 'Lesser than Equals',
					  'label'     => Mage::helper('banner')->__('Lesser than Equals'),
					),
					array(
					  'value'     => 'Between',
					  'label'     => Mage::helper('banner')->__('Between'),
					),
				),
  				'onchange' => "changeValueField(this.value)",
        ));
            

	   $conditionId = $this->getRequest()->getParam('id');
       if($conditionId){

			$resource = new Mage_Core_Model_Resource();
			$read = $resource->getConnection('core_read'); 
			$select = $read->select()->from('bannercondition')->where('id=?', $conditionId);  

			$items = $read->fetchAll($select);

			$attributeDetails = Mage::getResourceModel('eav/entity_attribute_collection')
						->setCodeFilter($items[0]['attr_code'])
						->load()
						->getFirstItem();

           if($items[0]['attr_code'] == "email") { $class = "validate-email";} else {  $class = ""; }
			$inputtype = $attributeDetails['frontend_input'];

			if($items[0]['relation'] == 'Between')  {

				
				if($inputtype == "date") {

					$fieldset->addField('attr_value', 'text', array(
						'label'     => Mage::helper('banner')->__('Value'),
						'name'      => 'attr_value',
						'required'  => true,
                        'class'     => 'validate-date-custom',
						'style'     => 'width:130px',
						'after_element_html' => '<span class="hint" id="replacevalue">
						 &nbsp;<input type="text" class=" input-text required-entry validate-date-custom" value="'.$items[0]['attr_value2'].'" name="attr_value2" id="attr_value2" style="width:130px;" ><div id="dateformat" >(Date format: YYYY-MM-DD )</div>
						 </span>',
					));
				

        		}else if($inputtype == "select") {

					$attributeDetails = Mage::getResourceModel('eav/entity_attribute_collection')
										            ->setCodeFilter($items[0]['attr_code'])->load()->getFirstItem()
										            ->getSource()
										            ->getAllOptions(false);

					$attrValues = array();
					$attrValues2 =  "";
					foreach($attributeDetails as $attoptions) {
						$attrValues[] = array(
										'value' => $attoptions['value'] ,
										'label' => $attoptions['label'] ,
										  );
						$selected='';
						if($items[0]['attr_value2'] == $attoptions['value'] )
							$selected = 'selected';

						$attrValues2 .= '<option value='.$attoptions['value'].' '.$selected.' >'.$attoptions['label'].'</option>';


					}
					

					$fieldset->addField('attr_value', 'select', array(
						'label'     => Mage::helper('banner')->__('Value'),
						'name'      => 'attr_value',
						'required'  => true,
						'values' => $attrValues,
						'style'  => "width:135px",					
						'after_element_html' => '<span class="hint" id="replacevalue"> &nbsp; <select id="attr_value2" style ="width:135px" name="attr_value2" class=" input-text required-entry">'.$attrValues2.'</select></span>',
				
					));
			
					
				

        		}else{

					$fieldset->addField('attr_value', 'text', array(
						'label'     => Mage::helper('banner')->__('Value'),
						'name'      => 'attr_value',
						'required'  => true,
						'style'     => 'width:128px',
                        'class'     =>  $class,
						'after_element_html' => '<span class="hint" id="replacevalue">
						 <input type="text" class=" input-text required-entry" value="'.$items[0]['attr_value2'].'" name="attr_value2" id="attr_value2" style="width:128px;" >
						 </span>',
					));
				}
					

				

			}  else {

				// We have taken only three type of input 1) select 2) date 3) rest all comes in text box

				if($inputtype == "select") {

					$attributeDetails = Mage::getResourceModel('eav/entity_attribute_collection')
										            ->setCodeFilter($items[0]['attr_code'])->load()->getFirstItem()
										            ->getSource()
										            ->getAllOptions(false);

					$attrValues = array();
					foreach($attributeDetails as $attoptions) {
					$attrValues[] = array(
										'value' => $attoptions['value'] ,
										'label' => $attoptions['label'] ,
										  );
						}

					$fieldset->addField('attr_value', 'select', array(
						'label'     => Mage::helper('banner')->__('Value'),
						'name'      => 'attr_value',
						'required'  => true,
						'values' => $attrValues,
						'after_element_html' => '<span class="hint" id="replacevalue"></span>',
				
					));

				}else if($inputtype == "date") {


					$fieldset->addField('attr_value', 'text', array(
						'label'     => Mage::helper('banner')->__('Value'),
						'name'      => 'attr_value',
						'required'  => true,
						'class'     => 'validate-date-custom',
						'after_element_html' => '<span class="hint" id="replacevalue"><div id="dateformat" >(Date format: YYYY-MM-DD )</div></span>',
					));


        		}else{
                  
					$fieldset->addField('attr_value', 'text', array(
						'label'     => Mage::helper('banner')->__('Value'),
						'name'      => 'attr_value',
                        'class'     => $class,
						'required'  => true,
						'after_element_html' => '<span class="hint" id="replacevalue"></span>',
					));
				}

			}

		}else{

			// only comes for adding the new condition

			$fieldset->addField('attr_value', 'text', array(
				'label'     => Mage::helper('banner')->__('Value'),
				'name'      => 'attr_value',
				'required'  => true,
				'before_element_html' => '<div id="text"></div>',
				'after_element_html' => '<span class="hint" id="replacevalue"></span>',
			));
		}

		$fieldset->addField('attr_cond', 'select', array(
		  'label'     => Mage::helper('banner')->__('Condition'),
		  'name'      => 'attr_cond',
		  'required'  => true,
		  'values'    => array(
			  array(
				  'value'     => '',
				  'label'     => Mage::helper('banner')->__('--Please Select--'),
			  ),
			  array(
				  'value'     => 'AND',
				  'label'     => Mage::helper('banner')->__('AND'),
			  ),

			  array(
				  'value'     => 'OR',
				  'label'     => Mage::helper('banner')->__('OR'),
			  ),                                
		  ),
		));

		$URL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
            
		$onchange->setAfterElementHtml("<script language='javascript'>
        jQuery.noConflict();	
		function changeAttributeCode(str) { 
       		var url = '$URL';
        	var full_url = url+'ajaxfiles/getattributes.php?str='+str+'&time='+ new Date().getTime();
	        if(window.XMLHttpRequest) 
            {
	          	xmlhttp=new XMLHttpRequest();
	      	}else{
				xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');
			}
	        xmlhttp.onreadystatechange=function()
	      	{
	          	if (xmlhttp.readyState==4 && xmlhttp.status==200)
	            {
                    jQuery('#attr_code').html(xmlhttp.responseText);
					//document.getElementById('attr_code').innerHTML=xmlhttp.responseText;
					jQuery('#relation').val('');		
					jQuery('#attr_value').css('width','268px');
					jQuery('#dateformat').remove();
					jQuery('#attr_value').removeClass('validate-date-custom');
					jQuery('#attr_value').removeClass('validate-email');
					jQuery('#attr_value').val('');
					jQuery('#attr_value2').val('');
					jQuery('#attr_value2').remove();
		        }
	      	}
        	xmlhttp.open('GET',full_url,true);
        	xmlhttp.send();
     	} 
        </script>");

        $onchange2->setAfterElementHtml("<script>
        jQuery.noConflict();
		function changeAttributeType(str) { 
        	var url = '$URL';
        	if(str){
				jQuery('#attr_value').remove();
				jQuery('#attr_value_trig').remove();
				jQuery('#advice-validate-email-attr_value').remove();
				jQuery('#advice-validate-email-attr_value2').remove();
				jQuery('#advice-validate-date-custom-attr_value').remove();
				jQuery('#advice-validate-date-custom-attr_value2').remove();
          	}
            var full_url = url+'ajaxfiles/getattributetype.php?str='+str+'&time='+ new Date().getTime();

            if (window.XMLHttpRequest){
             	xmlhttp=new XMLHttpRequest();
          	}else{
              	xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');
          	}
            xmlhttp.onreadystatechange=function()
          	{
          		if (xmlhttp.readyState==4 && xmlhttp.status==200)
                {
                    jQuery('#replacevalue').html(xmlhttp.responseText);
					jQuery('#relation').val('');
		            document.getElementById('replacevalue').style.display = 'block';
	            }
         	}
            xmlhttp.open('GET',full_url,true);
            xmlhttp.send();
     	} 

        </script>");

      	$onchange3->setAfterElementHtml("<script>
        jQuery.noConflict();   
		function changeValueField(str) {  
           		var attrcode = jQuery('#attr_code').val();
				str = str+'&attrcode='+attrcode;
				jQuery('#attr_value').remove();
				jQuery('#attr_value2').val('');
				jQuery('#advice-required-entry-attr_value2').remove();

            	var url = '$URL';
                var full_url = url+'ajaxfiles/changevaluefield.php?str='+str+'&time='+ new Date().getTime();
                //alert(full_url);
	            if (window.XMLHttpRequest){
					// code for IE7+, Firefox, Chrome, Opera, Safari
              		xmlhttp=new XMLHttpRequest();
              	}else{
					// code for IE6, IE5
             		xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');
              	}
        		xmlhttp.onreadystatechange=function(){
              		if (xmlhttp.readyState==4 && xmlhttp.status==200)
                	{
                		//document.getElementById('replacevalue').innerHTML=xmlhttp.responseText;
						document.getElementById('replacevalue').style.display = 'block';
                        jQuery('#replacevalue').html(xmlhttp.responseText);
                	}
              	}
            	xmlhttp.open('GET',full_url,true);
            	xmlhttp.send();
       	} 

        </script>");     		
				
		$fieldset->addField('sort_order', 'text', array(
		  'label'     => Mage::helper('banner')->__('Condition Order'),
		  'class'     => 'required-entry',
		  'required'  => true,
		  'name'      => 'sort_order',
		));
	
		$fieldset->addField('status', 'select', array(
		  'label'     => Mage::helper('banner')->__('Status'),
		  'name'      => 'status',
		  'values'    => array(
			  array(
				  'value'     => 1,
				  'label'     => Mage::helper('banner')->__('Enabled'),
			  ),

			  array(
				  'value'     => 2,
				  'label'     => Mage::helper('banner')->__('Disabled'),
			  ),
		  ),
		));

		

		$fieldset->addField('entity_type_id', 'text', array(
			  'name'     => 'entity_type_id',
			  'style'    => 'display:none;'		    	
		));


		$fieldset->addField('attribute_id', 'text', array(
			  'name'     => 'attribute_id',
			  'style'    => 'display:none;'		    	
		));
		
		$fieldset->addField('category_id', 'text', array(
			  'name'     => 'category_id',
			  'style'    => 'display:none;'		    	
		));
		


		if ( Mage::getSingleton('adminhtml/session')->getConditionData() )
		{
			  $form->setValues(Mage::getSingleton('adminhtml/session')->getConditionData());
			  Mage::getSingleton('adminhtml/session')->getConditionData(null);
		} elseif ( Mage::registry('condition_data') ) {
			  $form->setValues(Mage::registry('condition_data')->getData());
		}

		return parent::_prepareForm();
		}
}

echo "<script type='text/javascript'>
//<![CDATA[
if(Validation) { 
    Validation.add('validate-date-custom', 'Please use this date format: yyyy-mm-dd. For example 2006-03-17 for the 17th of March, 2006.', function(v) {
                if(Validation.get('IsEmpty').test(v)) return true;
                var regex = /^(\d{4})\-(\d{2})\-(\d{2})$/;
                if(!regex.test(v)) return false;
                var d = new Date(v.replace(regex, '$1/$2/$3'));
                return ( parseInt(RegExp.$2, 10) == (1+d.getMonth()) ) &&
                            (parseInt(RegExp.$3, 10) == d.getDate()) &&
                            (parseInt(RegExp.$1, 10) == d.getFullYear() );
            });
    var dataForm = new VarienForm('edit_form', true);
}

//]]>   
</script>";
