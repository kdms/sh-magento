<?php

class Magestore_Banner_Model_Customerattribute extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('banner/customerattribute');
    }
	
	public function getOptions()
	{
		$options = array();
		$customerattributeCollection = $this->getCollection();
		
		foreach($customerattributeCollection as $customerattribute)
		{
			$option = array();
			$option['label'] = $customerattribute->getName();
			$option['value'] = $customerattribute->getId();
			$options[] = $option;
		}
		
		return $options;
	}
}
