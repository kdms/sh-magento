<?php

class Springhills_Freecatalog_Block_Adminhtml_Freecatalog_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("freecatalogGrid");
				$this->setDefaultSort("freecatalog_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("freecatalog/freecatalog")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("freecatalog_id", array(
				"header" => Mage::helper("freecatalog")->__("ID"),
				"align" =>"right",
				"width" => "50px",
				"index" => "freecatalog_id",
				));
				$this->addColumn("firstname", array(
				"header" => Mage::helper("freecatalog")->__("Name"),
				"align" =>"left",
				"index" => "firstname",
				));

				$this->addColumn("email", array(
				"header" => Mage::helper("freecatalog")->__("Email"),
				"align" =>"left",
				"index" => "email",
				));

				$this->addColumn("company", array(
				"header" => Mage::helper("freecatalog")->__("Company"),
				"align" =>"left",
				"index" => "company",
				));


				$this->addColumn("phone", array(
				"header" => Mage::helper("freecatalog")->__("Contact Number"),
				"align" =>"left",
				"index" => "phone",
				));


				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}

}
