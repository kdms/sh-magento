<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Sphill
 * @package     Sphill_Customization
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class Sphill_Customization_Block_Product_AddToCart extends Mage_Core_Block_Template
{
    protected function _toHtml()
    {
        if ($this->_getData('sku')) {
            $_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $this->_getData('sku'));
            if ($_product) {
                $params = array('product'=>$_product->getId());

                $blockType = $this->_getData('variant');
                $link = Mage::getUrl('checkout/cart/add', $params);

                if ($blockType == 'link'){
                    return $link;
                } else {
                    $html = '<button class="button btn-buy" onclick="setLocation(\'' . $link . '\')"><span><span>' . Mage::helper('sphill_customization')->__('Buy Now') . '</span></span></button>';
                    return $html;
                }
            }
        }
        return '';
    }
}
