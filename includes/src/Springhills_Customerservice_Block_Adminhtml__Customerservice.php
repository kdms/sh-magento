<?php


class Springhills_Customerservice_Block_Adminhtml_Customerservice extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_customerservice";
	$this->_blockGroup = "customerservice";
	$this->_headerText = Mage::helper("customerservice")->__("customerservice Manager");
	$this->_addButtonLabel = Mage::helper("customerservice")->__("Add New Item");
	parent::__construct();

	}

}
