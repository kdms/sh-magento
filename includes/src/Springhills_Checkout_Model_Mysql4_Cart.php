<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */


/**
 * Flat sales order resource
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
require_once '/var/www/magento/redback/RedbackServices.php';
class Springhills_Checkout_Model_Mysql4_Cart extends  Mage_Checkout_Model_Resource_Cart
{

	public function getsecureurl(){
		if ($_SERVER["HTTPS"] == 'on') { 
			$gcbaseurl =  Mage::getUrl('', array('_forced_secure' => true) );
 		}else { 
 			$gcbaseurl = Mage::getBaseUrl();
 	    } 
 	    return $gcbaseurl;
	}


	public function insertCustomerCreditCardInfo($data,$quoteId){
		//Saving Credit card details temporarliy till the redback status is success
		$write =Mage::getSingleton('core/resource')->getConnection('core_write');
		// month is single digit then prepend 0
		$ccExpiryMonth = $data['cc_exp_month'];		
		if(strlen($data['cc_exp_month']) == 1)
			$ccExpiryMonth = '0'.$data['cc_exp_month'];
		$ccExpiryDate = $ccExpiryMonth.$data['cc_exp_year'];
		/*ENCODE THE CARD DETAILS*/
		$encust_ccType= base64_encode($data['cc_type']);
		$encust_ccNo=base64_encode($data['cc_number']);
		$encust_ccCid=base64_encode($data['cc_cid']);
		$encust_ccExpDate=base64_encode($ccExpiryDate);
		//check for the existence of quote_id
		$chkForQuote = $write->FetchOne("SELECT COUNT(*) AS COUNT FROM customer_card_details WHERE quote_id=".$quoteId);
		/*END OF ENCODE*/
		if($chkForQuote > 0){
			$write->query("UPDATE customer_card_details SET card_type='".$encust_ccType."',card_number='".$encust_ccNo."',card_cvv='".$encust_ccCid."',card_expirydate='".$encust_ccExpDate."' WHERE quote_id=".$quoteId);
		}else{
			$write->query("INSERT INTO customer_card_details (entity_id,quote_id,card_type,card_number,card_cvv,card_expirydate) VALUES 				(NULL,".$quoteId.",'".$encust_ccType."','".$encust_ccNo."','".$encust_ccCid."','".$encust_ccExpDate."')");
		}
	    //End of saving credit card details 	
	}

	public function  createRedbackOrder(){
		$session = Mage::getSingleton('checkout/session');
		$quoteId= $session->getQuote()->getId();
		//$quoteId= $session->getLastOrderId();
		$write =Mage::getSingleton('core/resource')->getConnection('core_write');
		/***GET THE CARD DETAILS and Customer Number***/
		$creditCardDetails = $write ->fetchAll("SELECT * FROM customer_card_details WHERE quote_id=".$quoteId);
		/******/		
		/** GET SHIPPING INFORMATION**/		
		$shippingAddress = $write->fetchAll('SELECT * FROM sales_flat_quote_address WHERE quote_id='.$quoteId.' and address_type="shipping"');

		/** GET SHIPPING INFORMATION**/		
		$billingAddress = $write->fetchAll('SELECT * FROM sales_flat_quote_address WHERE quote_id='.$quoteId.' and address_type="billing"');
		//Getting gift discount price 
		$FetchOffer = $write->fetchAll("SELECT redback_certificate_number,redback_certificate_amount,redback_replacecert_number,redback_replacecert_amount FROM sales_flat_quote WHERE entity_id=".$quoteId);
		$billingRegionInfo = $this->getRegionInfo($billingAddress[0]['region_id']);
		$ShippingRegionInfo = $this->getRegionInfo($shippingAddress[0]['region_id']);
		//get the customer Number		
		$coresession = Mage::getSingleton("core/session");
		$arrySession=$coresession->getData();
		/*$session->getCustKeycode($data['hdnOfferCatalog']);
		$session->getCustGiftAmounts($data['hdnGiftAmount']);
		$session->getCustGiftNumbers($data['hdnGiftNumber']);*/
		$custNumber = '';
		$custPin =$billingAddress[0]['postcode'];
		if(!empty($arrySession['cust_number'])){
			$custNumber = $arrySession['cust_number'];
			
		}
		$pin = $billingAddress[0]['postcode'];
		$pinhint = 'zipcode';	
		/*if(empty($arrySession['cust_pin'])){
			$pin = $billingAddress[0]['postcode'];
			$pinhint = 'zipcode';		
		}*/	
		
		$custPin = $billingAddress[0]['postcode'];
		
		$title='4';
		$unitPrice=array();
		$sku=array();
		$qty=array();
		$itemDate = array();
		foreach ($session->getQuote()->getAllItems() as $item) {
			$arraySku='';
			$arraySku = explode("_",$item->getSku());
			$sku[] = $arraySku[0];
			$qty[]=$item->getQty();
			$unitPrice[]=$item->getPrice();
			$itemDate[] = $item->getCreatedAt();
		}
		$strSku='';
		$strQty='';
		$strUnitPrice = '';
		$strItemDate	='';
		if(count($sku) > 0 ){
			$strSku= implode('ý',$sku);
		}
		if(count($qty) > 0 ){
			$strQty= implode('ý',$qty);
		}
		if(count($unitPrice) > 0 ){
			$strUnitPrice= implode('ý',$unitPrice);
		}
		if(count($itemDate) > 0 ){
			$strItemDate= implode('ý',$itemDate);
		}
		$promocode =$session->getQuote()->getCouponCode();
		if($promocode == "")
		 $promocode='';
		/*if($arrySession['offer']=='no')
			$promocode='';
		*/
		//Gift Replacments for 1 2 3
		$giftReplacementCertNum1='';
		$giftReplacementCertAmt1='';
		$giftReplacementCertNum2='';
		$giftReplacementCertAmt2='';
		$giftReplacementCertNum3='';
		$giftReplacementCertAmt3='';
		$strGiftNum= '';
		$strGiftAmt='' ;
		if(!empty($FetchOffer[0]['redback_certificate_number'])){
			$gifttArray1 = explode(",",$FetchOffer[0]['redback_certificate_number']);
			$giftReplacementCertNum1=$gifttArray1[0];
			$strGiftNum =$gifttArray1[0];
			if(isset($gifttArray1[1])){
				$giftReplacementCertNum2=$gifttArray1[1];
				$strGiftNum .= ','.$giftReplacementCertNum2;
			}
		}
		if(!empty($FetchOffer[0]['redback_certificate_amount'])){
			$gifttArray2 = explode(",",$FetchOffer[0]['redback_certificate_amount']);
			$giftReplacementCertAmt1=$gifttArray2[0];
			$strGiftAmt = $giftReplacementCertAmt1;
			if(isset($gifttArray2[1])){
				$giftReplacementCertAmt2=$gifttArray2[1];
				$strGiftAmt .= ','.$giftReplacementCertAmt2;
			}
		}
		$strReplaceGiftNum= '';
		$strReplaceGiftAmt='' ;
		if(!empty($FetchOffer[0]['redback_replacecert_number'])){
			$strReplaceGiftNum= $FetchOffer[0]['redback_replacecert_number'];
			$strReplaceGiftAmt= $FetchOffer[0]['redback_replacecert_amount'];
		}

			

		//Add two fields for Mobile
		$is_Mobilephone='';
		$is_Mobileoptin='';
		if($arrySession['is_mobilephone']==1){
			$is_Mobilephone= '1';
		}
		if($arrySession['is_mobileoptin']==1){
			$is_Mobileoptin= '1';
		}
	
		
		$session->setCustGiftReplacementOne($payment['hdnGiftreplacment1']);
	    	$session->setCustGiftReplacementTwo($payment['hdnGiftreplacment2']);
	    	$session->setCustGiftReplacementThree($payment['hdnGiftreplacment3']);

		$orderId= $session->getLastOrderId();
		$objorder = Mage::getModel('sales/order')->load($orderId);
		$data =array(	"OrderNumber"=>'',
				"CustFirstName"=>trim($billingAddress[0]['firstname']),
				"CustLastName"=>trim($billingAddress[0]['lastname']),
				"CustAdd1"=>trim($billingAddress[0]['street']),
				"CustAdd2"=>'',
				"CustAdd3"=>'',
				"CustCity"=>trim($billingAddress[0]['city']),
				//"CustState"=>trim($billingAddress[0]['region']),
				"CustState"=>$billingRegionInfo[0]['code'],
				"CustZip"=>trim($billingAddress[0]['postcode']),
				"CustCountry"=>trim($billingAddress[0]['country_id']),
				"CustPhoneDay"=>trim($billingAddress[0]['telephone']),
				"CustPhoneEve"=>'',
				"CustEmail1"=>trim($billingAddress[0]['email']),
				"CustEmail2"=>'',
				"CustPin"=>$custPin,
				"ShipFirstName"=>trim($shippingAddress[0]['firstname']),
				"ShipLastName"=>trim($shippingAddress[0]['lastname']),
				"ShipAdd1"=>trim($shippingAddress[0]['street']),
				"ShipAdd2"=>'',
				"ShipAdd3"=>'',
				"ShipCity"=>trim($shippingAddress[0]['city']),
				//"ShipState"=>trim($shippingAddress[0]['region']),
				"ShipState"=>$ShippingRegionInfo[0]['code'],
				"ShipZip"=>trim($shippingAddress[0]['postcode']),
				"ShipCountry"=>trim($shippingAddress[0]['country_id']),
				"ShipPhone"=>trim($shippingAddress[0]['telephone']),
				"ShipEmail"=>trim($shippingAddress[0]['email']),
				"ShipAttention"=>'',
				"CustNumber"=>$custNumber,
				"WebReference"=>$session->getLastRealOrderId(),
				"PromoCode"=>$promocode,
				"Title"=>$title,
				"ShipMethod"=>'',
				"PaymentMethod"=>trim(base64_decode($creditCardDetails[0]['card_type'])),
				"CreditCardNumber"=>trim(base64_decode($creditCardDetails[0]['card_number'])),
				"CardExpDate"=>trim(base64_decode($creditCardDetails[0]['card_expirydate'])),
				"CardCVV"=>trim(base64_decode($creditCardDetails[0]['card_cvv'])),
				"CardAddress"=>'',
				"CardZip"=>'',
				"MerchAmount"=>trim($session->getQuote()->getBaseSubtotal()),
				"CouponAmount"=>$session->getQuote()->getBaseSubtotal() - $session->getQuote()->getBaseSubtotalWithDiscount(), //base_discount_amount
				"DiscAmount"=>'',
				"ShipAmount"=>$objorder->getBaseShippingAmount(),
				"PremShipAmount"=>'',
				"OverweightAmount"=>'',
				"TaxAmount"=>$objorder->getBaseTaxAmount(),
				"TotalAmount"=>trim($session->getQuote()->getBaseGrandTotal()),//base_subtotal 	base_grand_total
				"Comments"=>'',
				"Items"=>utf8_decode($strSku), // comma separated
				"QtyOrdered"=>utf8_decode($strQty),// comma separated
				"UnitPrice"=>utf8_decode($strUnitPrice),// comma separated
				"ReleaseDate"=>utf8_decode($strItemDate),
				"FreeFlag"=>'',
				"PersonalizationCode"=>'',
				"PersonalizationDetail1"=>'',
				"PersonalizationDetail2"=>'',
				"PersonalizationDetail3"=>'',
				"PersonalizationDetail4"=>'',
				"PersonalizationDetail5"=>'',
				"PIN"=>$pin,
				"PINHint"=>$pinhint,
				"OptInFlag"=>'',
				"OptInDate"=>'',
				"OptOutDate"=>'',
				"Etrack"=>'',
				"IPSource"=>$_SERVER["REMOTE_ADDR"],
				"GVCFlag"=>'',
				"GVCDate"=>'',
				"ValidErrCode"=>'',
				"ValidErrMsg"=>'',
				"OrderErrCode"=>'',
				"OrderErrMsg"=>'',
				"Cert1_Number"=>$giftReplacementCertNum1,
				"Cert1_Amount"=>$giftReplacementCertAmt1,
				"Cert2_Number"=>$giftReplacementCertNum2,
				"Cert2_Amount"=>$giftReplacementCertAmt2,
				"Cert3_Number"=>$giftReplacementCertNum3,
				"Cert3_Amount"=>$giftReplacementCertAmt3,
				"Repc1_Number"=>$strReplaceGiftNum,
				"Repc1_Amount"=>$strReplaceGiftAmt,
				"Repc2_Number"=>'',
				"Repc2_Amount"=>'',
				"Repc3_Number"=>'',
				"Repc3_Amount"=>'',
				"Web_Date"=>'',
				"Web_Time"=>'',
				"MobilePhone"=>$is_Mobilephone,
				"MobileOptin"=>$is_Mobileoptin
			   );
			
			$objRedBackApi = new RedbackServices();
			$responseRedback = $objRedBackApi->createOrderService($data);
			$orderId= $session->getLastOrderId();
			if($responseRedback['OrderErrCode']=='0' ){
				
				$write->query("DELETE FROM customer_card_details WHERE quote_id=".$quoteId);
				$write->query("Update sales_flat_order SET redback_order_number='".$responseRedback['OrderNumber']."', redback_status='1',redback_keycode='".$promocode."',redback_certificate_number='".$strGiftNum."',redback_certificate_amount='".$strGiftAmt."',redback_mobilephone='".$is_Mobilephone."',redback_mobileoptin='".$is_Mobileoptin."',redback_cust_num='".$responseRedback['CustNumber']."',redback_cust_zip='".$responseRedback['CustZip']."', redback_replacecert_amount='".$strReplaceGiftAmt."',redback_replacecert_number='".$strReplaceGiftNum."'  WHERE quote_id=".$quoteId);
				//Set Session for new customer
				if(empty($custNumber)){
					$this->setSessionNewCustomer($responseRedback);
				}
			}else{
				$write->query("UPDATE customer_card_details SET entity_id= ".$orderId." WHERE quote_id=".$quoteId);
				$write->query("Update sales_flat_order SET redback_status='0',redback_keycode='".$promocode."',redback_certificate_number='".$strGiftNum."',redback_certificate_amount='".$strGiftAmt."',redback_mobilephone='".$is_Mobilephone."',redback_mobileoptin='".$is_Mobileoptin."' , redback_replacecert_amount='".$strReplaceGiftAmt."',redback_replacecert_number='".$strReplaceGiftNum."'  WHERE quote_id=".$quoteId);
			}
	
			
  }


	//http://www.dreamincode.net/forums/topic/10862-zip-code-to-city-state/
	public function get_zip_info($zip) {
		
	 //Function to retrieve the contents of a webpage and put it into $pgdata
	  $pgdata =""; //initialize $pgdata
	  $fd = fopen("http://zipinfo.com/cgi-local/zipsrch.exe?zip=$zip","r"); //open the url based on the user input and put the data into $fd
	  while(!feof($fd)) {//while loop to keep reading data into $pgdata till its all gone
		$pgdata .= fread($fd, 1024); //read 1024 bytes at a time
	  }
	  fclose($fd); //close the connection
	  if (preg_match("/is not currently assigned/", $pgdata)) {
		$city = "N/A";
			$state = "N/A";
	  }
	  else {
		$citystart = strpos($pgdata, "Code</th></tr><tr><td align=center>");
		$citystart = $citystart + 35;
		$pgdata = substr($pgdata, $citystart);
		$cityend = strpos($pgdata, "</font></td><td align=center>");
		$city = substr($pgdata, 0, $cityend);
	  
		$statestart = strpos($pgdata, "</font></td><td align=center>");
		$statestart = $statestart + 29;
		$pgdata = substr($pgdata, $statestart);
		$stateend = strpos($pgdata, "</font></td><td align=center>");
		$state = substr($pgdata, 0, $stateend);
	}
	 $zipinfo[zip] = $zip;
	 $zipinfo[city] = $city;
	 $zipinfo[state] = $state;
	 return $zipinfo;
	}
	
	
	public function getRegionInfo($region){
		//Saving Credit card details temporarliy till the redback status is success
		$write =Mage::getSingleton('core/resource')->getConnection('core_write');
		if(is_numeric($region))
			$regionInfo = $write->FetchAll("SELECT * FROM directory_country_region WHERE region_id=".$region." and country_id='US'");
		else
			$regionInfo = $write->FetchAll("SELECT * FROM directory_country_region WHERE code='".$region."' and country_id='US'");
		return $regionInfo;
	}

	public function setSessionNewCustomer($responseRedback){
		/*********IF customer comes as guest register then set the session bases on the new customer info**********/
			//REDBACK API CALL FOR AUTHENTICATION OF CUSTOMER  60606    '49150029'
			       $objRedBackApi = new RedbackServices();
			       $params =array(	'CustNumber'	=>	$responseRedback['CustNumber'],
						'Title'	=>	'4'	,
						'CustName'	=>	''	,
						'CustAdd1'	=>	''	,
						'CustAdd2'	=>	''	,
						'CustState'	=>	''	,
						'CustCity'	=>	''	,
						'CustZip'	=>	$responseRedback['CustZip'],
						'CustCountry'	=>	''	,
						'CustPhone'	=>	''	,
						'CustFax'	=>	''	,
						'CustEmail'	=>	'',
						'CustPin'	=>	''	,
						'CustClubFlag'	=>	''	,
						'CustClubDisc'	=>	''	,
						'CustClubDate'	=>	''	,
						'CustPinHint'	=>	''	,
						'CustOptInFlag'	=>	''	,
						'CustError' => '',
						'CustMessage' => '',
						'CustOrder'	=>	''	,
						'CustFirstName'	=>	''	,
						'CustLastName'	=>	''	,
						'ClubNumber'	=>	'');

				$responseCustomer = $objRedBackApi->CustMasterAccessEmailZip($params);
				
				//If Customer error code is zero then successfully logged
				if($responseCustomer['CustError']=='0'){
					$customerNumer= $responseCustomer['CustNumber'];
					$customerArray =array();
					$customerArray[$customerNumer]['CustNumber']=$responseCustomer['CustNumber'];
					$customerArray[$customerNumer]['CustAdd1']=$responseCustomer['CustAdd1'];
					$customerArray[$customerNumer]['CustAdd2']=$responseCustomer['CustAdd2'];
					$customerArray[$customerNumer]['State']=$responseCustomer['CustState'];	
					if(empty($responseCustomer['CustState'])){
						$stateCode = $this->get_zip_info($responseCustomer['CustZip']);
						$customerArray[$customerNumer]['State']=$stateCode['state'];	
					}
					$customerArray[$customerNumer]['City']=$responseCustomer['CustCity'];
					$customerArray[$customerNumer]['Country']=$responseCustomer['CustCountry'];
					$customerArray[$customerNumer]['Phone']=str_replace(".","",$responseCustomer['CustPhone']);
					$customerArray[$customerNumer]['CustFax']=$responseCustomer['CustFax'];
					$customerArray[$customerNumer]['Email']=$responseCustomer['CustEmail'];
					$customerArray[$customerNumer]['CustPin']=$responseCustomer['CustPin'];
					$customerArray[$customerNumer]['FirstName']=$responseCustomer['CustFirstName'];
					$customerArray[$customerNumer]['LastName']=$responseCustomer['CustLastName'];
					$customerArray[$customerNumer]['CustZip']=$responseCustomer['CustZip'];
					$session = Mage::getSingleton("core/session");
					$session->setCustNumber($customerArray[$customerNumer]['CustNumber']);
					$session->setCustAdd1($customerArray[$customerNumer]['CustAdd1']);
					$session->setCustAdd2($customerArray[$customerNumer]['CustAdd2']);
					$session->setCustState($customerArray[$customerNumer]['State']);
					$session->setCustCity($customerArray[$customerNumer]['City']);
					$session->setCustCountry($customerArray[$customerNumer]['Country']);
					$session->setCustPhone($customerArray[$customerNumer]['Phone']);
					$session->setCustFax($customerArray[$customerNumer]['CustFax']);
					$session->setCustEmail($customerArray[$customerNumer]['Email']);
					$session->setCustPin($customerArray[$customerNumer]['CustPin']);
					$session->setCustFirstName($customerArray[$customerNumer]['FirstName']);
					$session->setCustLastName($customerArray[$customerNumer]['LastName']);
					$session->setCustZip($customerArray[$customerNumer]['CustZip']);
				//exit;
			}
	}
//amitsharma@somemailll.com
		/*******************/

	public function getOfferPriceBlock($productSku , $keycode)	{
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
		$offerCode = $write->fetchOne("SELECT Offer_Code FROM Keycodes where Keycode='".$keycode."' ");
		$offerProductPrice= $write->fetchAll("SELECT * FROM Price where Product_Number='".$productSku."' and  Offer_Code='".$offerCode."' ");
		$productId = Mage::getModel('catalog/product')->getIdBySku("$productSku");
		$_prod = Mage::getModel('catalog/product')->load("$productId");
		$uom = $_prod->getUnitOfMeasure();
		if(count($offerProductPrice)>0){
			foreach($offerProductPrice as $tierInfo){
				if($uom){
					echo "<span class='price'>".($uom*$tierInfo['Qty_Low']) . " for </span>" . Mage::helper('core')->currency($uom*$tierInfo['Price']). "\n";
				}else{
					echo "<span class='price'>".$tierInfo['Qty_Low'] . " for </span>" . Mage::helper('core')->currency($tierInfo['Price']). "\n";
				}
			}
		
		}
	}
	
	public function getOfferCartPrice($productSku , $keycode ,$qty){
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
		$offerCode = $write->fetchOne("SELECT Offer_Code FROM Keycodes where Keycode='".$keycode."' ");
		$offerProductPrice= $write->fetchOne("SELECT Price FROM Price where Product_Number='".$productSku."' and  Offer_Code='".$offerCode."' and '".$qty."' BETWEEN Qty_Low AND Qty_High  ");
		if($offerProductPrice):
			$price = Mage::helper('core')->currency($offerProductPrice*$qty);
		else:
			$productId = Mage::getModel('catalog/product')->getIdBySku("$productSku");
			$_prod = Mage::getModel('catalog/product')->load("$productId");
			$productPrice = $_prod->getFinalPrice();
			$price = Mage::helper('core')->currency($productPrice*$qty);
		endif;
		return $price;

	}

	public function updateCart($itemId,$productId,$productQty,$action){
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
		$session = Mage::getSingleton('checkout/session');
		$quoteId= $session->getQuote()->getId();
		if($action=='remove'){
			$cartHelper = Mage::helper('checkout/cart');
			$cartHelper->getCart()->removeItem($itemId)->save();
			return true;
		}
		if($action=='update'){
			$cartHelper = Mage::helper('checkout/cart');
			$items = $cartHelper->getCart()->getItems();
			foreach ($items as $item) {
				if($item->getId()==$itemId){
					$item->setQty($productQty);
				    $cartHelper->getCart()->save();
					return true;
				}
			}
		}
	}

	
	/**** Update for Offer Product in cart ****/
	public function updateOfferProduct($offerproductid, $productId){
		
		$quoteid = Mage::getSingleton('checkout/session')->getQuote()->getId();
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
		//echo "Update sales_flat_quote_item set offer_product_id = ".$offerproductid." where quote_id=".$quoteid." and product_id=".$productId; die();
		$write->query("Update sales_flat_quote_item set offer_product_id = ".$offerproductid." where quote_id=".$quoteid." and product_id=".$productId);

	}
	
	/**** get for Offer Product in cart ****/
	public function checkOfferProduct($id){
		$quoteid = Mage::getSingleton('checkout/session')->getQuote()->getId();
        	$quoteItem = Mage::getModel('sales/quote_item')->load($id);
		$quote = Mage::getModel('sales/quote')->load($quoteItem->getQuoteId());
		$offerProductId = $quoteItem->getOfferProductId(); 
		$quoteid = Mage::getSingleton('checkout/session')->getQuote()->getId();
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
		$checkForOffer = $write->fetchOne("select item_id from sales_flat_quote_item where quote_id=".$quoteid." and product_id=".$offerProductId);
		return $checkForOffer;

	}

	public function updateCouponCode($ccode){
		$quoteid = Mage::getSingleton('checkout/session')->getQuote()->getId();
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
		$coupon_code = $write->fetchOne("select coupon_code from sales_flat_quote where entity_id = '".$quoteid."' "); 
		$quote = Mage::getModel('sales/quote')->load($quoteid);
		$coupon_code = $quote->getCouponCode();
		$bannerofferCode = Mage::getSingleton('core/session')->getData("offerCode");
		if($coupon_code=='' && $bannerofferCode!=null ){
			$coupon_code = $bannerofferCode;
		}
		//echo $coupon_code;
		$trimmedCode = explode('_', $coupon_code);
		$subtotal = $quote->getSubtotal();
		if($coupon_code!=null ||  $ccode!=null  ){

			if($coupon_code==null &&  $ccode!=null){
				$trimmedCode = explode('_', $ccode);
			}
			$records = $write->fetchAll("select * from salesrule_coupon where code like '".$trimmedCode[0]."%' order by coupon_id desc "); 
			$count =  count($records);
			$noCoupon = Mage::getModel("checkout/session")->getData("nocoupon");
			if($count>1 && $noCoupon==null){
				$applyFlag = 0 ;
				
				foreach($records as $coupon){
					$couponArray = explode("_",$coupon['code']);	

					if(count($couponArray)==1){
						$conditions_serialized = $write->fetchOne("select conditions_serialized from salesrule where rule_id = '".$coupon['rule_id']."' "); 
						$dataArray = unserialize($conditions_serialized);
						foreach($dataArray as $skey=> $svalue){
							if($skey=='conditions'){
								foreach($svalue as $sv){						
									if($sv['attribute']=='base_subtotal'){
										$rule_base_subtotal=$sv['value'];
									}	
								}
							}
						}
					}

					if(count($couponArray)>1){
						
						$amount = $couponArray[1];
						if($subtotal >= $amount){
						    
							// remove the coupon without "_" and applying the new with "_"
							$applyFlag = 1 ;
							// Applying the new coupon
							Mage::getModel("checkout/session")->setData("coupon_code",$coupon['code']);
							Mage::getModel('checkout/cart')->getQuote()->setCouponCode($coupon['code'])->save();
							Mage::getModel('checkout/cart')->getQuote()->collectTotals();
							Mage::getModel('checkout/cart')->getQuote()->save();
							//Mage::log("_applyCoupon : Set coupon to quote:".$quote->getCouponCode());a
							
							break;
						}
					}
				}
				if(!$applyFlag ){
					//echo $trimmedCode['0']."######".$subtotal."######".$rule_base_subtotal;
					if($subtotal >= $rule_base_subtotal ){
						// Applying the new coupon
						Mage::getModel("checkout/session")->setData("coupon_code",$trimmedCode['0']);
						Mage::getModel('checkout/cart')->getQuote()->setCouponCode($trimmedCode['0'])->save();
						Mage::getModel('checkout/cart')->getQuote()->collectTotals();
						Mage::getModel('checkout/cart')->getQuote()->save();
					}
				}
			} else{
				
				if( $bannerofferCode!=null && $ccode==null && $noCoupon==null){
						$records = $write->fetchAll("select * from salesrule_coupon where code like '".$trimmedCode[0]."%' order by coupon_id desc "); 
						$count =  count($records);
						if($count>1){
						$applyFlag = 0 ;
				
						foreach($records as $coupon){
							$couponArray = explode("_",$coupon['code']);	

							if(count($couponArray)==1){
								$conditions_serialized = $write->fetchOne("select conditions_serialized from salesrule where rule_id = '".$coupon['rule_id']."' "); 
								$dataArray = unserialize($conditions_serialized);
								foreach($dataArray as $skey=> $svalue){
									if($skey=='conditions'){
										foreach($svalue as $sv){						
											if($sv['attribute']=='base_subtotal'){
												$rule_base_subtotal=$sv['value'];
											}	
										}
									}
								}
							}

							if(count($couponArray)>1){
						
								$amount = $couponArray[1];
								if($subtotal >= $amount){
								
									// remove the coupon without "_" and applying the new with "_"
									$applyFlag = 1 ;
									// Applying the new coupon
									Mage::getModel("checkout/session")->setData("coupon_code",$coupon['code']);
									Mage::getModel('checkout/cart')->getQuote()->setCouponCode($coupon['code'])->save();
									Mage::getModel('checkout/cart')->getQuote()->collectTotals();
									Mage::getModel('checkout/cart')->getQuote()->save();
									//Mage::log("_applyCoupon : Set coupon to quote:".$quote->getCouponCode());a
							
									break;
								}
							}
						}
						
					}else{
						Mage::getModel("checkout/session")->setData("coupon_code",$trimmedCode['0']);
						Mage::getModel('checkout/cart')->getQuote()->setCouponCode($trimmedCode['0'])->save();
						Mage::getModel('checkout/cart')->getQuote()->collectTotals();
						Mage::getModel('checkout/cart')->getQuote()->save();
					}
				}
			}
			
		}else{
			//echo 'No Coupon';
		}
	}
	
	public function getBannerImage( $position, $bannerCode){

		if($bannerCode!=4){
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			$Offer_Code = $write->FetchOne("select Offer_Code from Keycodes where Keycode ='".$bannerCode."' ");
			$bannerImage = $write->FetchOne("select Offer_Text from catalog_offers where Offer_Code ='".$Offer_Code."' ");
			$imageArray = explode("^", $bannerImage);
			$expiredate = $imageArray[1];
			if($position =='home'){	
				if($imageArray[0]==1){
					$block ='
					<div class="bc-One"> 
						<span class="TextOne">Limited Time Special</span> 
						<span class="TextTwo">Take $100 OFF<span>your order of $200 or more</span></span> 
						<span class="TextThree"> This limited time offer is good on any of our hundreds of flowering plants, garden plants, shrubs, perennials, and ground covers </span> <span class="TextFour">Start Saving Now!</span>

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==2 ){
					$block ='
					<div class="bc-One"> 
						<span class="TextOne">Limited Time Special</span>
						<span class="TextTwo">Take $20 OFF <span>your order of $50 or more</span></span> 
						<span class="TextThree"> This limited time offer is good on any of our hundreds of flowering plants, garden plants, shrubs, perennials, and ground covers </span> <span class="TextFour">Start Saving Now!</span> 

						<span class="TextExpires">	<span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==3 ){
					$block ='
					<div class="bc-One"> 
						<span class="TextOne">Limited Time Special</span> 
						<span class="TextTwo">Take $25 OFF<span>your order of $50 or more</span></span> 
						<span class="TextThree"> This limited time offer is good on any of our hundreds of flowering plants, garden plants, shrubs, perennials, and ground covers </span> <span class="TextFour">Start Saving Now!</span> 

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==4){
					$block ='
					<div class="bc-One"> 
						<span class="TextOne">Limited Time Special</span> 
						<span class="TextTwo">Take $25 OFF<span>your order of $75 or more</span></span> 
						<span class="TextThree"> This limited time offer is good on any of our hundreds of flowering plants, garden plants, shrubs, perennials, and ground covers </span> <span class="TextFour">Start Saving Now!</span> 

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==5 ){
					$block ='
					<div class="bc-One"> 
						<span class="TextOne">Limited Time Special</span> 
						<span class="TextTwo">Take $50 OFF<span>your order of $100 or more</span></span> 
						<span class="TextThree"> This limited time offer is good on any of our hundreds of flowering plants, garden plants, shrubs, perennials, and ground covers </span> <span class="TextFour">Start Saving Now!</span> 

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==6 ){
					$block ='
					<div class="bc-One"> 
						<span class="TextOne">Limited Time Special</span> 
						<span class="TextTwo">Take $50 OFF<span>your order of $200 or more</span></span> 
						<span class="TextThree"> This limited time offer is good on any of our hundreds of flowering plants, garden plants, shrubs, perennials, and ground covers </span> <span class="TextFour">Start Saving Now!</span> 

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==7){

					$block ='
					<div class="bc-Seven"> 
						<span class="TextOne">Hurry! <br />Take advantage of our</span> 
						<span class="TextTwo">1¢ Sale</span> 
						<span class="TextThree"> Buy one item at regular price get the second for only a penny! </span> <span class="TextFour">Start Saving Now!</span> 

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==8 ){

					$block ='
					<div class="bc-Eight"> 
						<span class="TextOne">Limited -Time ONLY!</span> 
						<span class="TextTwo">FREE
						<span>Shipping & Handling</span></span> 
						<span class="TextThree"> Order by the deadline below and you\'ll get FREE Shipping Handling on your next order!</span> <span class="TextFour">Start Saving Now!</span> 

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==9 ){
					$block ='
					<div class="bc-Eight"> 
						<span class="TextOne">Limited -Time ONLY!</span> 
						<span class="TextTwo">FREE
						<span>Shipping & Handling</span></span> 
						<span class="TextThree">Order by the deadline below and you\'ll get FREE Shipping Handling on your next order of $25 or more!</span> <span class="TextFour">Start Saving Now!</span> 

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==10 ){
					$block ='
					<div class="bc-Eight"> 
						<span class="TextOne">Limited -Time ONLY!</span> 
						<span class="TextTwo">FREE
						<span>Shipping & Handling</span></span> 
						<span class="TextThree">Order by the deadline below and you\'ll get FREE Shipping Handling on your next order of $75 or more!</span> <span class="TextFour">Start Saving Now!</span> 

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==11 ){
					$block ='
					<div class="bc-Eleven"> 
						<span class="TextOne">Sitewide Sale!</span> 
						<span class="TextTwo">Save 10% Off<span>your total order.</span></span> 
						<span class="TextThree">Order by the deadline below and Save 10% off your next order!</span> 
						<span class="TextFour">Start Saving Now!</span> 

						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==12 ){
					$block ='
					<div class="bc-Twelve"> 
						<span class="TextOne">Sitewide Sale!</span> 
						<span class="TextTwo">Save 15% Off<span><em>Plus Free Shipping & Handling</em></span></span> 
						<span class="TextThree">Order by the deadline below and Save 10% off and get Free Shipping and Handling with $25 Order!</span> 
						<span class="TextFour">Start Saving Now!</span>
						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==13 ){
					$block ='';
					return $block;

				}else if($imageArray[0]==14 ){
					$block ='
					<div class="bc-Fourteen"> 
						<span class="TextOne">Take Our Survey and</span> 
						<span class="TextTwo">Get A FREE<br  />$5 Coupon</span> 
						<span class="TextThree">After you receive your order we\'ll email you a 5 question survey. Just answer the question and you\'ll receive a $5 savings coupon for your next purchase. It\'s that simple!</span> 
						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==15 ){
					$block ='
					<div class="bc-Fifteen"> 
						<span class="TextOne">For A Limited-Time ONLY!</span> 
						<span class="TextTwo">SAVE $25<span>when you spend $50</span></span>
						<span class="TextTwo Green">SAVE $50<span>when you spend $100</span></span>
						<span class="TextTwo DarkGreen" >SAVE $100<span>when you spend $200</span></span>


						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==16 ){
					$block ='
					<div class="bc-Sixteen"> 
						<span class="TextOne">For A Limited-Time ONLY!</span> 
						<span class="TextTwo">Save up to 75% Off<span>Select Items</span></span>
						<span class="TextThree">Take advantage of special saving on limited<br />
						qualities of some of our popular items!</span>
						<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
					</div>';
					return $block;
				}
				
			}
			if($position =='right'){  // Banner for Right Position

				if($imageArray[0]==1){
					$block ='
					<div class="bc-right-one">
						<span class="bc-right-text-one">Take $100 OFF</span> 
						<span class="bc-right-text-two">your order of $200 or more</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==2 ){
					$block ='
					<div class="bc-right-two">
						<span class="bc-right-text-one">Take $20 OFF</span> 
						<span class="bc-right-text-two">your order of $50 or more</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==3 ){
					$block ='
					<div class="bc-right-three">
						<span class="bc-right-text-one">Take $25 OFF</span> 
						<span class="bc-right-text-two">your order of $50 or more</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==4){
					$block ='
					<div class="bc-right-four">
						<span class="bc-right-text-one">Take $25 OFF</span> 
						<span class="bc-right-text-two">your order of $75 or more</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==5 ){
					$block ='
					<div class="bc-right-five">
						<span class="bc-right-text-one">Take $50 OFF</span> 
						<span class="bc-right-text-two">your order of $100 or more</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==6 ){
					$block ='
					<div class="bc-right-six">
						<span class="bc-right-text-one">Take $50 OFF</span> 
						<span class="bc-right-text-two">your order of $200 or more</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==7){
					$block ='
					<div class="bc-right-seven">
						<span class="bc-right-text-one">1¢ SALE</span> 
						<span class="bc-right-text-two">Going on NOW!</span>
						<span class="bc-right-text-four">Get your second item for only a penny!</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==8 ){
					$block ='
					<div class="bc-right-eight">
						<span class="bc-right-text-one">FREE</span> 
						<span class="bc-right-text-two">Shipping & Handling</span>
						<span class="bc-right-text-four">Limited -Time ONLY!</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==9 ){
					$block ='
					<div class="bc-right-nine">
						<span class="bc-right-text-one">FREE</span> 
						<span class="bc-right-text-two">Shipping & Handling</span>
						<span class="bc-right-text-four">On orders $25 or more</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==10 ){
					$block ='
					<div class="bc-right-ten">
						<span class="bc-right-text-one">FREE</span> 
						<span class="bc-right-text-two">Shipping & Handling</span>
						<span class="bc-right-text-four">On orders $75 or more</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==11 ){
					$block ='
					<div class="bc-right-eleven">
						<span class="bc-right-text-one">Sitewide Sale!</span> 
						<span class="bc-right-text-two">Save 10% Off</span>
						<span class="bc-right-text-four">your total purchase</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==12 ){
					$block ='
					<div class="bc-right-twelve">
						<span class="bc-right-text-one">Sitewide Sale!</span> 
						<span class="bc-right-text-two">Save 15% Off</span>
						<span class="bc-right-text-four">plus Free Shipping with any $25+ Order</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==13 ){
					$block ='';
					return $block;

				}else if($imageArray[0]==14 ){
					$block ='
					<div class="bc-right-fourteen">
						<span class="bc-right-text-one">Take Our Survey</span> 
						<span class="bc-right-text-two">Get A FREE $5 Coupon</span>
						<span class="bc-right-text-four">Make a purchase and we\'ll email survey. Answer it and you\'ll receive a $5 coupon for your next purchase.</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';

					return $block;

				}else if($imageArray[0]==15 ){
					$block ='
					<div class="bc-right-fifteen">
						<span class="bc-right-text-one"><span class="save">SAVE $25</span><span>when you spend $50</span></span> 
						<span class="bc-right-text-two"><span class="save">SAVE $50</span><span>when you spend $100</span></span>
						<span class="bc-right-text-four"><span class="save">SAVE $100</span><span>when you spend $200</span></span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
					return $block;

				}else if($imageArray[0]==16 ){
					$block ='
					<div class="bc-right-sixteen">
						<span class="bc-right-text-one">Save</span> 
						<span class="bc-right-text-two">up to</span>
						<span class="bc-right-text-four">75<sup>%</sup></span>
						<span class="bc-right-text-five">On Select Items</span>
						<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
					</div>';
					return $block;
				}
			}
		}else{
			if($position =='home'){	
					$block ='
						<div class="bc-One"> 
							<span class="TextOne">Limited Time Special</span> 
							<span class="TextTwo">Take $20 OFF<span>your order of $50 or more</span></span> 
							<span class="TextThree"> This limited time offer is good on any of our hundreds of flowering plants, garden plants, shrubs, perennials, and ground covers </span> <span class="TextFour">Start Saving Now!</span> 

							<span class="TextExpires"><span>Hurry!</span> Offer Expires '.$expiredate.'</span>
						</div>';
					return $block;

			}
			if($position =='right'){
				$block = '
				<div class="bc-right-one">
					<span class="bc-right-text-one">Take $100 OFF</span> 
					<span class="bc-right-text-two">your order of $200 or more</span>
					<span class="bc-right-text-three"><span class="hurry-text">Hurry!</span> Offer <br />Ends '.$expiredate.'</span>
				</div>';
				return $block;
			}

		}
		
	}
	public function deleteCoupon($ruleId){
		$write = Mage::getSingleton('core/resource')->getConnection('core_write');
		$write->query("DELETE from salesrule_coupon where rule_id = '".$ruleId."' "); 
	}
}
