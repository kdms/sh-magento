<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Sphill
 * @package    Sphill_Customization
 * @copyright  Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class Sphill_Customization_Block_Slifeed extends Mage_Core_Block_Text
{
    public function getText()
    {
        return '<script type="text/javascript"> function sli_init_self() {
                    if(typeof sli_init == \'function\') {
                        sli_init();
                    }
                }
                </script>
                <script type="text/javascript" src="' . $this->getSkinUrl('js/autosuggest_js.php') . '"></script>
                <script type="text/javascript">Event.observe(window, \'load\', sli_init_self);</script>';
    }
}
